
import React from 'react';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine, RiLogoutBoxLine, RiProfileFill, RiUser2Fill, RiHomeGearFill, RiSettings2Fill } from 'react-icons/ri';
import { useNavigate, Link, useLocation } from 'react-router-dom';
import Logo from '../assets/gcit_logo.svg'; // Import the logo image
import './Sidebar.css'; // Import the CSS file

const Sidebar = ({ menuItems }) => {
  const navigate = useNavigate();
  const location = useLocation();

  const handleLogout = () => {
    // Clear user session data
    sessionStorage.removeItem('userdata');
    // Redirect user to login page
    navigate('/');
  };

  return (
    <div className="bg-dark text-white vh-100 d-flex flex-column" style={{ position: 'fixed', top: 0, left: 0, width: '17%', zIndex: 1000 }}>
      {/* Logo Section */}
      <div className="d-flex align-items-center justify-content-center pt-5">
        <img src={Logo} alt="Logo" className="p-1 img-fluid ms-1" />
      </div>
      {/* Navigation Menu */}
      <ul className="nav flex-column mt-3 p-4">
        {/* Dynamic menu items */}
        {menuItems.map((menuItem, index) => (
          <li className="nav-item" key={index}>
            <Link
              to={menuItem.link}
              className={`nav-link ${location.pathname === menuItem.link ? 'active' : ''}`}
            >
              {menuItem.icon}
              <span>{menuItem.label}</span>
            </Link>
          </li>
        ))}
        <li className="nav-item">
          <Link
            to={'/profile'}
            className={`nav-link ${location.pathname === 'profile' ? 'active' : ''}`}
          >
            <RiSettings2Fill />
            <span>Change Password</span>
          </Link>
        </li>
        {/* Logout (Static) */}
        <li className="nav-item mt-5 pt-5">
          <button onClick={handleLogout} className='bg-dark border-0 p-0 text-decoration-none text-white'>
            <Link to={''} className="nav-link mt-5 mb-5 d-flex align-items-center">
              <RiLogoutBoxLine className="me-2 d-md-block" />
              <span className="d-none d-md-inline">Logout</span>
            </Link>
          </button>
        </li>
      </ul>
    </div>
  );
};

export default Sidebar;