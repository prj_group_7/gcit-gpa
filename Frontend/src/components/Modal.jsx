import * as React from 'react';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import CButton from './Button';
import TextFieldSizes from './Textfield'; // Import your TextFieldSizes component


  const style = {
    position: 'absolute',
    top: '50%', // Center vertically
    left: '50%', // Center horizontally
    transform: 'translate(-50%, -50%)', // Center both vertically and horizontally
    width: '25%',
    border: '2px solid #000',
    boxShadow: 24,
  };


export default function BasicModal({
  open,
  handleClose,
  title,
  fields,
  buttonName,
  handleSubmit,
  handleChange,
  // showFileUpload, // Prop to control visibility of file upload button
}) {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className='bg-white rounded border-warning d-flex flex-column justify-content-center align-items-center' style={style}>
        {/* Close button */}
        <div style={{ position: 'absolute', top: 7, right: 15, border: "none"}}>
          <button style={{ border: "none", background: "none", fontSize: "50px", color: "#74B444" }} onClick={handleClose} type="button" className="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        {/* Modal title */}
        <Typography className='text-center p-3 mt-5' variant="h4" component="h2" mb={2}>
          {title}
        </Typography>

        {/* Form fields */}
        <div className='w-75 justify-content-center'>
          <TextFieldSizes fields={fields} handleChange={handleChange} />
        </div>

        {/* File upload button (conditionally rendered) */}
        {/* {showFileUpload && (
          <div className='p-3 m-3'>
            <label htmlFor="fileUpload" className="btn btn-primary">
              Upload File
              <input
                id="fileUpload"
                type="file"
                style={{ display: 'none' }}
                onChange={(e)=> handleFileUpload(e.target.files[0])}
              />
            </label>
          </div>
        )} */}

        {/* Submit button */}
        <div className='align-self-center p-3 m-3 '>
          <CButton label={buttonName} onClick={handleSubmit} />
        </div>
      </div>
    </Modal>
  );
}