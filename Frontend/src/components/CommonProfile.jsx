import React, { useState } from "react";
import CButton from "./Button";
import { Alert } from "react-bootstrap";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001'

function CommonProfile() {
  const userData = JSON.parse(sessionStorage.getItem('userdata'));
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const [showChangePassword, setShowChangePassword] = useState(true);
  const [passwordVisibility, setPasswordVisibility] = useState({
    currentPassword: false,
    newPassword: false,
    confirmPassword: false,
  });
  const [passwords, setPasswords] = useState({
    currentPassword: "",
    newPassword: "",
    confirmPassword: ""
  });
  const [errors, setErrors] = useState({
    newPassword: "",
    confirmPassword: ""
  });
  const [showModal, setShowModal] = useState(false);

  const handleToggleChangePassword = () => {
    // setShowChangePassword(!showChangePassword);
  };

  const togglePasswordVisibility = (field) => {
    setPasswordVisibility((prevState) => ({
      ...prevState,
      [field]: !prevState[field],
    }));
  };

  const handleChange = (e) => {
    const { id, value } = e.target;
    setPasswords((prevPasswords) => ({
      ...prevPasswords,
      [id]: value,
    }));

    // Validation logic
    if (id === "newPassword") {
      if (value.length < 8) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          newPassword: "Password must be at least 8 characters long"
        }));
      } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          newPassword: ""
        }));
      }
    }
    if (id === "confirmPassword") {
      if (value !== passwords.newPassword) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          confirmPassword: "Passwords do not match"
        }));
      } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          confirmPassword: ""
        }));
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/users/change-password`, { ...passwords, user_id: userData.user_id });
      if (response.data.success) {
        showAlert(response.data.message, 'success');
      }
    } catch (error) {
      console.error('Error:', error);
      if (error.response.data?.message) {
        showAlert(error.response.data.message, 'danger'); // Display error message if login fails
      } else {
        showAlert('Something went wrong')
      }
    }
  };

  const handleConfirmChangePassword = () => {
    setShowModal(false);
    // Proceed with password change
    alert("Password changed successfully");
  };

  return (
    <div className="container w-75 border border-warning p-5 mt-4 rounded">
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
      <button className="d-block mb-5 rounded" style={{color: '#f68218', borderColor: '#f68218', backgroundColor: 'white'}} onClick={() => window.history.back()}>
        {`Back`}
      </button>
      <div className="d-flex justify-content-start mb-3">
        {/* <span
          className={`me-3 ${!showChangePassword ? "text-success border-bottom border-success" : "text-secondary"}`}
          onClick={() => setShowChangePassword(false)}
          style={{ cursor: "pointer" }}
          
        >
          Profile
        </span> */}
        <span
          className={`${showChangePassword ? "text-success border-bottom border-success" : "text-secondary"}`}
          onClick={handleToggleChangePassword}
          style={{ cursor: "pointer" }}
        >
          Change Password
        </span>
      </div>
      <div>
        {!showChangePassword ? (
          <div className="profile-details">
            <h4 className="mt-4">Profile Information</h4>
            <div className="mt-4 row">
              <div className="col-md-6">
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-person-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Name:</strong></p>
                    <p className="mb-0">John Doe</p>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-book-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Program:</strong></p>
                    <p className="mb-0">Computer Science</p>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-envelope-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Email:</strong></p>
                    <p className="mb-0">johndoe@example.com</p>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-phone-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Contact No:</strong></p>
                    <p className="mb-0">+1234567890</p>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-calendar-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Date of Birth:</strong></p>
                    <p className="mb-0">01/01/1990</p>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className="bi bi-geo-alt-fill me-3" style={{ fontSize: '1.5rem', color: '#75B445' }}></i>
                  <div>
                    <p className="mb-0"><strong>Address:</strong></p>
                    <p className="mb-0">1234 Elm Street, Springfield</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="change-password-form">
            <h4>Change Password</h4>
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label htmlFor="currentPassword">Current Password</label>
                <div className="input-group">
                  <input
                    type={passwordVisibility.currentPassword ? "text" : "password"}
                    className="form-control"
                    id="currentPassword"
                    value={passwords.currentPassword}
                    onChange={handleChange}
                  />
                  <div className="input-group-append">
                    <span
                      className="input-group-text"
                      onClick={() => togglePasswordVisibility("currentPassword")}
                      style={{ cursor: "pointer" }}
                    >
                      <i className={`bi ${passwordVisibility.currentPassword ? "bi-eye" : "bi-eye-slash"}`} ></i>
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-group mt-3">
                <label htmlFor="newPassword">New Password</label>
                <div className="input-group">
                  <input
                    type={passwordVisibility.newPassword ? "text" : "password"}
                    className="form-control"
                    id="newPassword"
                    value={passwords.newPassword}
                    onChange={handleChange}
                  />
                  <div className="input-group-append">
                    <span
                      className="input-group-text"
                      onClick={() => togglePasswordVisibility("newPassword")}
                      style={{ cursor: "pointer" }}
                    >
                      <i className={`bi ${passwordVisibility.newPassword ? "bi-eye" : "bi-eye-slash"}`} ></i>
                    </span>
                  </div>
                </div>
                {errors.newPassword && (
                  <small className="text-danger">{errors.newPassword}</small>
                )}
              </div>
              <div className="form-group mt-3">
                <label htmlFor="confirmPassword">Confirm New Password</label>
                <div className="input-group">
                  <input
                    type={passwordVisibility.confirmPassword ? "text" : "password"}
                    className="form-control"
                    id="confirmPassword"
                    value={passwords.confirmPassword}
                    onChange={handleChange}
                  />
                  <div className="input-group-append">
                    <span
                      className="input-group-text"
                      onClick={() => togglePasswordVisibility("confirmPassword")}
                      style={{ cursor: "pointer" }}
                    >
                      <i className={`bi ${passwordVisibility.confirmPassword ? "bi-eye" : "bi-eye-slash"}`} ></i>
                    </span>
                  </div>
                </div>
                {errors.confirmPassword && (
                  <small className="text-danger">{errors.confirmPassword}</small>
                )}
              </div>
              <div className="col-3 mt-3">
                <CButton className="ms-3 px-5" label="Save" type="submit" />
              </div>


            </form>
          </div>
        )}
      </div>
      {/* Modal */}
      <div className={`modal ${showModal ? 'd-block' : 'd-none'}`} tabIndex="-1" style={{ background: 'rgba(0,0,0,0.5)' }}>
        <div className="modal-dialog " >
          <div className="modal-content border-warning">
            <div className="modal-header">

              <button type="button" className="btn-close" onClick={() => setShowModal(false)}></button>
            </div>
            <div className="modal-body text-center">
              <p>Are you sure you want to change the password?</p>
            </div>
            <div className="modal-footer">

              <div className="d-flex w-100 justify-content-evenly">
                <div className="">
                  <CButton className="ms-3" label="Cancle" onClick={() => setShowModal(false)} />
                </div>
                <div className="">
                  <CButton className="ms-3 px-3 " label="Confirm" onClick={handleConfirmChangePassword} /></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CommonProfile;
