import React from "react";

function CButton({ label, onClick, disabled }) {
  const buttonStyle = {
    backgroundColor: disabled ? "rgba(255, 255, 255, 0.5)" : "rgba(255, 255, 255, 0)",
    border: "2px solid",
    borderColor: disabled ? 'rgba(169, 169, 169, 0.5)' : 'rgba(246,130,24,0.67)',
    color: disabled ? 'rgba(169, 169, 169, 0.5)' : 'rgba(246,130,24,0.67)',
    borderRadius: "10px",
    width: "100%",
    height: "5.5vh",
    transition: 'background-color 0.3s, color 0.3s',
    fontSize: "1.3vw",
  };

  const handleMouseEnter = (e) => {
    if (!disabled) {
      e.target.style.backgroundColor = 'rgba(246, 130, 24, 0.67)';
      e.target.style.borderColor = 'rgba(246, 130, 24, 0.67)';
      e.target.style.color = 'white';
    }
  };

  const handleMouseLeave = (e) => {
    if (!disabled) {
      e.target.style.backgroundColor = 'rgba(255, 255, 255, 0)';
      e.target.style.borderColor = 'rgba(246, 130, 24, 0.67)';
      e.target.style.color = 'rgba(246, 130, 24, 0.67)';
    }
  };

  return (
    <button
      onClick={onClick}
      disabled={disabled}
      style={buttonStyle}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      {label}
    </button>
  );
}

export default CButton;
