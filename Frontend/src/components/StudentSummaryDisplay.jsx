import React from 'react';

const StudentSummaryDisplay = ({ label, count }) => {
  return (
    <div className="container">
          <div className="d-flex flex-column align-items-center">
            <div className="d-flex align-items-center justify-content-center border border-warning border-1 rounded-top-5 p-5 mb-3 w-75">
              <span className="fs-2 fs-md-4 fw-bold text-dark">{count}</span>
            </div>
            <div className="d-flex align-items-center justify-content-center bg-success text-white rounded-bottom-5 p-2 w-75">
              <span className="fs-6 fw-normal">{label}</span>
            </div>
          </div>
    </div>
  );
};

export default StudentSummaryDisplay;
