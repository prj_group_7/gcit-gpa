import React from 'react';
import { RiEdit2Fill } from 'react-icons/ri';

const Profile = ({ imageUrl, name, iconUrl }) => {
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <div className="profile-avatar position-relative">
            <img src={imageUrl} alt="Avatar" className="avatar-image img-fluid rounded-circle" style={{ maxWidth: '100%', maxHeight: '350px' }} />
            <div className="avatar-icon position-absolute" style={{ bottom: '7px', right: '8px' }}>
              <RiEdit2Fill style={{ fontSize: '2rem', color: 'green' }} />
            </div>
          </div>
          <div className="avatar-name text-center mt-3">
            <h2>{name}</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
 