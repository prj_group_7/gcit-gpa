import React from 'react';
import CButton from './Button';

export default function SearchBar({ handleSearch, value }) {
  return (
    <nav className="navbar bg-body">
      <div className=" w-100">
        <form className="d-flex justify-content-between" role="search">
          <input
            className="form-control w-100 border-2 rounded-3"
            type="search"
            placeholder="Search"
            aria-label="Search"
            onChange={handleSearch}
            value={value}
            style={{borderColor: 'rgba(246,130,24,0.67)', fontSize:'1.1vw', height:'5.5vh'}}
          />
        </form>
      </div>
    </nav>
  );
}
