import React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { RiEyeLine } from 'react-icons/ri';
import CButton from './Button';
import { useNavigate } from 'react-router-dom';

function TableHeader({ colNames }) {
  return (
    <TableHead>
      <TableRow>
        {colNames.map((colName, index) => (
          <TableCell
            key={index}
            align="center"
            style={{
              minWidth: 140,
              color: 'rgba(116,180,68,1)',
              // fontWeight: 'bold',
              // backgroundColor: 'lightgrey',
              padding: '15px',
              borderBottom: '2px solid rgba(18, 128, 176, 1)'
            }}
          >
          <h6>{colName}</h6> 
          </TableCell>
        ))}
        <TableCell 
        align='center'   
         style={{
            minWidth: 140,
            color: 'rgba(116,180,68,1)',
            // fontWeight: 'bold',
            // backgroundColor: 'lightgrey',
            padding: '15px',
            borderBottom: '2px solid rgba(18, 128, 176, 1)'
            }}>
            Manage Programs
        </TableCell>
      </TableRow>
    </TableHead>
  );
}

function TableContent({ rows, colNames, colKeyMapping }) {
    const navigate = useNavigate();
    const handleManageRoute = (programId) => {
        navigate(`/manageprogramoffered/${programId}`);
      };
    
      // const handleViewDetails = (programName) => {
      //   navigate(`/viewprogramdetail/${programName}`);
      // };
      const handleViewDetails = () => {
        navigate('/viewprogramdetail');
      };
  return (
    <TableBody>
      {rows.map((row, rowIndex) => (
        <TableRow
          key={rowIndex}
          sx={{ backgroundColor: rowIndex % 2 !== 1 ? 'white' : 'rgba(18, 128, 176, 0.12)' }}
        >
          {colNames.map((colName, colIndex) => (
            <TableCell key={colIndex} align="center">
              {row[colKeyMapping[colName]]}
            </TableCell>
          ))}
          <TableCell align='center'>
            <CButton className="ms-3" label="Manage" onClick={()=>handleManageRoute(row.programoffered_id)} />
          </TableCell>
         
        </TableRow>
      ))}
    </TableBody>
  );
}

export default function AddProgramTable({ rows, colNames, colKeyMapping }) {
  // console.log('table content', rows);
  // console.log(colNames)
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
 
  return (
    <Paper sx={{ width: '100%', overflow: 'hidden' , border: '2px solid rgba(18, 128, 176, 1)', borderRadius:'10px'}}>
      <TableContainer sx={{ maxHeight: 393 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHeader colNames={colNames} />
          <TableContent rows={rows} colNames={colNames} colKeyMapping={colKeyMapping} />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 20, 30]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
