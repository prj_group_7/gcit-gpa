import React from 'react';
import Switch from '@mui/material/Switch';

export default function ToggleButton({ defaultChecked, onToggleChange }) {
  const [checked, setChecked] = React.useState(defaultChecked);

  const handleChange = (event) => {
    const isChecked = event.target.checked;
    setChecked(isChecked);
    onToggleChange(isChecked); // Call the function passed via props
  };

  return (
    <Switch
      checked={checked}
      onChange={handleChange}
      inputProps={{ 'aria-label': 'controlled' }}
    />
  );
}
