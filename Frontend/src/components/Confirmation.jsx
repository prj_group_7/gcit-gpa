import * as React from 'react';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Button from './Button';

const style = {
  position: 'absolute',
  top: '80%',
  left: '50%',
  transform: 'translate(-50%, -100%)',
  width:"25%",
  border: '2px solid #000',
  boxShadow: 24,
};

export default function ConfirmModal({ open, handleClose, title }) {
  const handleSubmit = () => {
    // Handle form submission logic here
    handleClose(); // Close the modal after submission
  };

  return (
    <Modal className='mw-100 p-3'
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      
      <div className='bg-white rounded border-warning  d-flex flex-column justify-content-center align-items-center' style={style}>
      <div style={{ position: 'absolute', top: 7, right: 15, border: "none"}}>
        <button style={{border:"none", background:"none",fontSize:"50px", color:"#74B444"}} onClick={handleClose} type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <Typography className='text-center p-3 mt-5' variant="h4" component="h2" mb={2}>
          {title}
        </Typography>
        
        
        <div className='align-self-center p-3 m-3'>
            <Button label="CONFIRM" onClick={handleSubmit} />
        </div>
        <div className='align-self-center p-3 m-3'>
            <Button label="CANCLE" onClick={handleClose} />
        </div>
        
      </div>
    </Modal>
  );
}
