import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { styled } from '@mui/system';

// Custom styled TextField with blue border color
const CustomTextField = styled(TextField)({
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: 'red', // Initial border color
    },
    '&:hover fieldset': {
      borderColor: 'blue', // Hover border color
    },
    '&.Mui-focused fieldset': {
      borderColor: 'blue', // Focused border color
    },
  },
});

const TextFieldSizes = ({ fields, handleChange }) => {
  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': {
          m: 1,
          width: '100%',
          '& .MuiInputBase-input': {
            height: '1.5em',
            fontSize: '0.875rem',
          },
          '& .MuiInputLabel-root': {
            fontSize: '0.875rem',
          },
        },
      }}
      noValidate
      autoComplete="off"
    >
      {fields.map((field, index) => (
        <div key={index}>
          {field.type === 'text' && (
            <CustomTextField
              className="my-3"
              error={field.error || false}
              id={`outlined-${field.label}-${index}`}
              name={field.name}
              label={field.label}
              defaultValue={field.value || ''}
              onChange={handleChange}
              size="small"
              variant="outlined"
            />
          )}
          
          {field.type === 'dropdown' && (
            <CustomTextField
              className="my-3"
              select
              id={`outlined-${field.label}-${index}`}
              label={field.label}
              name={field.name}
              defaultValue={field.value || ''}
              onChange={handleChange}
              size="small"
              variant="outlined"
            >
              {field.options.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </CustomTextField>
          )}
        </div>
      ))}
    </Box>
  );
};

export default TextFieldSizes;