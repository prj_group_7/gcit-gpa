import React from 'react';
import PropTypes from 'prop-types';
import 'chart.js/auto';
import { Pie } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import ChartDataLabels from 'chartjs-plugin-datalabels'; // Import the plugin
import './sth.css'
const AdminVisuals = ({ title, buttonText, numbers, pageLink }) => {
  const data = {
    labels: ['Male', 'Female'],
    datasets: [
      {
        data: numbers,
        backgroundColor: ['#2196f3', '#ff9800'],
        hoverBackgroundColor: ['#0d47a1', '#e65100'],
      },
    ],
  };

  const options = {
    plugins: {
      datalabels: {
        formatter: (value, context) => {
          return value + '%';
        },
        color: 'white',
        anchor: 'center', // Center align the percentage value
        align: 'center', // Center align the percentage value
        font: {
          weight: 'bold',
        },
      },
    },
    maintainAspectRatio: false, // Disable aspect ratio to control with CSS
    responsive: true,
  };

  return (
    <div className="border border-1 border-success rounded-4 p-4 d-flex flex-column align-items-center">
      <h2 className="mb-4">{title}</h2>
      <div className="position-relative w-100 mb-4 chart-container">
        <Pie data={data} options={options} plugins={[ChartDataLabels]} /> {/* Include the plugin */}
      </div>
      <div className="d-flex justify-content-center align-items-center mb-4">
        <div className="d-flex align-items-center me-4">
          <div className="rounded-circle me-2" style={{ width: '12px', height: '12px', backgroundColor: '#2196f3' }} />
          <span className="font-weight-bold">Male ({numbers[0]})</span>
        </div>
        <div className="d-flex align-items-center me-4">
          <div className="rounded-circle me-2" style={{ width: '12px', height: '12px', backgroundColor: '#ff9800' }} />
          <span className="font-weight-bold">Female ({numbers[1]})</span>
        </div>
        {/* Additional legend items can be added dynamically here */}
      </div>
      <Link to={pageLink}> {/* Replace "/target-page" with the URL of the page you want to navigate to */}
        <button className="btn btn-primary">{buttonText}</button>
      </Link>
    </div>
  );
};

AdminVisuals.propTypes = {
  title: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  numbers: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default AdminVisuals;
