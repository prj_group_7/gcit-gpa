import React, { useState } from 'react';

const Dropdown = ({ options, onChange, value, dropdown_prompt }) => {
  const [selectedOption, setSelectedOption] = useState(value);

  const handleSelect = (event) => {
    const option = event.target.value;
    setSelectedOption(option);
    onChange(option);
  };

  return (
    <div className="dropdown w-100">
      <select className="form-select border-2 rounded-3" value={selectedOption} onChange={handleSelect} style={{borderColor: 'rgba(246,130,24,0.67)'}}>
        <option value="">{dropdown_prompt}</option>
        {options.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Dropdown;
