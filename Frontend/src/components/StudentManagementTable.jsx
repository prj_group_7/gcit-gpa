import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import DropdownStudent from './DropdownStudent';
import { Alert } from 'react-bootstrap';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

const remarks = [
  "Active",
  "Suspended",
  "Medical Leave",
  "Terminated",
];

function TableHeader({ colNames }) {
  return (
    <TableHead>
      <TableRow>
        {colNames.map((colName, index) => (
          <TableCell
            key={index}
            align="center"
            style={{
              minWidth: 140,
              color: 'rgba(116,180,68,1)',
              padding: '15px',
              borderBottom: '2px solid rgba(18, 128, 176, 1)'
            }}
          >
            <h6>{colName}</h6>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

function TableContent({ rows, colNames, colKeyMapping, handleAction }) {
  return (
    <TableBody>
      {rows.map((row, rowIndex) => (
        <TableRow
          key={rowIndex}
          sx={{ backgroundColor: rowIndex % 2 !== 1 ? 'white' : 'rgba(18, 128, 176, 0.12)' }}
        >
          {colNames.map((colName, colIndex) => (
            <TableCell key={colIndex} align="center">
              {colName === 'Remarks' ? (
                <DropdownStudent
                  options={remarks}
                  dropdown_prompt="Select One"
                  onChange={(value) => handleAction(value, row.user_id)}
                  value={row.status} // Set the initial value of the dropdown
                />
              ) : (
                row[colKeyMapping[colName]]
              )}
            </TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  );
}

export default function StudentManagementTable({ rows, colNames, colKeyMapping, reload }) {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const handleAction = async (value, user_id) => {
    try {
      const response = await axios.post(`${API_BASE_URL}/students/updatestudentstatus/${user_id}`, { status: value });
      if (response.data.success) {
        showAlert(response.data.message, 'success');
        // Optionally, you can update the state to reflect the changes
      } else {
        showAlert(response.data.message, 'danger');
      }
      reload();
    } catch (err) {
      showAlert('Internal Server Error', 'danger');
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: '100%', overflow: 'hidden', border: '2px solid rgba(18, 128, 176, 1)', borderRadius: '10px' }}>
      <TableContainer sx={{ maxHeight: 410 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHeader colNames={colNames} />
          <TableContent rows={rows} colNames={colNames} colKeyMapping={colKeyMapping} handleAction={handleAction} />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 20, 30]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
    </Paper>
  );
}
