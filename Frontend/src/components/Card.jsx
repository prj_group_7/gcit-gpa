import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle, faCheckCircle, faHammer } from "@fortawesome/free-solid-svg-icons";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "bootstrap/dist/css/bootstrap.min.css";

const data = [
  {
    title: "Artificial Intelligence",
    programLeader: "Yoenten Jamtsho",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  },
  {
    title: "Machine Learning",
    programLeader: "Pema Yangden",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  },
  {
    title: "Full Stack",
    programLeader: "Sonam Tshering",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  },
  {
    title: "Blockchain",
    programLeader: "Sonam Pemo",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  },
  {
    title: "Blockchain",
    programLeader: "Sonam Pemo",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  },
  {
    title: "Blockchain",
    programLeader: "Sonam Pemo",
    activeStudents: "120",
    totalModules: "24",
    status: "Program Offered"
  }
];

const CardCarousel = () => {
  const [slidesToShow, setSlidesToShow] = useState(4);

  useEffect(() => {
    const updateSlidesToShow = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth < 576) {
        setSlidesToShow(1);
      } else if (screenWidth >= 576 && screenWidth < 768) {
        setSlidesToShow(2);
      } else if (screenWidth >= 768 && screenWidth < 992) {
        setSlidesToShow(3);
      } else if (screenWidth >= 992 && screenWidth < 1200) {
        setSlidesToShow(4);
      } else {
        setSlidesToShow(4);
      }
    };

    updateSlidesToShow();
    window.addEventListener("resize", updateSlidesToShow);
    return () => {
      window.removeEventListener("resize", updateSlidesToShow);
    };
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: slidesToShow,
    slidesToScroll: 1
  };

  return (
    <div className="container border border-warning rounded px-4 py-1 shadow">
      <Slider {...settings}>
        {data.map((item, index) => (
          <div className="p-1" key={index}>
            <div className="card my-3 border p-3 text-start" style={{ height: "300px" }}>
              <h3 className="h5">{item.title}</h3>
              <hr />
              <p className="small">
                <FontAwesomeIcon icon={faUserCircle} /> Program Leader: {item.programLeader}
              </p>
              <p className="small">
                <FontAwesomeIcon icon={faCheckCircle} /> Active Students: {item.activeStudents}
              </p>
              <p className="small">
                <FontAwesomeIcon icon={faHammer} /> Total Modules: {item.totalModules}
              </p>
              <p className="small">
                <FontAwesomeIcon icon={faHammer} /> Status: {item.status}
              </p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default CardCarousel;
