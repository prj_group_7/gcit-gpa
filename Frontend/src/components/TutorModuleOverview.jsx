import React from 'react';

const TutorModuleOverview = ({ moduleCode, moduleImage, activeStudents, viewLink }) => {
  return (
    <div className="text-center">
      <h5 className="card-title fw-bold">{moduleCode}</h5>
      <img 
        src={moduleImage} 
        className="card-img-top img-thumbnail align-self-center rounded-4 border-0" 
        alt="Module" 
        style={{ width: '250px', height: '140px', objectFit: 'cover' }} 
      />
      <div className="card-body">
        <p className="fw-bold">Active Students: {activeStudents}</p>
        <a href={viewLink} className="btn btn-primary">View</a>
      </div>
    </div>
  );
};

export default TutorModuleOverview;
