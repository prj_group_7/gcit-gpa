import React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import CButton from './Button';
import BasicModal from './Modal'
import { useState } from 'react';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001'

function TableHeader({ colNames }) {
  return (
    <TableHead>
      <TableRow>
        {colNames.map((colName, index) => (
          <TableCell
            key={index}
            align="center"
            style={{
              minWidth: 140,
              color: 'rgba(116,180,68,1)',
              // fontWeight: 'bold',
              // backgroundColor: 'lightgrey',
              padding: '15px',
              borderBottom: '2px solid rgba(18, 128, 176, 1)'
            }}
          >
          <h6>{colName}</h6> 
          </TableCell>
        ))}
        <TableCell 
        align='center'   
         style={{
            minWidth: 140,
            color: 'rgba(116,180,68,1)',
            // fontWeight: 'bold',
            // backgroundColor: 'lightgrey',
            padding: '15px',
            borderBottom: '2px solid rgba(18, 128, 176, 1)'
            }}>
            <h6>Actions</h6>
        </TableCell>
      </TableRow>
    </TableHead>
  );
}

function TableContent({ rows, colNames, colKeyMapping }) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [year, setYear] = useState('');
    const [programid, setProgramId] =useState(null);
    const [error,setError] = useState('');
    const [success,setSuccess] = useState('');
    const handleCloseModal = () => {
        setIsModalOpen(false);
      };

    const handleChange = (e) => {
        setYear(e.target.value);
    };
    const handleAlertClose = () => {
        setError('');
        setSuccess('');
      };
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
          const response = await axios.post(`${API_BASE_URL}/coursemanager/programoffered`, {
            programid,
            year,
          });
          setSuccess(response.data.message);
          setError('');
          setIsModalOpen(false); // Close the modal on success
        } catch (err) {
          setError(err.response.data.message);
          setSuccess('');
        }
      };
    const fields = [
        {
          type: 'text',
          label: 'Year',
          name: 'year',
          value: year,
          error: true,
        },
      ];
  return (
    <TableBody>
      {rows.map((row, rowIndex) => (
        <TableRow
          key={rowIndex}
          sx={{ backgroundColor: rowIndex % 2 !== 1 ? 'white' : 'rgba(18, 128, 176, 0.12)' }}
        >
          {colNames.map((colName, colIndex) => (
            <TableCell key={colIndex} align="center">
              {row[colKeyMapping[colName]]}
            </TableCell>
          ))}
          <TableCell align='center'>
             <CButton label={"Offer"} onClick={() => {
                setProgramId(row.program_id)
                setIsModalOpen(true)
             }} />
             {isModalOpen && (
                <BasicModal
                  open={isModalOpen}
                  handleClose={handleCloseModal}
                  title="Offer Program"
                  buttonName="Offer"
                  fields={fields}
                  handleChange={handleChange}
                  handleSubmit={handleSubmit}
                />
              )}
          </TableCell>
        </TableRow>
      ))}

{error && (
        <div className="alert alert-danger position-fixed bottom-0 start-0 p-3">
          {error}
          <span className="ms-auto" onClick={handleAlertClose}>&times;</span>
        </div>
      )}
      {success && (
        <div className="alert alert-success position-fixed bottom-0 start-0 p-3">
          {success}
          <span className="ms-auto" onClick={handleAlertClose}>&times;</span>
        </div>
      )}
    </TableBody>
  );
}

export default function AddProgramTable({ rows, colNames, colKeyMapping }) {
  // console.log('table content', rows);
  // console.log(colNames)
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: '100%', overflow: 'hidden',  border: '2px solid rgba(18, 128, 176, 1)', borderRadius: '10px' }}>
      <TableContainer sx={{ maxHeight: 393 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHeader colNames={colNames} />
          <TableContent rows={rows} colNames={colNames} colKeyMapping={colKeyMapping} />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 20, 30]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
