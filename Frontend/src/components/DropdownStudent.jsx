import React from 'react';

function DropdownStudent({ options, dropdown_prompt, onChange, value }) {
    
  return (
   <div className='dropdown w-100'>
     <select className="form-select border-2 rounded-3" value={value} onChange={(e) => onChange(e.target.value, e)} style={{borderColor: 'rgba(246,130,24,0.67)'}}>
      <option value="" disabled>{dropdown_prompt}</option>
      {options.map((option, index) => (
        <option key={index} value={option}>
          {option}
        </option>
      ))}
    </select>
   </div>
  );
}

export default DropdownStudent;
