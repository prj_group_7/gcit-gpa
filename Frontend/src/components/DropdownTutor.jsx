import React from "react";

function DropdownTutor({ options, onChange }) {
  return (
    <div className="dropdown w-50">
      <select
        className="form-select border-2 rounded-3"
        onChange={onChange}
        style={{ borderColor: 'rgba(246,130,24,0.67)', color:'gray' }}
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
}

export default DropdownTutor;
