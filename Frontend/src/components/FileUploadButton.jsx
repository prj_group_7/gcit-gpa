import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import Typography from '@mui/material/Typography';
import CButton from './Button';

const ModalContent = styled('div')({
  position: 'absolute',
  top: '80%',
  left: '50%',
  transform: 'translate(-50%, -100%)',
  width:"25%",
  backgroundColor: '#fff',
  boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.2)',
  padding: '24px',
  textAlign: 'center',
  borderRadius: '10px',
  border: '2px solid #F68218', // Set border color to F68218
});

const FileUploadModal = ({ open, onClose, onFileSelect,title,mode }) => {
  const [selectedFile, setSelectedFile] = useState(null);

  const handleFileSelect = (file) => {
    setSelectedFile(file);
    if (onFileSelect) {
      onFileSelect(file);
    }
  };

  const handleUpload = () => {
    console.log('Uploading file:', selectedFile);
    onClose();
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    handleFileSelect(file);
  };

  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const buttonLabel = mode === 'update' ? 'Update File' : 'Upload File';  //This is to give the name of the button 

  return (
    <Modal open={open} onClose={onClose}>
      <ModalContent className='bg-white rounded border-warning  d-flex flex-column justify-content-center align-items-center'>
      
        <div style={{ position: 'absolute', top: 3, right: 15, border: "none"}}>
        <button style={{border:"none", background:"none",fontSize:"50px", color:"#74B444"}} onClick={onClose} type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <Typography className='p-3 mt-5 mb-1' variant="h5" gutterBottom>
          {title}
        </Typography>
        <div
          onDrop={handleDrop}
          onDragOver={handleDragOver}
          style={{
            width:'100%', height:100,
            border: '2px dashed #ccc',
            borderRadius: '4px',
            padding: '16px',
            marginTop: '16px',
            marginBottom: '20px',
            cursor: 'pointer',
          }}
        >
          <CloudUploadIcon fontSize="large" style={{color:"74B444"}} />
          <br />
          Drag & Drop files here
        </div>
        <div className="d-flex align-items-center w-75">
          <hr className="flex-grow-1 m-0" />
          <h5 className="m-0 mx-3">OR</h5>
          <hr className="flex-grow-1 m-0" />
        </div>
        <Button
          variant="contained"         
          component="label"
          style={{ marginTop: '16px',marginBottom: '16px', height:'40',width:'150',backgroundColor:"grey" }}
        >
          Browse File
          <input
            name='csvFile'
            type='file'
            style={{ display: 'none' }}
            onChange={(e) => handleFileSelect(e.target.files[0])}
          />
        </Button>
        {selectedFile && (
          <Typography variant="body1" style={{ marginTop: '16px' }}>
            Selected File: {selectedFile.name}
          </Typography>
        )}
        <div style={{ marginTop: '24px' }}>
          
          <CButton  variant="contained"
            color="primary"
            onClick={handleUpload}
            label={buttonLabel}
            
            > 
          </CButton>
        
        </div>
      </ModalContent>
    </Modal>
  );
};

export default FileUploadModal;
