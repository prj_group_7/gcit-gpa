import React from 'react';

const StudentPersonalDetails = ({ name, programme, email, contactNo, dateOfBirth, gender, emergencyContactNo, address }) => {
  return (
    <div className="container bg-white border rounded p-4">
      <h4 className="font-weight-bold mb-4">Personal Details</h4>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#9899;</span>
        <span className="font-weight-bold me-2">Name:</span>
        {name}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#128218;</span>
        <span className="font-weight-bold me-2">Programme:</span>
        {programme}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#9993;</span>
        <span className="font-weight-bold me-2">Email:</span>
        {email}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#9742;</span>
        <span className="font-weight-bold me-2">Contact No:</span>
        {contactNo}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#128197;</span>
        <span className="font-weight-bold me-2">Date of Birth:</span>
        {dateOfBirth}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#9794;</span>
        <span className="font-weight-bold me-2">Gender:</span>
         {gender}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#9990;</span>
        <span className="font-weight-bold me-2">Emergency Contact:</span>
        {emergencyContactNo}
      </div>
      <div className="d-flex align-items-center mb-3">
        <span className="d-inline-block rounded-circle bg-success text-white text-center" style={{ width: '20px', height: '20px', lineHeight: '20px', marginRight: '8px' }}>&#127968;</span>
        <span className="font-weight-bold me-2">Address:</span>
        {address}
      </div>
    </div>
  );
};

export default StudentPersonalDetails;
