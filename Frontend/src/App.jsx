import LoginPage from './pages/LoginPage';
import ManageFaculty from "./pages/admin/ManageFaculty";
import ManageStudent from './pages/admin/ManageStudent';
import AdminDashboard from './pages/admin/AdminDashboard';
import Result from './pages/student/Result';
import StudentProfile from './pages/student/StudentProfile';
import StudentDashboard from './pages/student/StudentDashboard';
import StudentSummaryDisplay from './components/StudentSummaryDisplay';
import GPA from './pages/student/GPA';
import PresidentDashboard from './pages/president/PresidentDashboard';
import FacultyList from './pages/president/FacultyList';
import President_Dashboard from './pages/president/PresidentDashboards';
import TutorEnterMarks from './pages/tutor/TutorEnterMarks';
import TutorVerifyMarks from './pages/tutor/TutorVerifyMarks';
import TutorResults from './pages/tutor/TutorResult';
import TutorModuleRepeat from './pages/tutor/TutorModuleRepeat';
import TeamLeadVeiwProgress from './pages/tutor/TeamLeadViewProgress';
import TutorProfile from './pages/tutor/TutorProfile';
import TutorViewMarks from './pages/tutor/TutorViewMarks';
import TutorEditMarks from './pages/tutor/TutorEditMarks';
import TutorDashboard from './pages/tutor/TutorDashboard';
import TeamLeadDashboard from './pages/tutor/TeamLeadDashboard';
import {Routes, Route} from 'react-router-dom';
import StudentOverview from './pages/president/StudentOverview';
import TeamLeadLists from './pages/president/TeamLeadLists';
import CourseManagerLists from './pages/president/CourseManagerLists';
import AdminLists from './pages/president/AdminLists';
import ModuleOverview from './pages/president/ModuleOverview';
import AddModule from './pages/coursemanager/AddModule';
import AddProgram from './pages/coursemanager/AddProgram';
// import CourseManagerDashboard from './pages/president/CourseManagerLists';
import ManageProgramOffered from './pages/coursemanager/ManageProgramOffered';
import ProgramOffered from './pages/coursemanager/ProgramOffered';
import ViewProgramDetail from './pages/coursemanager/ViewProgramDetail';
import ForgotPassword from './pages/ForgotPassword';
import CourseManagerDashboard from './pages/coursemanager/CourseManagerDashboard';
import CompileMark from './pages/coursemanager/CompileMark';
import IndividualModuleMark from './pages/coursemanager/IndividualModuleMark';
import UpdateFirstYearStudent from './pages/admin/UpdateStudent';
import CommonProfile from './components/CommonProfile';



function App() {

  return (


    <Routes>
      {/* tutor routes */}
      <Route path="/" element={<LoginPage/>}/>
      <Route path="/forgotpassword" element={<ForgotPassword/>}/>
      <Route path="/tutordashboard" element={<TutorDashboard />} />
      <Route path="/tutorprofile" element={<TutorProfile />} />
      <Route path="/tutorentermarks" element={<TutorEnterMarks />} />
      <Route path="/tutorverifymarks" element={<TutorVerifyMarks />} />
      <Route path="/tutorviewmarks" element={<TutorViewMarks />} />
      <Route path="/tutorresults" element={<TutorResults />} />
      <Route path="/tutoreditmarks" element={<TutorEditMarks />} />
      <Route path="/tutormodulerepeat" element={<TutorModuleRepeat />} />
      <Route path="/teamleadviewprogress" element={<TeamLeadVeiwProgress />} />
      <Route path="/teamleaddashboard" element={<TeamLeadDashboard />} />

      {/* president routes */}
      <Route path="/president_dashboard" element={<President_Dashboard />} />
      <Route path="/studentoverview" element={<StudentOverview />} />
      <Route path="/moduleoverview" element={<ModuleOverview />} />
      <Route path="/facultylist" element={<FacultyList />} />


      {/* admin routes */}
      <Route path="/admindashboard" element={<AdminDashboard />} />
      <Route path="/managefaculty" element={<ManageFaculty />} />
      <Route path="/managestudent" element={<ManageStudent />} />
      <Route path='/updatefirstyear' element={<UpdateFirstYearStudent />} />

      {/* student routes */}
      <Route path="/studentdashboard" element={<StudentDashboard />} />
      <Route path="/studentprofile" element={<StudentProfile />} />
      <Route path="/studentresult" element={<Result />} />
      <Route path="/studentgpa" element={<GPA />} />

      {/* Course Manager routes */}
      
      <Route path='/addmodule' element={<AddModule/>} />
      <Route path='/addprogram' element={<AddProgram/>} />
      <Route path='/coursemanagerdashboard' element={<CourseManagerDashboard/>} />
      <Route path='/manageprogramoffered/:programofferedId' element={<ManageProgramOffered/>} />
      <Route path='/programoffered' element={<ProgramOffered/>} />
      <Route path='/viewprogramdetail' element={<ViewProgramDetail/>} />
      <Route path='/compilemark' element={<CompileMark/>} />
      <Route path="/modulemarks/:moduleCode/:moduleoffered_id" element={<IndividualModuleMark />} />

      {/* Profie */}
      <Route path='/profile' element={<CommonProfile/>} />
    </Routes>

  )
}

export default App
