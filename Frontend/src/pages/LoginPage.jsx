import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../assets/logo.svg";
import background from "../assets/Background.png";
import axios from 'axios';
import "./style.css";
import { Alert } from "react-bootstrap";

const API_BASE_URL = 'http://localhost:3001'
function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/login`, { email, password });
      // console.log(response.data)
      if (response.data.success) {
        const userdata = {
          role: response.data.role,
          user_id: response.data.user_id
        };
        console.log(userdata)
        sessionStorage.setItem('userdata', JSON.stringify(userdata));
        if (response.data.role === 'Tutor') {
          navigate('/tutordashboard');
        } else if (response.data.role === 'Course Manager') {
          navigate('/coursemanagerdashboard');
        } else if (response.data.role === 'President') {
          navigate('/president_dashboard')
        } else if (response.data.role === 'Student') {
          navigate('/studentdashboard')
        } else if (response.data.role === 'admin') {
          navigate('/admindashboard')
        } else {
          navigate('/')
        }
      } else {
        showAlert(response.data.error, 'danger');
      }

    } catch (error) {
      console.error('Error:', error);
      if (error.response) {
        showAlert(error.response, 'danger'); // Display error message if login fails
      } else {
        showAlert('Something went wrong')
      }
    }
  }

  return (
    <div
      className="container-fluid p-4 background-radial-gradient overflow-hidden"
      style={{
        background: `url(${background})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        height: "100vh",
      }}
    >
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
      <div className="row">
        <div className="col-md-6 d-flex flex-column justify-content-center align-items-center text-center text-md-start mt-5">
          <img src={logo} alt="logo" style={{ width: 400, height: 400 }} />
        </div>
        <div
          className="col-md-4 position-relative mt-5"
          style={{
            borderRadius: "30px",
            borderColor: "rgba(246,130,24,0.7)",
            border: "2px solid",
          }}
        >
          <div
            id="radius-shape-1"
            className="position-absolute rounded-circle shadow-5-strong"
          ></div>
          <div
            id="radius-shape-2"
            className="position-absolute shadow-5-strong"
          ></div>
          <div className="my-5 bg-glass">

            <div className="p-5">
              <h2 className="mb-3" style={{ color: "rgba(246,130,24,0.67)" }}>
                WELCOME
              </h2>
              <h6
                className="mt-2 mb-4"
                style={{
                  color: "rgba(246,130,24,0.67)",
                  fontSize: "19px",
                  fontWeight: "normal",
                }}
              >
                Please login to your account
              </h6>
              <input
                className="form-control mb-5 placeholder-color"
                placeholder="Email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                style={{
                  color: "white",
                  borderBottom: "2px solid rgba(246,130,24,0.67)",
                  borderTop: "none",
                  borderLeft: "none",
                  borderRight: "none",
                  backgroundColor: "rgba(255,255,255,0)",
                  transition: "border-bottom 0.3s, box-shadow-0 0.1s",
                  borderRadius: "0px",
                }}
              />
              <input
                className="form-control mb-5 placeholder-color"
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                style={{
                  color: "white",
                  borderBottom: "2px solid rgba(246,130,24,0.67)",
                  borderTop: "none",
                  borderLeft: "none",
                  borderRight: "none",
                  backgroundColor: "rgba(255,255,255,0)",
                  transition: "border-bottom 0.3s, box-shadow 0.1s",
                  borderRadius: "0px",
                }}
              />
              <button
                className="btn btn-primary w-100 mb-4"
                type="button"
                style={{
                  backgroundColor: "rgba(255, 255, 255, 0)",
                  border: "2px solid",
                  borderColor: "rgba(246,130,24,0.67)",
                  color: "rgba(246,130,24,0.67)",
                  borderRadius: "10px",
                  transition: "background-color 0.3s, color 0.3s",
                }}
                onMouseEnter={(e) => {
                  e.target.style.backgroundColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.borderColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.color = "white";
                }}
                onMouseLeave={(e) => {
                  e.target.style.backgroundColor = "rgba(255, 255, 255, 0)";
                  e.target.style.borderColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.color = "rgba(246, 130, 24, 0.67)";
                }}
                onClick={handleSubmit}
              >
                Login
              </button>
              <Link
                to="/forgotpassword"
                style={{
                  color: "rgba(255, 255, 255, 0.7)",
                  textDecoration: "none",
                }}
              >
                Forgot Password?
              </Link>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
