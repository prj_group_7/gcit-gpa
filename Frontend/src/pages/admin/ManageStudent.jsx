import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StudentManagementTable from "../../components/StudentManagementTable";
import { RiDashboardLine, RiTeamLine, RiUserSearchLine,RiUpload2Line } from "react-icons/ri";
import {Alert} from "react-bootstrap"
import CButton from "../../components/Button";
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";
import FileUploadModal from "../../components/FileUploadButton";
import axios from 'axios';
import Papa from 'papaparse';

const API_BASE_URL = 'http://localhost:3001';

function ManageStudent() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalMode, setModalMode] = useState('add');
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [studentLists, setStudentLists] = useState([]);
  const [filteredStudents, setFilteredStudents] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [selectedProgram, setSelectedProgram] = useState('');
  const [selectedYear, setSelectedYear] = useState('');

  useEffect(() => {
    
    fetchUsers();
  }, []);
  const fetchUsers = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/students`);
      setStudentLists(response.data.data);
    } catch (error) {
      console.error(error);
    }
  };
  // console.log(studentLists)
  const menuItems = [
    { link: "/admindashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/managefaculty", icon: <RiTeamLine />, label: "Faculty" },
    { link: "/managestudent", icon: <RiUserSearchLine />, label: "Student" },
    { link: "/updatefirstyear", icon: <RiUpload2Line />, label: "Update Detail" },
  ];

  const programOptions = [
    "Bachelor of Science in Computer Science",
    "Bachelor of Science in Information Technology",
    "Bachelor Of Computer Science (AI Development & Data Science)",
    "Bachelor Of Computer Science (Blockchain Development)",
    "Bachelor Of Computer Science (Full Stack Development)",
    "Bachelor of Digital Media & Development",
  ];

  const yearOptions = [
    "Year 1",
    "Year 2",
    "Year 3",
    "Year 4",
    "Year 5",
    "Year 6",
    "Year 7",
    "Year 8",
  ];

  // Define column names
  const columnNames = ["ID", "Name", "Semester", "Programme", "Remarks"];

  // Sample data (replace with your own data structure)
  // const listOfStudents = [
  //   { ID: '12200011', Name: 'Balav Sharma', Semester: '7', StudentType: 'Regular' },
  //   { ID: '12200012', Name: 'Rohit Sharma', Semester: '8', StudentType: 'Regular' },
  //   { ID: '12240013', Name: 'Kunal Sharma', Semester: '1', StudentType: 'Regular' },
  //   { ID: '12200014', Name: 'Resi Dev Kafley', Semester: '8', StudentType: 'Regular' },
  //   { ID: '12200015', Name: 'Karan Sharma', Semester: '4', StudentType: 'Regular' },
  //   { ID: '12210016', Name: 'Vishal Sharma', Semester: '6', StudentType: 'Regular' },
  //   { ID: '12230017', Name: 'Amit Sharma', Semester: '2', StudentType: 'Regular' },
  //   { ID: '12200018', Name: 'Sherab Dorji', Semester: '8', StudentType: 'Regular' },
  //   { ID: '12200019', Name: 'Nitin Sharma', Semester: '8', StudentType: 'Regular' },
  // ]

  const colKeyMapping = {
    'ID': 'student_id',  
    'Name': 'name',
    'Semester': 'enrolled_semester',
    'Programme':'program_name',
    "Remarks": "remarks"
  };
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
        setAlertMessage(null);
    }, 3000);
    };
  const handleAddButtonClick = () => {
    setModalMode('add');
    setIsModalOpen(true);
  };

  const handleUpdateButtonClick = () => {
    setModalMode('add');
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleFileUpload = async (file) => {
    try {
      if (!file) {
        setSuccessMessage('');
        setErrorMessage('Please select a CSV file to upload!');
        return;
      }

      const parsedData = Papa.parse(file, {
        complete: async (results) => {
          if (results && results.data) {
            const response = await axios.post(`${API_BASE_URL}/students`, results.data);
            if (response.data.success) {
              showAlert(response.data.message, 'success');
             
              handleCloseModal();
            } else {
              // setSuccessMessage('');
              showAlert(response.data.message, 'danger');
            }
          }
        }
      });

    } catch (error) {
      console.error('Error uploading file:', error);
      setSuccessMessage('');
      setErrorMessage('Error uploading file!');
    }
  };

  const handleSearch = (event) => {
    const { value } = event.target;
    setSearchText(value);
    const filteredStudents = studentLists.filter((student) => 
      student.name.toLowerCase().includes(value.toLowerCase()) ||
      student.student_id.toString().includes(value.toLowerCase()) ||
      student.type.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredStudents(filteredStudents);
  };
  
  

  const handleProgramChange = (selectedProgram) => {
    setSelectedProgram(selectedProgram);
    const filteredStudents = studentLists.filter(student => {
      return student.programoffered_id === selectedProgram;
    });
    setFilteredStudents(filteredStudents);
  };

  const handleYearChange = (selectedYear) => {
    setSelectedYear(selectedYear);
    const filteredStudents = studentLists.filter(student => {
      return student.enrolled_semester === selectedYear;
    });
    setFilteredStudents(filteredStudents);
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Student Management</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2">
              {/* <CButton className="ms-2" label="UPDATE" onClick={handleUpdateButtonClick} /> */}
            </div>
            <div className="col-2">
              {/* <CButton className="ms-2" label="ADD" onClick={handleAddButtonClick} /> */}
              <FileUploadModal 
                open={isModalOpen}
                onClose={handleCloseModal}
                onFileSelect={handleFileUpload}
                title={modalMode === 'add' ? 'Upload CSV File To Add Student' : 'Upload CSV File To Update Student'}
                mode={modalMode}
              />
            </div>
          </div>

          <div className="row align-items-center mb-3">
            <div className="col-7">
              <SearchBar handleSearch={handleSearch} value={searchText} />
            </div>
            <div className="col-1"></div>
            <div className="col-2">
              {/* <Dropdown options={programOptions} dropdown_prompt="Select Program" onChange={handleProgramChange} value={selectedProgram} /> */}
            
            </div>
            <div className="col-2">
              {/* <Dropdown options={yearOptions} dropdown_prompt="Select Year" onChange={handleYearChange} value={selectedYear} /> */}
              <CButton className="ms-2" label="ADD" onClick={handleUpdateButtonClick} />
            </div>
          </div>

          <div className="row">
            <div className="col-12 main rounded-2">
            <StudentManagementTable rows={filteredStudents.length ? filteredStudents : studentLists} colNames={columnNames} colKeyMapping={colKeyMapping} reload={fetchUsers} />
              {/* <StickyHeadTable rows={listOfStudents} colNames={columnNames} colKeyMapping={colKeyMapping}/> */}
            </div>
          </div>
          <div className="row mt-3">
            <div className="col d-flex justify-content-end">
              {/* <CButton className="ms-2" label="ADD" onClick={handleUpdateButtonClick} /> */}
            </div>
            <div className="col d-flex justify-content-end">
              {/* <CButton className="ms-2" label="UPDATE" onClick={handleUpdateButtonClick} /> */}
            </div>
          </div>
        </div>
      </div>

        {alertMessage && (
            <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
              {alertMessage.message}
            </Alert>
          )}
    </div>
  );
}

export default ManageStudent;
