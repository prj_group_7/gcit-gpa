import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import TutorManagementTable from '../../components/TutorManagementTable';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine,RiUpload2Line } from 'react-icons/ri';
import {Alert} from 'react-bootstrap';
import CButton from '../../components/Button';
import BasicModal from '../../components/Modal';
import axios from 'axios';
import Papa from 'papaparse';
import FileUploadModal from '../../components/FileUploadButton';

const API_BASE_URL = 'http://localhost:3001';

function ManageFaculty() {
  const [formData, setFormData] = useState({
    username: '',
    email: '',
    role: ''
  });

  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [modalMode, setModalMode] = useState('add');
  const [users, setUsers] = useState([]);
  const [filteredUsers, setFilteredUsers] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');

  // Separate states for each modal
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [isFileUploadModalOpen, setIsFileUploadModalOpen] = useState(false);
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
        setAlertMessage(null);
    }, 3000);
    };
  useEffect(() => {
    
    fetchUsers();
  }, []); // Empty dependency array means this effect runs only once when the component mounts
  const fetchUsers = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/users`);
      setUsers(response.data.data);
      setFilteredUsers(response.data.data);
      console.log('Inside use Effect', filteredUsers);
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleCloseModal = () => {
    setIsAddModalOpen(false);
    setIsUpdateModalOpen(false);
    setIsFileUploadModalOpen(false);
  };

  const handleAddButtonClick = () => {
    setModalMode('add');
    setIsAddModalOpen(true);
  };

  const handleUpdateButtonClick = () => {
    setModalMode('update');
    setIsUpdateModalOpen(true);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
     // Validate form data
  if (!formData.username || !formData.email || !formData.role) {
    showAlert('Please fill in all fields before submitting.', 'danger');
    return;
  }

    try {
      const response = await axios.post(`${API_BASE_URL}/users`, formData);
      console.log('This is form data: ', formData);
      showAlert(response.data.message, 'success')
      setIsAddModalOpen(false); // Close the modal on success
      setIsUpdateModalOpen(false); // Close the modal on success
    } catch (err) {
      showAlert(err.response.data.message, 'danger');
    }
  };


  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = users.filter((user) =>
      user.username.toLowerCase().includes(value.toLowerCase()) ||
      user.email.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredUsers(filtered);
  };

  const menuItems = [
    { link: "/admindashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/managefaculty", icon: <RiTeamLine />, label: "Faculty" },
    { link: "/managestudent", icon: <RiUserSearchLine />, label: "Student" },
    { link: "/updatefirstyear", icon: <RiUpload2Line />, label: "Update Detail" },
  ];

  // Define column names
  const columnNames = ["SL.No", "Name", "Email", "Role", "Remarks"];

  const colKeyMapping = {
    'SL.No': 'user_id',
    'Name': 'username',
    'Email': 'email',
    'Role': 'role'
  };

  const fields = [
    {
      type: 'text',
      label: 'Name',
      name: 'username',
      value: formData.username,
    },
    {
      type: 'text',
      label: 'Email',
      name: 'email',
      value: formData.email,
      error: true,
    },
    {
      type: 'dropdown',
      label: 'Role',
      name: 'role',
      value: formData.role,
      options: [
        { label: 'Tutor', value: 'Tutor' },
        { label: 'Course Manager', value: 'Course Manager' },
        { label: 'President', value: 'President' },
      ],
    },
  ];

  const handleFileUpload = (file) => {
    try {
      if (!file) {
        setSuccess('');
        setError('Please select a CSV file to upload!');
        return;
      }

      Papa.parse(file, {
        complete: async (results) => {
          if (results && results.data) {
            const response = await axios.post(`${API_BASE_URL}/users/addall`, results.data);
            console.log(results.data);
            if (response.data.success) {
              showAlert(response.data.message, 'success')
              handleCloseModal();
            } else {
              showAlert(response.data.message, 'danger')
            }
          }
        }
      });

    } catch (error) {
      console.error('Error uploading file:', error);
      setSuccess('');
      setError('Error uploading file!');
    }
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{ color: 'rgba(246,130,24,0.8)' }}>Faculty Management</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
              
              {isFileUploadModalOpen && (
                <FileUploadModal
                  open={isFileUploadModalOpen}
                  onClose={handleCloseModal}
                  onFileSelect={handleFileUpload}
                  title={modalMode === 'add' ? 'Upload CSV File To Bulk Upload Staff' : 'Upload CSV File To Update Staff'}
                  mode={modalMode}
                  
                />
              )}
            </div>
          </div>
          <div className="row align-items-center mb-3">
            <div className="col-7">
              <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
            <div className='col-1'></div>
            <div className="col-2 d-flex justify-content-end">
              <CButton className="ms-3" label="ADD" onClick={handleAddButtonClick} />
              {isAddModalOpen && (
                <BasicModal
                  open={isAddModalOpen}
                  handleClose={handleCloseModal}
                  title="Add Faculty"
                  fields={fields}
                  handleChange={handleChange}
                  handleSubmit={handleSubmit}
                  // showFileUpload={false}
                  buttonName="Add Faculty"
                />
              )}
            </div>
            <div className='col-2 d-flex justify-content-end'>
            <CButton label="UPLOAD" onClick={() => setIsFileUploadModalOpen(true)} />
            </div>
          </div>
          <div className="row">
            <div className="col-12 main rounded-2">
              <TutorManagementTable rows={filteredUsers} colNames={columnNames} colKeyMapping={colKeyMapping} reload={fetchUsers} />
            </div>
          </div>
          <div className="row mt-3">
            <div className="col d-flex justify-content-center"></div>
          </div>
        </div>
      </div>
      {alertMessage && (
            <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
              {alertMessage.message}
            </Alert>
          )}
    </div>
  );
}

export default ManageFaculty;
