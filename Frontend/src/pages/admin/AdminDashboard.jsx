import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { RiDashboardLine, RiTeamLine, RiUserSearchLine, RiUpload2Line } from "react-icons/ri";
import AdminVisuals from "../../components/AdminVisuals";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function AdminDashboard() {
  // const [studentGenderData, setStudentGenderData] = useState([]);
  // const [nonStudentGenderData, setNonStudentGenderData] = useState([]);

  const [studentMaleGenderCount, setStudentMaleGenderCount]=useState(0)
  const [studentFemaleGenderCount, setStudentFemaleGenderCount]=useState(0)
  const [facultyMaleGenderCount, setFacultyMaleGenderCount]=useState(0)
  const [facultyFemaleGenderCount,setFacultyFemaleGenderCount]=useState(0)

  useEffect(() => {
    fetchGenderData();
  }, []);

  const fetchGenderData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/students/gendercount`);
      const { nonStudentGenderCount, studentGenderCount } = response.data;
      
      // setNonStudentGenderData(nonStudentGenderCount);
      // setStudentGenderData(studentGenderCount);
      console.log(studentGenderCount);
      setStudentMaleGenderCount(studentGenderCount[1].count)
     setStudentFemaleGenderCount(studentGenderCount[0].count)
      setFacultyMaleGenderCount(nonStudentGenderCount[1].count)
      setFacultyFemaleGenderCount(nonStudentGenderCount[0].count)
    
   
    } catch (error) {
      console.error("Error fetching gender data:", error);
    }
  };
  console.log(studentMaleGenderCount)
  // console.log(studentGenderData)
  // Define the array of menu items
  const menuItems = [
    { link: "/admindashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/managefaculty", icon: <RiTeamLine />, label: "Faculty" },
    { link: "/managestudent", icon: <RiUserSearchLine />, label: "Student" },
    { link: "/updatefirstyear", icon: <RiUpload2Line />, label: "Update Detail" },
  ];

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">
          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Dashboard</h3>
            </div>
            <div className="col-6"></div>
          </div>

          <div className="row align-items-center justify-content-center pt-4">
            <div className="col">
              <AdminVisuals title="Student Overview" buttonText="Manage Students" numbers={[studentMaleGenderCount, studentFemaleGenderCount]} pageLink={"/managestudent"} />
            </div>
            <div className="col">
              <AdminVisuals title="Faculty Overview" buttonText="Manage Faculty" numbers={[facultyMaleGenderCount, facultyMaleGenderCount[1]]} pageLink={"/managefaculty"} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDashboard;
