import React from "react";
import ReactDOM from "react-dom";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarCheckFill, RiCalendarScheduleLine, RiCalendarView, RiDashboardLine, RiMarkPenFill, RiSoundModuleFill, RiTeamLine, RiUser2Fill, RiUserFill, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";
import { Checkbox } from "@mui/material";

function TutorEditMarks() {
  // Define the array of menu items
  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView />, label: "View Marks" },
    { link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  const moduleOptions = [
    "PLT 302",
    "MAT 303",
    "MLT 304",
    "PRJ 303",
    "PRJ 305",
  ];

  // Define column names
  const columnNames = ["SN","ID","Marks",];

  // Sample data (replace with your own data structure)
  const listOfMarks = [
    { SN: 1, ID:'12200001', Marks: <input type="text" defaultValue="" style={{ width: '60px' }} />   },
    { SN: 2, ID:'12200021', Marks: <input type="text" defaultValue="" style={{ width: '60px' }} />  },
    { SN: 3, ID:'12200031', Marks: <input type="text" defaultValue="" style={{ width: '60px' }} />   },
    { SN: 4, ID:'12200022', Marks: <input type="text" defaultValue="" style={{ width: '60px' }} />   },
    // Add more rows as needed
  ];

  const colKeyMapping = {
    'SN':'SN',
    'ID': 'ID',
    'Marks': 'Marks',
    'Verify': 'Verify',
  };

  const handleSave = () => {
    // Implement your upload logic here
    alert("Marks Saved!");
  };

  return (
    <div className="container-fluid">
      <div className="row">

        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3>Edit Marks</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>

          <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
              <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
              <Dropdown options={moduleOptions} dropdown_prompt="Select Module" />
            </div>
          </div>

          <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
              <StickyHeadTable rows={listOfMarks} colNames={columnNames} colKeyMapping={colKeyMapping} />
              <div className="text-center mt-3">
                <button onClick={handleSave} className="btn btn-orange" style={{ color: 'orange', border: '2px solid orange', width: '150px' }}>Save</button>
              </div>
              <div className="text-center mt-3">
                 {/* Line break */}
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
}
export default TutorEditMarks;