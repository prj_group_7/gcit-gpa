import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import DropdownTutor from "../../components/DropdownTutor";
import SearchBar from "../../components/SearchBar";
import { Alert } from 'react-bootstrap';
import { RiCalendarCheckFill, RiCalendarView, RiDashboardLine, RiMarkPenFill } from "react-icons/ri";
import axios from 'axios';
import CButton from "../../components/Button";
import FileUploadModal from "./FileUploadButton";
import Papa from 'papaparse';
const API_BASE_URL = 'http://localhost:3001';

function TutorEnterMarks() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalMode, setModalMode] = useState('add');
  const [modules, setModules] = useState([]);
  const [students, setStudents] = useState([]);
  const [selectedModule, setSelectedModule] = useState('');
  const [marks, setMarks] = useState({});
  const [isVerifier, setIsVerifier] = useState(false);
  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }

  useEffect(() => {
    checkIfVerifier();
    fetchModules();
  }, []);

  useEffect(() => {
    if (selectedModule) {
      fetchStudents(selectedModule);
    }
  }, [selectedModule]);

  const checkIfVerifier = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor/check`, {
        params: { user_id },
      });
      setIsVerifier(response.data.is_verifier);
    } catch (e) {
      console.log(e.message);
    }
  };

  const fetchModules = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor/getmodules`, {
        params: { user_id },
      });
      setModules(response.data);
      if (response.data.length > 0) {
        setSelectedModule(response.data[0].moduleoffered_id);
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  const fetchStudents = async (moduleoffered_id) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/coursemanager/modulesoffered/getstudents`, {
        params: { moduleoffered_id },
      });

      // Fetch existing marks for each student
      const marksResponse = await axios.get(`${API_BASE_URL}/tutor/marks/getmarks`, {
        params: { moduleoffered_id }, // Assuming you have an API endpoint to fetch marks
      });

      // Map marks to student IDs for easy lookup
      const studentMarks = marksResponse.data.reduce((acc, mark) => {
        acc[mark.student_id] = mark.marks_obtained;
        return acc;
      }, {});

      // Update the local state with fetched students and their marks
      setStudents(response.data);
      setMarks(studentMarks);
    } catch (e) {
      console.log(e.message);
    }
  };

  const handleMarksChange = (student_id, marks_obtained) => {
    const marksValue = parseFloat(marks_obtained);
    if (marksValue < 0 || marksValue > 100) {
      showAlert('Marks must be between 0 and 100', 'danger');
      return;
    }

    setMarks((prevMarks) => ({
      ...prevMarks,
      [student_id]: marks_obtained,
    }));
  };

  const handleUpdateButtonClick = () => {
    setModalMode('update');
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleFileUpload = async (file) => {
    try {
      if (!file) {
        showAlert('Please select a CSV file to upload!', 'danger');
        return;
      }

      Papa.parse(file, {
        complete: async (results) => {
          if (results && results.data) {
            const dataToSend = {
              parsedData: results.data,
              moduleoffered_id: selectedModule, // Include moduleoffered_id
              staff_id: user_id, // Include staff_id
            };

            const response = await axios.post(`${API_BASE_URL}/tutor/marks/addallmarks`, dataToSend);
            if (response.data.success) {
              showAlert(response.data.message, 'success');
              handleCloseModal();
            } else {
              showAlert(response.data.message, 'danger');
            }
          }
        }
      });

    } catch (error) {
      console.error('Error uploading file:', error);
    }
  };

  const handleUploadMarks = async () => {
    const marksData = students.map((student) => ({
      student_id: student.student_id,
      modulesoffered_id: selectedModule,
      marks_obtained: marks[student.student_id] || 0,
      staff_id: user_id,
    }));

    try {
      const response = await axios.post(`${API_BASE_URL}/tutor/marks/addmarks`, { marks: marksData, moduleoffered_id: selectedModule });
      if (response.data.success) {
        showAlert(response.data.message, 'success');
      } else {
        showAlert(response.data.message, 'danger');
      }
    } catch (e) {
      console.log(e.message);
      showAlert('Internal Server Error', 'danger');
    }
  };

  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView />, label: "View Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  if (isVerifier) {
    menuItems.push({ link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" });
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5 ">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{ color: 'rgba(246,130,24,0.8)' }}>Enter Marks</h3>
            </div>
          </div>
          <div className="row align-items-center mb-3">
            <div className="col-6">
              <SearchBar />
            </div>
            <div className="col-1">
              <FileUploadModal
                open={isModalOpen}
                onClose={handleCloseModal}
                onFileSelect={handleFileUpload}
                title={modalMode === 'add' ? 'Upload CSV File To Upload student marks' : 'Upload CSV File To Upload Student marks'}
                mode={modalMode}
              />
            </div>
            <div className="col-5 d-flex flex-row-reverse">
              <DropdownTutor
                options={modules.map((module) => ({ value: module.moduleoffered_id, label: module.module_name }))}
                onChange={(e) => setSelectedModule(e.target.value)}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <StickyHeadTable
                rows={students.map((student, index) => ({
                  SN: index + 1,
                  ID: student.student_id,
                  Marks: (
                    <input
                      type="text"
                      value={marks[student.student_id] || 0} // Display existing marks or 0 if none
                      onChange={(e) => handleMarksChange(student.student_id, e.target.value)}
                      style={{
                        width: "60px",
                        borderRadius: "5px",
                        border: "2px solid rgba(246, 130, 24, 0.8)",
                        paddingLeft: '20px',
                          color: student.student_type === 'MR' ? 'red' : 'gray'
                      }}
                    />
                  ),
                }))}
                colNames={["Serial No.", "Student ID", "Marks"]}
                colKeyMapping={{ "Serial No.": "SN", "Student ID": "ID", Marks: "Marks" }}
              />
              {alertMessage && (
                <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
                  {alertMessage.message}
                </Alert>
              )}
            </div>
            <div className="row mt-3 d-flex flex-row-reverse">
              <div className="col-2">
                <CButton className="me-0" label="SAVE" onClick={handleUploadMarks} />
              </div>
              <div className="col-2">
                <CButton className="me-0" label="UPLOAD" onClick={handleUpdateButtonClick} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TutorEnterMarks;
