import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiDashboardLine, RiMarkPenFill, RiCalendarView, RiCalendarCheckFill, RiUser2Fill } from "react-icons/ri";
import SearchBar from "../../components/SearchBar";
import { Checkbox } from "@mui/material";
import { Alert } from 'react-bootstrap';
import axios from 'axios';
import DropdownTutor from "../../components/DropdownTutor";
import { useNavigate } from 'react-router-dom';
import CButton from "../../components/Button";

const API_BASE_URL = 'http://localhost:3001';

function TutorVerifyMarks() {
  const [isVerifier, setIsVerifier] = useState(false);
  const [modules, setModules] = useState([]);
  const [selectedModule, setSelectedModule] = useState(null);
  const [marksData, setMarksData] = useState([]);
  const [isModulePending, setIsModulePending] = useState(false);
  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const navigate = useNavigate();

  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }

  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView />, label: "View Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  if (isVerifier) {
    menuItems.push({ link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" });
  }

  const checkIfVerifier = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor/check`, { params: { user_id } });
      setIsVerifier(response.data.is_verifier);
    } catch (e) {
      console.log(e.message);
    }
  };

  const fetchModules = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor/getverifiermodules`, { params: { user_id } });
      setModules(response.data);
       console.log(response.data)
      if (response.data.length > 0) {
        setSelectedModule(response.data[0].moduleoffered_id);
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  const fetchMarksData = async (moduleoffered_id) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor/marks/getmarksverifier`, { params: { moduleoffered_id } });
      setMarksData(response.data);
      
      console.log(isModulePending)
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    checkIfVerifier();
    fetchModules();
  }, []);

  useEffect(() => {
    if (selectedModule) {
      fetchMarksData(selectedModule);
    }
  }, [selectedModule, modules]);

  const handleVerifyChange = async (marks_id, checked, staff_id, student_id, marks_obtained) => {
    try {
      await axios.put(`${API_BASE_URL}/tutor/marks/verifymark`, {
        marks_id,
        verified: checked,
        module_verifier: user_id,
        staff_id,
        student_id,
        marks_obtained
      });
      fetchMarksData(selectedModule); // Refresh the data
    } catch (e) {
      console.log(e.message);
    }
  };

  const handleNotify = async () => {
    const uncheckedMarks = marksData.filter(mark => !mark.verified);

    if (uncheckedMarks.length === 0) {
      showAlert('No unchecked marks to notify', 'primary')
      return;
    }

    try {
      await axios.post(`${API_BASE_URL}/tutor/marks/notify`, { uncheckedMarks });
      showAlert('Notification email sent successfully', 'success');
    } catch (e) {
      console.log(e.message);
      showAlert('Error sending notification emails', 'danger')
    }
  };

  const moduleOptions = modules.map(module => ({
    moduleoffered_id: module.moduleoffered_id,
    module_name: module.module_name
  }));

  const columnNames = ["Serial No.", "Student ID", "Marks", "Verify"];
  const listOfMarks = marksData.map((mark, index) => ({
    SN: index + 1,
    ID: mark.student_id,
    Marks: mark.marks_obtained,
    Verify: (
      <Checkbox
        checked={mark.verified}
        onChange={(e) => handleVerifyChange(mark.marks_id, e.target.checked, mark.staff_id, mark.student_id, mark.marks_obtained)}
      />
    )
  }));

  const colKeyMapping = {
    'Serial No.': 'SN',
    'Student ID': 'ID',
    'Marks': 'Marks',
    'Verify': 'Verify',
  };

  
    const handleModuleChange = async (e) => {
      const selectedModuleId = e.target.value;
      setSelectedModule(selectedModuleId);

      
      // Fetch details of the selected module to check its status
      const selectedModuleDetails = modules.find(module => module.moduleoffered_id == selectedModuleId)
      if (selectedModuleDetails && selectedModuleDetails.status === 'Pending') {
        
        setIsModulePending(true);
        console.log("Hello")
      } else {
        setIsModulePending(false);
        console.log("HI")
      }
    
      // Fetch marks data for the selected module
      // await fetchMarksData(selectedModuleId);
    };


  const handleApprove = async () => {
    if (marksData.length === 0) {
      showAlert('No marks data available to approve', 'primary');
      return;
    }

    try {
      const response = await axios.post(`${API_BASE_URL}/coursemanager/modulesoffered/approve`, { selectedModule });
      if (response.data.success) {
        showAlert(response.data.message, 'success');
      } else {
        showAlert(response.data.message, 'danger');
      }
    } catch (e) {
      console.error(e.message);
      showAlert('Error approving module', 'danger');
    }
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Verify Marks</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>

          <div className="row align-items-center mb-3">
            <div className="col-6">
              <SearchBar />
            </div>
            <div className="col-1"></div>
            <div className="col-5 d-flex flex-row-reverse">
              <DropdownTutor
                options={modules.map((module) => ({
                  value: module.moduleoffered_id,
                  label: module.module_name
                }))}
                onChange={handleModuleChange}
                value={selectedModule}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12 main">
              <StickyHeadTable rows={listOfMarks} colNames={columnNames} colKeyMapping={colKeyMapping} />
              {alertMessage && (
                <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
                  {alertMessage.message}
                </Alert>
              )}

              <div className="row text-center mt-3 justify-content-end ">
                <div className="col-2">
                  <CButton className="me-0" label="Notify" onClick={handleNotify} disabled={isModulePending} />
                </div>
                <div className="col-2">
                  <CButton className="me-0" label="Approve" onClick={handleApprove} disabled={isModulePending} />
                </div>
              </div>
              <div className="text-center mt-3" style={{ marginBottom: '20px' }}>
                {/* Add some space here */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TutorVerifyMarks;
