import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle, faCheckCircle, faBook, faCalendarAlt, faGraduationCap } from "@fortawesome/free-solid-svg-icons";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import CButton from "../../components/Button";

const handleRoute = () => {
  window.location.href = "/tutorviewmarks";
};

const CardCarousel = ({ data }) => {
  const [slidesToShow, setSlidesToShow] = useState(4);

  useEffect(() => {
    const updateSlidesToShow = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth < 992) {
        setSlidesToShow(Math.min(data.length, 1));
      } else if (screenWidth >= 992 && screenWidth < 1200) {
        setSlidesToShow(Math.min(data.length, 2));
      } else {
        setSlidesToShow(Math.min(data.length, 4));
      }
    };

    updateSlidesToShow();
    window.addEventListener("resize", updateSlidesToShow);
    return () => {
      window.removeEventListener("resize", updateSlidesToShow);
    };
  }, [data.length]);

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: slidesToShow,
    slidesToScroll: 1
  };

  return (
    <div className="border border-warning rounded px-4 py-1 shadow justify-content-center">
      <Slider {...settings}>
        {data.map((item, index) => (
          <div className="p-1" key={index}>
            <div className="my-3 border p-3 text-start" style={{ height: "400px" }}>
              <h5>{item.module_name}</h5>
              <hr />
              <p>
                <FontAwesomeIcon icon={faBook} /> Program: {item.program_name} {item.year}
              </p>
              <p>
                <FontAwesomeIcon icon={faUserCircle} /> Module Verifier: {item.module_verifier_name}
              </p>
              <p>
                <FontAwesomeIcon icon={faCalendarAlt} /> Semester: {item.semester_id}
              </p>
              <p>
                <FontAwesomeIcon icon={faGraduationCap} /> Active Students: {item.student_count}
              </p>
              <div className="justify-content-center">
                <CButton className="ms-2" label="View" onClick={handleRoute} />
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default CardCarousel;