import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import { RiDashboardLine, RiMarkPenFill, RiCalendarView, RiCalendarCheckFill, RiUser2Fill } from "react-icons/ri"; // Import the required icons
import StudentPersonalDetails from "../../components/StudentPersonalDetails";
import Profile from "../../components/Profile";
import Image from "../../assets/Balav.jpg";

function TutorProfile() {
  // Define the array of menu items
  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView/>, label: "View Marks" },
    // { link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
                <div className="col-6">
                    <h3>Profile</h3>
                </div>
            </div>

            <hr style={{ borderColor: 'blue', borderWidth: '2px' }} />

            <div className="row align-items-center text-center mb-3 justify-content-center"> {/* Body/main content */}
                <div className="col-6 p-1">
                    <Profile
                        imageUrl={Image}
                        name="Balav Sharma"
                    />
                </div>
            </div>
            <div className="row align-items-center text-center mb-3 justify-content-center"> {/* Body/main content */}
                <div className="col-4 border rounded-5 border-success p-3">
                    <StudentPersonalDetails
                        name="Balav Sharma"
                        email="12200011.gcit@rub.edu.bt"
                        contactNo="1234567890"
                    />
                </div>
            </div>
        </div>
        </div>
    </div>
  );
}

export default TutorProfile;
