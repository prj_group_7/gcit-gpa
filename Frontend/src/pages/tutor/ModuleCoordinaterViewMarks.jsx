import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarCheckFill, RiCalendarScheduleLine, RiCalendarView, RiDashboardLine, RiMarkPenFill, RiSoundModuleFill, RiTeamLine, RiUser2Fill, RiUserFill, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";
import TutorPieChart from "../president/TutorPieChart";

function ModuleCoordinaterViewMarks() {
  // Define the array of menu items
  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView/>, label: "View Marks" },
    { link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];
  const moduleOptions = [
    "PLT 302",
    "MAT 303",
    "MLT 304",
    "PRJ 303",
    "PRJ 305",
  ];

 // Define column names
 const columnNames = ["SN", "ID", "Marks"];
  
 // Sample data (replace with your own data structure)
 const listOfMarks = [
   { SN: 1, ID: '12200001', Marks: '71'},
   { SN: 2, ID: '12200002', Marks: '74'},
   { SN: 3, ID: '12200003', Marks: '80'},
   { SN: 4, ID: '12200004', Marks: '82' },
  
   // Add more rows as needed
 ];

 const colKeyMapping = {
   'SN':'SN',
   'ID': 'ID',
   'Marks': 'Marks',
 };

  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3>View Marks</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
                <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
            <div className="col-2">
              <input type="text" defaultValue="Edit marks" style={{ width: "140px", textAlign: "center" }} />
            </div>
            </div>
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
                <StickyHeadTable rows={listOfMarks} colNames={columnNames} colKeyMapping={colKeyMapping}/>
            </div>

            </div>
        </div>
        </div>
    </div>
  );
}

export default ModuleCoordinaterViewMarks;
