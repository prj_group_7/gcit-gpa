import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiDashboardLine, RiMarkPenFill, RiCalendarView, RiUser2Fill, RiCalendarCheckFill } from "react-icons/ri";
import DropdownTutor from "../../components/DropdownTutor";
import SearchBar from "../../components/SearchBar";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function TutorViewMarks() {
    const [isVerifier, setIsVerifier] = useState(false);
    const [modules, setModules] = useState([]);
    const [selectedModule, setSelectedModule] = useState(null);
    const [marks, setMarks] = useState([]);
    const navigate = useNavigate();
    const getUserData = sessionStorage.getItem('userdata');
    let user_id = 0;
    let userdata = null;

    if (!getUserData) {
        navigate('/');
    } else {
        userdata = JSON.parse(getUserData);
        user_id = userdata.user_id;
    }

    const menuItems = [
        { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
        { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
        { link: "/tutorviewmarks", icon: <RiCalendarView />, label: "View Marks" },
        // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
    ];

    if (isVerifier) {
        menuItems.push({ link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" });
    }

    const checkIfVerifier = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/tutor/check`, {
                params: { user_id },
            });
            setIsVerifier(response.data.is_verifier);
        } catch (e) {
            console.log(e.message);
        }
    };

    const fetchModules = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/tutor/getmodules`, {
                params: { user_id },
            });
            setModules(response.data);
            if (response.data.length > 0) {
                const firstModule = response.data[0];
                setSelectedModule(firstModule.moduleoffered_id);
                fetchMarks(firstModule.moduleoffered_id); // Fetch marks for the first module
            }
        } catch (e) {
            console.log(e.message);
        }
    };

    const fetchMarks = async (moduleoffered_id) => {
        try {
            const response = await axios.get(`${API_BASE_URL}/tutor/marks/getmarks`, {
                params: { moduleoffered_id },
            });
            setMarks(response.data);
        } catch (e) {
            console.log(e.message);
        }
    };

    useEffect(() => {
        checkIfVerifier();
        fetchModules();
    }, []);

    useEffect(() => {
        if (selectedModule !== null) {
            fetchMarks(selectedModule);
        }
    }, [selectedModule]);

    const handleModuleChange = (e) => {
        const selectedModuleId = e.target.value;
        setSelectedModule(selectedModuleId);
    };

    const columnNames = ["Serial No.", "Student ID", "Marks"];
    const colKeyMapping = { 'Serial No.': 'SN', 'Student ID': 'student_id', 'Marks': 'marks_obtained' };

    const rows = marks.map((mark, index) => ({ ...mark, SN: index + 1 }));

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-2">
                    <Sidebar menuItems={menuItems} />
                </div>
                <div className="col-10 p-5">
                    <div className="row mb-3">
                        <div className="col-6">
                            <h3 style={{color: 'rgba(246,130,24,0.8)'}}>View Marks</h3>
                        </div>
                    </div>
                    <div className="row align-items-center mb-3">
                        <div className="col-6">
                            <SearchBar />
                        </div>
                        <div className="col-1"></div>
                        <div className="col-5 d-flex flex-row-reverse">
                            <DropdownTutor
                                options={modules.map((module) => ({
                                    value: module.moduleoffered_id,
                                    label: module.module_name
                                }))}
                                onChange={handleModuleChange}
                                value={selectedModule}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 main">
                            <StickyHeadTable
                                rows={rows}
                                colNames={columnNames}
                                colKeyMapping={colKeyMapping}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TutorViewMarks;
