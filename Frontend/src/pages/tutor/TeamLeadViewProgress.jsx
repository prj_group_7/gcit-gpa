import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarCheckFill, RiCalendarScheduleLine, RiCalendarView, RiDashboardLine, RiMarkPenFill, RiSoundModuleFill, RiTeamLine, RiUser2Fill, RiUserFill, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";

function TeamLeadVeiwProgress() {
  // Define the array of menu items
  const menuItems = [
    { link: "/teamleaddashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorresults", icon: <RiCalendarView/>, label: "View Result" },
    { link: "/teamleadviewprogress", icon: <RiMarkPenFill />, label: "View Progress" },
    { link: "/tutormodulerepeat", icon: <RiCalendarCheckFill />, label: "MR List" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];


  const moduleOptions = [
    "PLT 302",
    "MAT 303",
    "MLT 304",
    "PRJ 303",
    "PRJ 305",
  ];
  const yearOptions = [
    "Year 1",
    "Year 2",
    "Year 3",
    "Year 4",
  ];

  const semesterOptions = [
    "Semester 1",
    "Semester 2",
    "Semester 3",
    "Semester 4",
  ];

  // Define column names
  const columnNames = ["SN","ID", "PLT101", "PLT101","PLT101","PLT101","PLT101","GPA"];

  // Sample data (replace with your own data structure)
 const listOfMarks = [
  { SN: 1, ID: '12200001', PLT101: '3',PLT101: '3.5',PLT101: '2.5',PLT101: '4',PLT101: '3',GPA: '3'},
  { SN: 2, ID: '12200002', PLT101: '3',PLT101: '3.5',PLT101: '2.5',PLT101: '4',PLT101: '3',GPA: '3'},
  { SN: 3, ID: '12200003', PLT101: '3',PLT101: '3.5',PLT101: '2.5',PLT101: '4',PLT101: '3',GPA: '3'},
  { SN: 4, ID: '12200004', PLT101: '3',PLT101: '3.5',PLT101: '2.5',PLT101: '4',PLT101: '3',GPA: '3'},
 
  // Add more rows as needed
];

const colKeyMapping = {
  'SN':'SN',
  'ID': 'ID',
  'PLT101':'PLT101',
  'PLT101':'PLT101',
  'PLT101':'PLT101',
  'PLT101':'PLT101',
  'PLT101':'PLT101',
  'GPA': 'GPA',
};


  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3>View Progress</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
                <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2">
                <Dropdown options={yearOptions} dropdown_prompt="Select Year"/>
            </div>
            <div className="col-2">
                <Dropdown options={semesterOptions} dropdown_prompt="Select Semester"/>
            </div>
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
                <StickyHeadTable rows={listOfMarks} colNames={columnNames} colKeyMapping={colKeyMapping}/>
            </div>

            </div>
        </div>
        </div>
    </div>
  );
}

export default TeamLeadVeiwProgress;