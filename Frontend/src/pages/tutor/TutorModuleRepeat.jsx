import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarCheckFill, RiCalendarScheduleLine, RiCalendarView, RiDashboardLine, RiMarkPenFill, RiSoundModuleFill, RiTeamLine, RiUser2Fill, RiUserFill, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";
import { Email, EmailOutlined } from "@mui/icons-material";

function TutorModuleRepeat() {
  // Define the array of menu items
  const menuItems = [
    { link: "/teamleaddashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorresults", icon: <RiCalendarView/>, label: "View Result" },
    { link: "/teamleadviewprogress", icon: <RiMarkPenFill />, label: "View Progress" },
    { link: "/tutormodulerepeat", icon: <RiCalendarCheckFill />, label: "MR List" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];


  const moduleOptions = [
    "PLT 302",
    "MAT 303",
    "MLT 304",
    "PRJ 303",
    "PRJ 305",
  ];



  // Define column names
  const columnNames = ["SN","ID", "Email", "Semester"];
  // Define column names

  
 // Sample data (replace with your own data structure)
 const listOfMarks = [
   { SN: 1, ID: '12200001', Email: '12200001.gcit@rub.edu.bt',Semester: '1',},
   { SN: 2, ID: '12200002', Email: '12200002.gcit@rub.edu.bt',Semester: '1',},
   { SN: 3, ID: '12200003', Email: '12200003.gcit@rub.edu.bt',Semester: '1',},
   { SN: 4, ID: '12200004', Email: '12200001.gcit@rub.edu.bt',Semester: '1', },
  
   // Add more rows as needed
 ];

 const colKeyMapping = {
   'SN':'SN',
   'ID': 'ID',
   'Email':'Email',
   'Semester':'Semester',
 };

  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3>Module Repeat Students</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
                <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
                <Dropdown options={moduleOptions} dropdown_prompt="Select Module"/>
            </div>
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
                <StickyHeadTable rows={listOfMarks} colNames={columnNames} colKeyMapping={colKeyMapping}/>
            </div>

            </div>
        </div>
        </div>
    </div>
  );
}

export default TutorModuleRepeat;
