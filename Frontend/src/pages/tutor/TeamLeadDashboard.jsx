import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel, faChartLine } from "@fortawesome/free-solid-svg-icons"; // Import the line graph icon
import StudentPieChart from "../president/StudentPieChart";
import TeamLeadPieChart from "../president/TeamLeadPieChart";
import TutorPieChart from "../president/TutorPieChart";
import CardCarousel from "../president/Components/Card";
import LineGraph from "../president/Components/LineGraph";
import Sidebar from "../../components/Sidebar";
import { RiDashboardLine, RiCalendarView, RiMarkPenFill, RiCalendarCheckFill, RiUser2Fill } from "react-icons/ri"; // Import the required icons
import { Line } from "recharts";
import PresidentDashboard from "../president/PresidentDashboard";

export default function TeamLeadDashboard() {
     // Define the array of menu items
  const menuItems = [
    { link: "/teamleaddashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorresults", icon: <RiCalendarView/>, label: "View Result" },
    { link: "/teamleadviewprogress", icon: <RiMarkPenFill />, label: "View Progress" },
    { link: "/tutormodulerepeat", icon: <RiCalendarCheckFill />, label: "MR List" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  
  return (
    <div className="container-fluid">
        <div className="row flex-nowrap">
            <div className="col-2">
                <Sidebar menuItems={menuItems} />
            </div>
            <div className="col-10">
                <PresidentDashboard />
            </div>
        </div>
    </div>
  );
}