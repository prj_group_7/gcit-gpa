import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Sidebar from "../../components/Sidebar";
import CardCarousel from "./ModuleCard";
import axios from 'axios';
import { RiAccountBoxFill, RiCalendarCheckFill, RiCalendarView, RiDashboardLine, RiMarkPenFill, RiProfileLine, RiUser2Fill } from "react-icons/ri"; // Import the required icons

const API_BASE_URL = 'http://localhost:3001';

function TutorDashboard() {
  const navigate = useNavigate();
  const [modulesData, setModulesData] = useState([]);
  const [isVerifier, setIsVerifier] = useState(false);
  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }
  console.log(user_id);

  const menuItems = [
    { link: "/tutordashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/tutorentermarks", icon: <RiMarkPenFill />, label: "Enter Marks" },
    { link: "/tutorviewmarks", icon: <RiCalendarView/>, label: "View Marks" },
    // { link: "/tutorprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  if (isVerifier) {
    menuItems.push({ link: "/tutorverifymarks", icon: <RiCalendarCheckFill />, label: "Verify Marks" });
    }
    const checkIfVerifier = async () => {
    try {
        const response = await axios.get(`${API_BASE_URL}/tutor/check`, {
        params: {
            user_id,
        }
        });
        setIsVerifier(response.data.is_verifier);
    } catch (e) {
        console.log(e.message);
    }
    }

  useEffect(() => {
    fetchData();
    checkIfVerifier();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/tutor`, {
        params: {
          user_id,
        }
      });
      console.log(response.data);
      setModulesData(response.data);
    } catch (e) {
      console.log(e.message);
    }
  }


  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="ms-3 mb-5"><h3 className=' mt-1' style={{ color: '#F68218' }}>Dashboard</h3></div>
          <div className=""><CardCarousel data={modulesData}></CardCarousel></div>
        </div>
      </div>
    </div>
  );
}

export default TutorDashboard;
