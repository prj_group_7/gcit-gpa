import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import LineGraph from "../../components/LineGraph";
import {
  RiDashboardLine,
  RiProfileLine,
  RiProgress5Line,
} from "react-icons/ri";
import StudentPieChart from "./StudentPieChart";
import StudentSummaryDisplay from "../../components/StudentSummaryDisplay";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import './StudentDashboard.css';

const API_BASE_URL = 'http://localhost:3001';

function StudentDashboard() {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }

  const [dashboardData, setDashboardData] = useState({ completedModules: 0, repeatedModules: 0, cgpa: 0 });
  const [gpaData, setGPAData] = useState([]);
  const [cgpaData, setCGPAData] = useState([]);
  const [xLabels, setXLabels] = useState([]);

  useEffect(() => {
    fetchDashboardData();
    fetchStudentData();
  }, []);

  const fetchDashboardData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/students/studentdashboard/${user_id}`);
      const data = response.data;
      setDashboardData(data);
      console.log(data);
    } catch (error) {
      console.error("Error fetching dashboard data:", error);
    }
  };

  const fetchStudentData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/students/gpa/${user_id}`);
      const data = response.data;
      console.log(data);

      // Extract GPA and CGPA data
      const gpaArray = data.map(item => item.gpa);
      const cgpaArray = data.map(item => parseFloat(item.cgpa));
      const labels = data.map(item => `Sem ${item.semester}`);

      setGPAData(gpaArray);
      setCGPAData(cgpaArray);
      setXLabels(labels);
    } catch (error) {
      console.error("Error fetching GPA data:", error);
    }
  };

  const menuItems = [
    { link: "/studentdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/studentresult", icon: <RiProfileLine />, label: "Result" },
    { link: "/studentgpa", icon: <RiProgress5Line />, label: "GPA" },
  ];

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2 sidebar-col">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 main-col">
          <div className="row">
            <div className="col-12 p-3 ps-5">
              <h3 className="dashboard-title">Dashboard</h3>
            </div>
          </div>
          <div className="row align-items-center text-center justify-content-center">
            <div className="col-lg-6 col-md-12 col-sm-12 rounded-4">
              <StudentPieChart completed={dashboardData.completedModules} inprogress={dashboardData.repeatedModules} />
            </div>
            <div className="col-lg-2 col-md-4 col-sm-12 rounded-4 border-success">
              <StudentSummaryDisplay label="Module Completed" count={dashboardData.completedModules} />
            </div>
            <div className="col-lg-2 col-md-4 col-sm-12 rounded-4 border-success">
              <StudentSummaryDisplay label="Module Repeat (MR)" count={dashboardData.repeatedModules} />
            </div>
            <div className="col-lg-2 col-md-4 col-sm-12 rounded-4 border-success">
              <StudentSummaryDisplay label="Cumulative GPA (CGPA)" count={dashboardData.cgpa} />
            </div>
          </div>
          <div className="row align-items-center text-center pt-3">
            <div className="col-12">
              <LineGraph 
                data={[gpaData, cgpaData]}
                xLabels={xLabels} 
                yLimit={4.0} 
                xAxisLabel="Semester" 
                yAxisLabel="Grade"
                legendLabels={['GPA', 'CGPA']} 
                title="CGPA Statistics of each program" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default StudentDashboard;
