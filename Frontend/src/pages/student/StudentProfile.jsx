import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAccountBoxFill, RiDashboardLine, RiMarkPenFill, RiMarkupFill, RiProfileLine, RiProgress1Fill, RiProgress3Fill, RiProgress5Line, RiProgress6Fill, RiProgress6Line, RiTeamLine, RiUser2Fill, RiUser3Line, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import { MarkAsUnread, MarkunreadSharp, ResetTvOutlined } from "@mui/icons-material";
import StudentPersonalDetails from "../../components/StudentPersonalDetails";
import Profile from "../../components/Profile";
import Image from "../../assets/Balav.jpg";

function StudentProfile() {
  // Define the array of menu items
  const menuItems = [
    { link: "/studentdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/studentresult", icon: <RiProfileLine />, label: "Result" },
    { link: "/studentgpa", icon: <RiProgress5Line />, label: "GPA" },
    { link: "/studentprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
                <div className="col-6">
                    <h3>Profile</h3>
                </div>
                <div className="col-2"></div>
                <div className="col-2"></div>
                <div className="col-2"></div>
            </div>

            <hr style={{ borderColor: 'blue', borderWidth: '2px' }} />

            <div className="row align-items-center mb-5"> {/* Second-level header */}
                <div className="col-6"></div>
                <div className="col-2"></div>
                <div className="col-2"></div>
                <div className="col-2"></div>
            </div>

            <div className="row align-items-center text-center mb-3 justify-content-center"> {/* Body/main content */}
                <div className="col-4 border rounded-5 border-success p-3">
                    <StudentPersonalDetails 
                    name = "Balav Sharma"
                    programme = "BSc in Computer Science"
                    email = "12200011.gcit@rub.edu.bt"
                    contactNo = "1234567890"
                    dateOfBirth = "01-01-2000"
                    gender = "Male"
                    emergencyContactNo = "1234567890"
                    address = "Samtse, Bhutan"
                    />
                </div>
                <div className="col-6 p-1">
                    <Profile
                    imageUrl={Image}
                    name = "Balav Sharma"
                    />
                </div>
            </div>
        </div>
        </div>
    </div>
  );
}

export default StudentProfile;
