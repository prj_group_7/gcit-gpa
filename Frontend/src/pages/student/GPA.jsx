import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiDashboardLine, RiProfileLine, RiProgress5Line, RiUser2Fill } from "react-icons/ri"; // Import the required icons
import { useNavigate } from "react-router-dom";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function GPA() {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }

  // Define the array of menu items
  const menuItems = [
    { link: "/studentdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/studentresult", icon: <RiProfileLine />, label: "Result" },
    { link: "/studentgpa", icon: <RiProgress5Line />, label: "GPA" },
    // { link: "/studentprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  // Define column names
  const columnNames = ["Semester", "GPA", "Grade", "Cumulative GPA"];

  // Define initial state for GPA data
  const [gpadata, setGPAData] = useState([]);

  useEffect(() => {
    // Fetch GPA data from the API
    fetchGPAData();
  }, []); // Empty dependency array ensures useEffect runs only once on component mount

  // Function to fetch GPA data from the API
  const fetchGPAData = async () => {
    try {
      // Fetch data from the API endpoint
      const response = await axios.get(`${API_BASE_URL}/students/gpa/${user_id}`);

      // Extract GPA data from the response
      const data = response.data;
      console.log(data)

      // Update state with fetched GPA data
      setGPAData(data);
    } catch (error) {
      console.error("Error fetching GPA data:", error);
    }
  };

  const colKeyMapping = {
    "Semester": "semester",
    "GPA": "gpa",
    "Grade": "grade",
    "Cumulative GPA": "cgpa",
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3 style={{ color: 'rgba(246,130,24,0.8)' }}>Grade Point Average</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row"> {/* Body/main content */}
            <div className="col-12 ">
              <StickyHeadTable rows={gpadata} colNames={columnNames} colKeyMapping={colKeyMapping} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default GPA;
