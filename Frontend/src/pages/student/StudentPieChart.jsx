import React from 'react';
import { Pie } from 'react-chartjs-2';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import './chart.css'

ChartJS.register(ArcElement, Tooltip, Legend);

const StudentPieChart = (completed, inprogress) => {
  // console.log('HELLO THERE', completed)
  const data = {
    labels: ['Completed', 'In Progress'],
    datasets: [
      {
        data: [completed.completed, inprogress.inprogress],
        backgroundColor: ['#4caf50', '#ff9800'],
        borderColor: ['#ffffff', '#ffffff'],
        borderWidth: 2,
        hoverOffset: 10,
      },
    ],
  };
  console.log(data)

  const options = {
    plugins: {
      legend: {
        position: 'right',
        labels: {
          padding: 10,
          boxWidth: 10,
          usePointStyle: true,
        },
      },
    },
    elements: {
      arc: {
        borderWidth: 0,
      },
    },
    maintainAspectRatio: false, // Disable aspect ratio to control with CSS
    responsive: true,
  };

  return (
    <div className="chart-container">
      <Pie data={data} options={options} />
    </div>
  );
};

export default StudentPieChart;
