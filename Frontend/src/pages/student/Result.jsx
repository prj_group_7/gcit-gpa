import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import DropdownTutor from "../../components/DropdownTutor";
import { RiDashboardLine, RiProfileLine, RiProgress5Line, RiUser2Fill } from "react-icons/ri"; 
import { useNavigate } from "react-router-dom";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function Result() {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let user_id = 0;
  let userdata = null;

  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    user_id = userdata.user_id;
  }


  // Define the array of menu items
  const menuItems = [
    { link: "/studentdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/studentresult", icon: <RiProfileLine />, label: "Result" },
    { link: "/studentgpa", icon: <RiProgress5Line />, label: "GPA" },
    // { link: "/studentprofile", icon: <RiUser2Fill />, label: "Profile" },
  ];

  // Update semester options to include both name and value
  const semesterOptions = [
    { label: "Semester 1", value: 1 },
    { label: "Semester 2", value: 2 },
    { label: "Semester 3", value: 3 },
    { label: "Semester 4", value: 4 },
    { label: "Semester 5", value: 5 },
    { label: "Semester 6", value: 6 },
    { label: "Semester 7", value: 7 },
    { label: "Semester 8", value: 8 },
  ];

  const [selectedSemester, setSelectedSemester] = useState(1);
  const [resultData, setResultData] = useState([]);

  useEffect(() => {
    const fetchMarks = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/students/marks/${user_id}/${selectedSemester}`);
        setResultData(response.data.data);
      } catch (error) {
        console.error('Error fetching student marks:', error);
      }
    };

    fetchMarks();
  }, [user_id, selectedSemester]);

  const columnNames = ["Module Code", "Module Name", "Mark", "Grade Point", "Grade", "Remark"];

  const colKeyMapping = {
    "Module Code": "module_code",
    "Module Name": "module_name",
    "Mark": "marks_obtained",
    "Grade Point": "gradepoint",
    "Grade": "grade",
    "Remark": "remark",
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Result</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-4 d-flex flex-row-reverse">
              <DropdownTutor 
                options={semesterOptions} 
                dropdown_prompt="Select Semester"
                onChange={(e) => setSelectedSemester(parseInt(e.target.value))} 
              />
            </div>
          </div>
          
          <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row"> {/* Body/main content */}
            <div className="col-12">
              <StickyHeadTable rows={resultData} colNames={columnNames} colKeyMapping={colKeyMapping}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Result;
