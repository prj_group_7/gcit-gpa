import React from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend } from 'chart.js';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const StudentLineChart = () => {
  const data = {
    labels: ['Sem 1', 'Sem 2', 'Sem 3', 'Sem 4', 'Sem 5', 'Sem 6', 'Sem 7', 'Sem 8'],
    datasets: [
      {
        label: 'GPA',
        data: [3.2, 2.8, 3.6, 3.1, 2.9, 3.5, 3.0, 3.4],
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1,
      },
      {
        label: 'CGPA',
        data: [2.8, 2.2, 3.1, 2.9, 2.5, 3.2, 2.7, 3.0],
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255, 99, 132, 1)',
        borderWidth: 1,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      x: {
        title: {
          display: true,
          text: 'Semester',
        },
      },
      y: {
        title: {
          display: true,
          text: 'Grade',
        },
        min: 0,
        max: 4,
        ticks: {
          stepSize: 1,
        },
      },
    },
    plugins: {
      title: {
        display: true,
        text: 'CGPA Statistics of each program',
      },
    },
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-7">
          <div className="card border-0">
            <div className="card-body p-0">
              <Line data={data} options={options} style={{ height: '280px' }} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StudentLineChart;
