import React, { useState } from "react";
import { Link } from "react-router-dom";
import { RiLock2Line } from "react-icons/ri";
import axios from 'axios';
import logo from "../assets/logo.svg";
import background from "../assets/Background.png";
import "./style.css";
import { Alert } from "react-bootstrap";
const API_BASE_URL = 'http://localhost:3001'


function ForgotPassword() {
  const [email, setEmail] = useState('');
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/users/forgot-password`, { email });
      console.log("huh")
      if (response.data.success) {
        showAlert(response.data.message, 'success');
      }
    } catch (error) {
      console.error('Error:', error);
      if (error.response.data?.message) {
        showAlert(error.response.data.message, 'danger'); // Display error message if login fails
      } else {
        showAlert('Something went wrong')
      }
    }
  }

  return (
    <div
      className="container-fluid p-4 background-radial-gradient overflow-hidden"
      style={{
        background: `url(${background})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        height: "100vh",
      }}
    >
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
      <div className="row">
        <div className="col-md-6 d-flex flex-column justify-content-center align-items-center text-center text-md-start mt-5">
          <img src={logo} alt="logo" style={{ width: 400, height: 400 }} />
        </div>
        <div
          className="col-md-4 position-relative mt-5"
          style={{
            borderRadius: "30px",
            borderColor: "rgba(246,130,24,0.7)",
            border: "2px solid",
          }}
        >
          <div
            id="radius-shape-1"
            className="position-absolute rounded-circle shadow-5-strong"
          ></div>
          <div
            id="radius-shape-2"
            className="position-absolute shadow-5-strong"
          ></div>
          <div className="my-5 bg-glass">
            <div className="p-5">
              {/* Lock icon */}
              <div className="mb-3 d-flex justify-content-center">
                <RiLock2Line
                  style={{
                    fontSize: "3rem",
                    color: "rgba(255, 255, 255, 0.60)",
                  }}
                />
              </div>
              <h2
                className="mb-3 text-center"
                style={{ color: "rgba(246,130,24,0.67)" }}
              >
                Forgot Password
              </h2>
              <h6
                className="mt-2 mb-4 text-center"
                style={{
                  color: "rgba(246,130,24,0.67)",
                  fontSize: "19px",
                  fontWeight: "normal",
                }}
              >
                Enter your email and we will send you a link to reset your
                password
              </h6>
              <input
                className="form-control mb-5 form-label placeholder-color"
                placeholder="enrollment.gcit@rub.edu.bt"
                type="email"
                onChange={(e) => setEmail(e.target.value)}
                style={{
                  color: "white",
                  borderBottom: "2px solid rgba(246,130,24,0.67)",
                  borderTop: "none",
                  borderLeft: "none",
                  borderRight: "none",
                  backgroundColor: "rgba(255,255,255,0)",
                  transition: "border-bottom 0.3s, box-shadow-0 0.1s",
                  borderRadius: "0px",
                }}
              />
              <button
                className="btn btn-primary w-100 mb-4"
                type="button"
                style={{
                  backgroundColor: "rgba(255, 255, 255, 0)",
                  border: "2px solid",
                  borderColor: "rgba(246,130,24,0.67)",
                  color: "rgba(246,130,24,0.67)",
                  borderRadius: "10px",
                  transition: "background-color 0.3s, color 0.3s",
                }}
                onMouseEnter={(e) => {
                  e.target.style.backgroundColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.borderColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.color = "white";
                }}
                onMouseLeave={(e) => {
                  e.target.style.backgroundColor = "rgba(255, 255, 255, 0)";
                  e.target.style.borderColor = "rgba(246, 130, 24, 0.67)";
                  e.target.style.color = "rgba(246, 130, 24, 0.67)";
                }}
                onClick={handleSubmit}
              >
                Submit
              </button>
              <Link
                to="/"
                style={{
                  color: "rgba(255, 255, 255, 0.7)",
                  textDecoration: "none",
                }}
              >
                <h6
                  style={{
                    color: "rgba(255, 255, 255, 0.7)",
                    textAlign: "center",
                    fontWeight: "normal",
                  }}
                >
                  Back To Login
                </h6>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
