import React,{useState,useEffect} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel, faChartLine } from "@fortawesome/free-solid-svg-icons"; // Import the line graph icon
import StudentPieChart from "./StudentPieChart";
import TeamLeadPieChart from "./TeamLeadPieChart";
import TutorPieChart from "./TutorPieChart";
import CardCarousel from "./Components/Card";
import LineGraph from "./Components/LineGraph";
import { Line } from "recharts";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

export default function PresidentDashboard() {
  const [programsdata, setProgramsData] = useState([]);

  useEffect(()=>{
    fetchData()
  },[])
  const fetchData = async()=>{
    try{
      const response = await axios.get(`${API_BASE_URL}/coursemanager/programoffered/getprogramoffereddetails`);
      setProgramsData(response.data.data);
    }catch(e){
      console.log(e);
    }
  }

  const aiData = [3.0, 1.0, 2.5, 1.5, 4.0, 3.5, 2.5, 3.5];
  const fullStackData = [1.5, 3.0, 0.0, 4.0, 2.5, 1.0, 3.0, 1.5];
  const blockchainData = [2.0, 2.5, 3.0, 3.5, 4.0, 2.0, 0.0, 2.0];

  const xLabels = [
    "2021",
    "2022",
    "2023",
    "2024",
    "2025",
    "2026",
    "2027",
    "2028",
  ];
  return (
    <div className="mt-2">
      <div className="container" style={{ width: "80%" }}>
        <div className="px-5 py-4 row">
          <div className="col-md-4 col-sm-12">
            <div className="border border-warning" style={{ borderRadius: "20px" }}>
              <StudentPieChart />
            </div>
          </div>
          <div className="col-md-4 col-sm-12 mb-3">
            <div className="border border-warning" style={{ borderRadius: "20px" }}>
              <TutorPieChart />
            </div>
          </div>
          <div className="col-md-4 col-sm-12 mb-3">
            <div className="border border-warning" style={{ borderRadius: "20px" }}>
              <TeamLeadPieChart />
            </div>
          </div>
        </div>
      </div>
      <div className="px-5 text-start d-flex align-items-center py-2">
        <FontAwesomeIcon icon={faFileExcel} style={{ color: "green", marginRight: "5px", height: "25px", width: '25px' }} />
        <h5 style={{color: 'rgba(246,130,24,0.8)'}}>Programs</h5>
      </div>
      <div className="px-4 mt-2">
        <CardCarousel data={programsdata} />
      </div>
      <div>
        <div className="px-5 text-start d-flex align-items-center py-4">
            <FontAwesomeIcon icon={faChartLine} style={{ color: "blue", marginRight: "5px",height:'25px',width:'25px' }} />
            <h5 className="ms-3" style={{color: 'rgba(246,130,24,0.8)'}}>
            Statistics CGPA For Each Program.
            </h5>
        </div>
        <div className="px-4 py-4">
          <div className="border border-warning shadow">
          <LineGraph
                        data={[aiData, fullStackData, blockchainData]}
                        xLabels={xLabels}
                        yLimit={4.0}
                        yAxisLabel="Grade"
                        legendLabels={["AI", "Full Stack", "Blockchain"]}
                      />
          </div>
        </div>
      </div>
    </div>
  );
}
