import React from "react";
import { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarScheduleLine, RiDashboardLine, RiSoundModuleFill, RiTeamLine, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";

import axios from "axios";

const API_BASE_URL = 'http://localhost:3001';

function ModuleOverview() {
  const [modules,setModules] = useState([])
  
  useEffect(()=>{
    const getmodules = async ()=>{
      const response= await axios.get(`${API_BASE_URL}/coursemanager/modules`)
      setModules(response.data.data);
      console.log(modules)

    }
    getmodules()
  },[])

  // Define the array of menu items
  const menuItems = [
    { link: "/president_dashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/facultylist", icon: <RiTeamLine />, label: "User" },
    { link: "/moduleoverview", icon: <RiCalendarScheduleLine />, label: "Modules" },
  ];

  // Define column names
  const columnNames = ["SL.NO", "Name", "Code"];

  const colKeyMapping ={
    "SL.NO": "module_id",
    "Name": "module_name",
    "Code": "module_code",

  }
  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Module Overview</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
                <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main ">
                <StickyHeadTable rows={modules} colNames={columnNames} colKeyMapping={colKeyMapping} />
            </div>

            </div>
        </div>
        </div>
    </div>
  );
}

export default ModuleOverview;
