import * as React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

function LineGraph({ data, xLabels, yLimit, yAxisLabel, title, legendLabels }) {
  // Combine xLabels with data to create chart data
  const chartData = xLabels.map((label, index) => ({
    name: label,
    ai: data[0][index],
    fullStack: data[1][index],
    blockchain: data[2][index],
  }));

  return (
    <div style={{ width: '100%', maxWidth: '1200px', margin: '0 auto', height: '300px' }}>
      {title && <h5 style={{ textAlign: 'center', marginBottom: '6px', color: 'rgba(246,130,24,0.8)' }}>{title}</h5>}
      <ResponsiveContainer>
        <LineChart data={chartData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis 
            label={{ value: yAxisLabel, angle: -90, position: 'insideLeft' }} 
            domain={[0, yLimit]} 
            tickFormatter={value => value.toFixed(1)}
          />
          <Tooltip />
          <Legend verticalAlign="top" align="right" height={36} margin={{ bottom: 30 }} />
          <Line type="monotone" dataKey="ai" stroke="#8884d8" name={legendLabels[0]} activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="fullStack" stroke="#82ca9d" name={legendLabels[1]} />
          <Line type="monotone" dataKey="blockchain" stroke="#ffc658" name={legendLabels[2]} />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}

export default LineGraph;
