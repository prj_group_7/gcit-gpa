import React from 'react';
import { PieChart, Pie, Cell, Tooltip, Legend } from 'recharts';

const data = [
  { name: 'Male', value: 50 },
  { name: 'Female', value: 50 },
];

const COLORS = ['#74B444', '#F68218'];

const PieChartComponent = ({data}) => {
  return (
    <PieChart width={100} height={100}>
      <Pie
        dataKey="value"
        isAnimationActive={false}
        data={data}
        startAngle={90} // Rotate the pie chart by 180 degrees
        endAngle={540} 

        outerRadius={40}
        fill="#8884d8"
        // label
      >
        {data.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
      <Tooltip />
      {/* <Legend /> */}
    </PieChart>
  );
};

export default PieChartComponent;
