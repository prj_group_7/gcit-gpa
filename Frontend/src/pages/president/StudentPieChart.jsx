import * as React from 'react';
import PieChartComponent from './Components/PieChart'

export default function StudentPieChart() {
  const studentData = [
    { name: 'Male', value: 60 },
    { name: 'Female', value: 40 },
  ];

  // Calculate male and female percentages
  const malePercentage = ((studentData[0].value / (studentData[0].value + studentData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');
  const femalePercentage = ((studentData[1].value / (studentData[0].value + studentData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');

  return (
    <div className=''>
      <div className=''>
        <div className=' d-flex align-items-center justify-content-evenly' >
          <div className=''>
              <PieChartComponent data={studentData} />
          </div>
          <div className='text-center' style={{ width: 300 }}>
          <h4>Student</h4>
          <div>
            <span style={{ fontSize: '12px' }}>Male: ({malePercentage}%)</span>
            <span className='ms-2' style={{ fontSize: '12px' }}>Female:({femalePercentage}%)</span>
          </div>
        </div>
        </div>
      </div>
    </div>
  );
}
