import * as React from 'react';
import PieChartComponent from './Components/PieChart'

export default function FacultyPieChart() {
  const facultyData = [
    { name: 'Male', value: 70 },
    { name: 'Female', value: 30 },
  ];

  // Calculate male and female percentages
  const malePercentage = ((facultyData[0].value / (facultyData[0].value + facultyData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');
  const femalePercentage = ((facultyData[1].value / (facultyData[0].value + facultyData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');

  return (
    <div className=''>
      <div className='  d-flex  align-items-center justify-content-evenly' >
        <div className=''>
            <PieChartComponent data={facultyData} />
        </div>
        <div className=' text-center'>
          <h4>Faculty</h4>
          <div>
            <span>Male: ({malePercentage}%)</span> 

            <span className='ms-2'>Female:({femalePercentage}%)</span>
          </div>
        </div>
      </div>
    </div>
  );
}
