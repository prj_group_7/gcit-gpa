import * as React from 'react';
import PieChartComponent from './Components/PieChart'

export default function TeamLeadPieChart() {
  const teamLeadData = [ 
    { name: 'Male', value: 30 },
    { name: 'Female', value: 70 },
  ];

  // Calculate male and female percentages
  const malePercentage = ((teamLeadData[0].value / (teamLeadData[0].value + teamLeadData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');
  const femalePercentage = ((teamLeadData[1].value / (teamLeadData[0].value + teamLeadData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');

  return (
    <div className=''>
      <div className='  d-flex  align-items-center justify-content-evenly'>
        <div className=''>
            <PieChartComponent data={teamLeadData} />
        </div>
        <div className='text-center' style={{ width: 300 }}>
          <h4>Team Lead</h4>
          <div>
            <span style={{ fontSize: '12px' }}>Male: ({malePercentage}%)</span>
            <span className='ms-2' style={{ fontSize: '12px' }}>Female:({femalePercentage}%)</span>
          </div>
        </div>
      </div>
    </div>
  );
}
