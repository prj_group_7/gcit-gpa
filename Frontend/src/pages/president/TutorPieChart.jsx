import * as React from 'react';
import PieChartComponent from './Components/PieChart';

export default function TutorPieChart() {
  const tutorData = [
    { name: 'Male', value: 30 },
    { name: 'Female', value: 70 },
  ];

  // Calculate male and female percentages
  const malePercentage = ((tutorData[0].value / (tutorData[0].value + tutorData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');
  const femalePercentage = ((tutorData[1].value / (tutorData[0].value + tutorData[1].value)) * 100).toFixed(2).replace(/\.?0+$/, '');

  return (
    <div className=''>
      <div className='d-flex align-items-center justify-content-evenly'>
        <div>
          <PieChartComponent data={tutorData} />
        </div>
        <div className='text-center' style={{ width: 300 }}>
          <h4>Tutor</h4>
          <div>
            <span style={{ fontSize: '12px' }}>Male: ({malePercentage}%)</span>
            <span className='ms-2' style={{ fontSize: '12px' }}>Female:({femalePercentage}%)</span>
          </div>
        </div>
      </div>
    </div>
  );
}
