import React from "react";
import { useState } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiAddBoxFill, RiBookReadFill, RiCalendarScheduleLine, RiDashboardLine, RiSoundModuleFill, RiTeamLine, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import Dropdown from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";

function TeamLeadLists() {
  // Define the array of menu items
  const menuItems = [
    { link: "#", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "#", icon: <RiTeamLine />, label: "User" },
    { link: "#", icon: <RiCalendarScheduleLine />, label: "Modules" },
  ];

  const userOptions = [
    "Student",
    "Faculty",
    "Admin",
    "Team Lead",
    "Course Manager",
  ];

  // Define column names
  const columnNames = ["SL.NO", "Program", "Total Student", "Year"];

  return (
    <div className="container-fluid">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3>Team Lead Lists</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-6">
                <SearchBar />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
                <Dropdown options={userOptions} dropdown_prompt="Select User"/>
            </div>
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main border border-success border-2 rounded-1">
                <StickyHeadTable nrow={40} colNames={columnNames} />
            </div>

            </div>
        </div>
        </div>
    </div>
  );
}

export default TeamLeadLists;
