import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import StickyHeadTable from "../../components/Table";
import { RiDashboardLine, RiTeamLine, RiCalendarScheduleLine } from "react-icons/ri";
import DropdownTutor from "../../components/Dropdown";
import SearchBar from "../../components/SearchBar";
import axios from "axios";

const API_BASE_URL = 'http://localhost:3001';

function FacultyList() {
  const [roles, setRoles] = useState([]);
  const [users, setUsers] = useState([]);
  const [selectedRole, setSelectedRole] = useState('');
  const [searchText, setSearchText] = useState('');
  const [filteredUsers, setFilteredUsers] = useState([]);

  useEffect(() => {
    // Fetch roles from the server and set the first role as selected
    axios.get(`${API_BASE_URL}/users/roles`)
      .then(response => {
        const rolesData = response.data.map(role => role.role);
        setRoles(rolesData);
        if (rolesData.length > 0) {
          const initialRole = rolesData[0];
          setSelectedRole(initialRole);
          // Fetch users for the first role
          axios.get(`${API_BASE_URL}/users/roles/${initialRole}`)
            .then(response => {
              setUsers(response.data);
              setFilteredUsers(response.data);
            })
            .catch(error => console.error('Error fetching users:', error));
        }
      })
      .catch(error => console.error('Error fetching roles:', error));
  }, []);

  useEffect(() => {
    if (selectedRole) {
      // Fetch users by role from the server
      axios.get(`${API_BASE_URL}/users/roles/${selectedRole}`)
        .then(response => {
          setUsers(response.data);
          setFilteredUsers(response.data);
        })
        .catch(error => console.error('Error fetching users:', error));
    }
  }, [selectedRole]);

  const handleSearch = (e) => {
    const value = e.target.value;
    setSearchText(value);
    const filtered = users.filter((user) =>
      user.username.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredUsers(filtered);
  };

  // Define the array of menu items
  const menuItems = [
    {
      link: "/president_dashboard",
      icon: <RiDashboardLine />,
      label: "Dashboard",
    },
    { link: "/facultylist", icon: <RiTeamLine />, label: "User" },
    {
      link: "/moduleoverview",
      icon: <RiCalendarScheduleLine />,
      label: "Modules",
    },
  ];

  // Define column names
  const columnNames = ["ID", "Name", "Email", "Role"];
  const colKeyMapping = {
    "ID": "user_id",
    "Name": "username",
    "Email": "email",
    "Role": "role",
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Faculty Lists</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row align-items-center mb-3">
            <div className="col-6">
              <SearchBar handleSearch={handleSearch} value={searchText} />
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2">
              <DropdownTutor
                options={roles} 
                dropdown_prompt="Select User"
                onChange={(role) => setSelectedRole(role)}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 main">
              <StickyHeadTable rows={filteredUsers} colNames={columnNames} colKeyMapping={colKeyMapping} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FacultyList;
