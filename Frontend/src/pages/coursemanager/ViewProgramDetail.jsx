import React, { useState } from 'react';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from 'react-icons/ri';
import Sidebar from '../../components/Sidebar';
import { TextField, MenuItem, Table, TableHead, TableBody, TableRow, TableCell, Typography, Button } from '@mui/material';
import CButton from '../../components/Button';

function ViewProgramDetail() {
  const [programLead, setProgramLead] = useState('John Doe');
  const [startSemester, setStartSemester] = useState('Semester 1');
  const [selectedSemester, setSelectedSemester] = useState('Semester 1');
  const [sections, setSections] = useState([
    {
      formData: [
        { module: 'Module 1', tutor: 'Tutor 1', verifier: 'Verifier A' },
        { module: 'Module 2', tutor: 'Tutor 2', verifier: 'Verifier B' },
        { module: 'Module 3', tutor: 'Tutor 3', verifier: 'Verifier C' },
      ],
    },
  ]);

  const [editMode, setEditMode] = useState(false);

  const semesterOptions = [
    'Semester 1', 'Semester 2', 'Semester 3', 'Semester 4', 'Semester 5', 'Semester 6', 'Semester 7', 'Semester 8'
  ];

  const moduleOptions = ['Module 1', 'Module 2', 'Module 3', 'Module 4', 'Module 5'];
  const tutorOptions = ['Tutor 1', 'Tutor 2', 'Tutor 3', 'Tutor 4', 'Tutor 5'];
  const verifierOptions = ['Verifier A', 'Verifier B', 'Verifier C', 'Verifier D', 'Verifier E'];

  const handleSemesterChange = (event) => {
    const selectedSemester = event.target.value;
    setSelectedSemester(selectedSemester);

    const initialFormData = sections[0].formData.map(({ module }) => ({
      module,
      tutor: '',
      verifier: '',
    }));

    setSections([{ formData: initialFormData }]);
  };

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleCancel = () => {
    setEditMode(false);
  };

  const handleSave = () => {
    // Implement save logic here, e.g., send data to backend
    console.log('Saving data:', sections);
    setEditMode(false);
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-9 ms-3 mt-1">
          <h4 className='p-5 mt-1' style={{color:'#F68218'}}>Manage Program Offered</h4>
          <h5 className='ps-5' style={{color:'#F68218'}}>Program clicked</h5>
          <div className='p-5 ms-5 my-3' style={{borderColor:"#F68218", borderWidth:"2px", borderStyle:"solid", borderRadius:"10px"}}>
            <div style={{ display: 'flex', gap: '16px', marginBottom: '16px' }}>
              <Typography variant="subtitle1">Program Lead Name: {programLead}</Typography>
              <Typography variant="subtitle1">Program Start Semester: {startSemester}</Typography>
            </div>
            <TextField
              select
              label="Select Semester"
              variant="outlined"
              fullWidth
              value={selectedSemester}
              onChange={handleSemesterChange}
              disabled={!editMode}
            >
              {semesterOptions.map((semester) => (
                <MenuItem key={semester} value={semester}>
                  {semester}
                </MenuItem>
              ))}
            </TextField>
            {selectedSemester && (
              <div style={{ marginTop: '16px', overflowX: 'auto' }}>
                <Table style={{borderStyle:'solid', borderColor:'#1280B0', borderWidth:'1px', borderRadius:'10px'}}>
                  <TableHead>
                    <TableRow>
                      <TableCell style={{fontSize:"22px", textAlign:"center"}}>Module</TableCell>
                      <TableCell style={{fontSize:"22px", textAlign:"center"}}>Tutor</TableCell>
                      <TableCell style={{fontSize:"22px", textAlign:"center"}}>Verifier</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {sections[0].formData.map((data, dataIndex) => (
                      <TableRow key={dataIndex} style={{ backgroundColor: dataIndex % 2 === 0 ? 'white' : 'rgba(18, 128, 176, 0.12)' }}>
                        <TableCell>
                          <TextField
                            select
                            value={data.module}
                            fullWidth
                            variant="outlined"
                            disabled={!editMode}
                          >
                            {moduleOptions.map((option) => (
                              <MenuItem key={option} value={option}>
                                {option}
                              </MenuItem>
                            ))}
                          </TextField>
                        </TableCell>
                        <TableCell>
                          <TextField
                            select
                            value={data.tutor}
                            fullWidth
                            variant="outlined"
                            disabled={!editMode}
                          >
                            {tutorOptions.map((option) => (
                              <MenuItem key={option} value={option}>
                                {option}
                              </MenuItem>
                            ))}
                          </TextField>
                        </TableCell>
                        <TableCell>
                          <TextField
                            select
                            value={data.verifier}
                            fullWidth
                            variant="outlined"
                            disabled={!editMode}
                          >
                            {verifierOptions.map((option) => (
                              <MenuItem key={option} value={option}>
                                {option}
                              </MenuItem>
                            ))}
                          </TextField>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
                {editMode ? (
                  <div className='mt-3' style={{textAlign: 'center'}}>
                    <CButton className="ms-3" label="SAVE" onClick={handleSave} />
                    <CButton className="ms-3" label="CANCEL" onClick={handleCancel} />
                  </div>
                ) : (
                  <div className='mt-3' style={{textAlign: 'center'}}>
                    <CButton className="ms-3" label="EDIT" onClick={handleEdit} />
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ViewProgramDetail;
