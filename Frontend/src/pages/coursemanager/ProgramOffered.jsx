import React from 'react';
import {useState, useEffect} from 'react';
import Sidebar from '../../components/Sidebar';
import AddProgramTable from '../../components/ProgramOfferedTable';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine} from 'react-icons/ri';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001'


function ProgramOffered() {
  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];

  

  const [offeredPrograms,setOfferedPrograms] = useState([])  
  const [filteredPrograms,setFilteredPrograms] = useState([])
  const [searchQuery,setSearchQuery] = useState('') 

  useEffect(() => {
    const fetchPrograms = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/coursemanager/programoffered`);
        console.log(response.data.data)
        setOfferedPrograms(response.data.data);
        setFilteredPrograms(response.data.data);
       
      } catch (error) {
        console.error(error);
      }
    };

    fetchPrograms();
  }, []);
console.log(filteredPrograms)
  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = offeredPrograms.filter((program) =>
      program.program_name.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredPrograms(filtered);
  };

  

  const columnNames = ["Program", 'Year'];

  // const ProgramList = [
  //   { 
  //     SlNo: 1, Program: 'CAS201', Manage_Program: <CButton className="ms-3" label="MANAGE" onClick={handleManageRoute} />,
  //     View_Details: (
  //       <button className="btn btn-link" onClick={() => handleViewDetails}>
  //         <RiEyeLine size={20} />
  //       </button>
  //     ),
  //   },
  //   { 
  //     SlNo: 2, Program: 'Joe503', Manage_Program: <CButton className="ms-3" label="MANAGE" onClick={handleRoute} />,
  //     View_Details: (
  //       <button className="btn btn-link" onClick={() => handleViewDetails}>
  //         <RiEyeLine size={20} />
  //       </button>
  //     ),
  //   },
  //   { 
  //     SlNo: 3, Program: 'DSA500', Manage_Program: <CButton className="ms-3" label="MANAGE" onClick={handleRoute} />,
  //     View_Details: (
  //       <button className="btn btn-link" onClick={() => handleViewDetails}>
  //         <RiEyeLine size={20} />
  //       </button>
  //     ),
  //   },
  //   { 
  //     SlNo: 4, Program: 'PTL203', Manage_Program: <CButton className="ms-3" label="MANAGE" onClick={handleRoute} />,
  //     View_Details: (
  //       <button className="btn btn-link" onClick={() => handleViewDetails}>
  //         <RiEyeLine size={20} />
  //       </button>
  //     ),
  //   },
  //   // Add more rows as needed
  // ];

  const colKeyMapping = {
    'Program': 'program_name',
    'Year': 'year'
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Manage Program Offered</h3>
            </div>
          </div>
          <div className='row align-items-center mb-3'>
          <div className="col-7 text-end">
              <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
          </div>
          <div className="row">
            <div className="col-12 main">
              <AddProgramTable rows={filteredPrograms} colNames={columnNames} colKeyMapping={colKeyMapping}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProgramOffered;
