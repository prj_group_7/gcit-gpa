import React from 'react';
import { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import StickyHeadTable from '../../components/Table';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from 'react-icons/ri'; // Import the required icons
import CButton from '../../components/Button';
import BasicModal from '../../components/Modal';
import {Alert} from 'react-bootstrap'
import axios from 'axios';
import { useParams } from 'react-router-dom';

const API_BASE_URL = 'http://localhost:3001';

function IndividualModuleMark() {
  // Define the array of menu items
  const {moduleCode} = useParams();
  const {moduleoffered_id} = useParams();

  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];
  
  const [formData, setFormData] = useState({
    modulename:'',
    modulecode:'',
    modulecredit:'',
    descriptions:'',
  });

  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modules,setModules] = useState([])
  const [filteredModules,setFilteredModules] = useState([])
  const [searchQuery,setSearchQuery] = useState('')
  const [marks, setMarks] = useState([])
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
        setAlertMessage(null);
    }, 3000);
    };

  useEffect(() => {
    const fetchMarks = async () => {
      try {
          const response = await axios.get(`${API_BASE_URL}/tutor/marks/getmarks`, {
              params: { moduleoffered_id },
          });
          setMarks(response.data);
      } catch (e) {
          console.log(e.message);
      }
  };
    fetchMarks();
  }, []); 
  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    try {
      const response = await axios.post(`${API_BASE_URL}/coursemanager/modules`, formData);
      console.log('This is form data: ',formData)
      setSuccess(response.data.message);
      setError('');
      setIsModalOpen(false); // Close the modal on success
    } catch (err) {
      setError(err.response.data.message);
      setSuccess('');
    }
  };

  const handleAlertClose = () => {
    setError('');
    setSuccess('');
  };

  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = modules.filter((module) =>
      module.module_name.toLowerCase().includes(value.toLowerCase()) ||
      module.module_code.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredModules(filtered);
  };


  // Define column names
  const columnNames = ["Serial No.", "Student ID", "Marks"];
  const colKeyMapping = { 'Serial No.': 'SN', 'Student ID': 'student_id', 'Marks': 'marks_obtained' };

  const rows = marks.map((mark, index) => ({ ...mark, SN: index + 1 }));

  return (
    <div className="">
        <div className="row">
            
        <div className="col-2">
            <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">

            <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
                <h3 style={{color: 'rgba(246,130,24,0.8)'}}>{moduleCode}</h3> {/* Replace the Page Heading */}
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            </div>

            <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-7">
                <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
            <div className="col-1"></div>
            <div className="col-2">
                
            </div>
            
            </div>

            <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
                <StickyHeadTable rows={rows} colNames={columnNames} colKeyMapping={colKeyMapping} />
                {alertMessage && (
                <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
                  {alertMessage.message}
                </Alert>
              )}
            </div>
            
            </div>
        </div>
        </div>
        {error && (
              <div className="alert alert-danger position-fixed bottom-0 start-0 p-3">
                {error}
                <span className="ms-auto" onClick={handleAlertClose}>&times;</span>
              </div>
            )}
            {success && (
              <div className="alert alert-success position-fixed bottom-0 start-0 p-3">
                {success}
                <span className="ms-auto" onClick={handleAlertClose}>&times;</span>
              </div>
            )}
    </div>
  );
}

export default IndividualModuleMark;
