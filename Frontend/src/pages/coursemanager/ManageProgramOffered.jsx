import React, { useState, useEffect } from "react";
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from "react-icons/ri";
import CButton from "../../components/Button";
import Sidebar from "../../components/Sidebar";
import {
  TextField,
  MenuItem,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Button,
} from "@mui/material";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Alert } from "react-bootstrap";

const API_BASE_URL = "http://localhost:3001";

function ManageProgramOffered() {
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const { programofferedId } = useParams();
  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];

  const [programLead, setProgramLead] = useState("");
  const [programOffered, setProgramOffered] = useState("");
  const [programLeadId, setProgramLeadId] = useState("");
  const [selectedSemester, setSelectedSemester] = useState("");
  const [gettutors, setGetTutors] = useState([]);
  const [getmodules, setGetModules] = useState([]);
  const [sections, setSections] = useState([
    {
      formData: Array.from({ length: 5 }, () => ({
        moduleId: "",
        moduleName: "",
        tutorId: "",
        tutorName: "",
        verifierId: "",
        verifierName: "",
      })),
    },
  ]);

  const [semesterOptions, setSemesterOptions] = useState([
    "Semester 1",
    "Semester 2",
    "Semester 3",
    "Semester 4",
    "Semester 5",
    "Semester 6",
    "Semester 7",
    "Semester 8"
  ]);

  useEffect(() => {
    const fetchTutors = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/users/tutors`);
        setGetTutors(response.data.data);
      } catch (error) {
        console.error(error);
      }
    };

    const fetchModules = async () => {
      try {
        const response = await axios.get(
          `${API_BASE_URL}/coursemanager/modules`
        );
        setGetModules(response.data.data);
      } catch (error) {
        console.error(error);
      }
    };



    fetchModules();
    fetchTutors();
  }, []);

  useEffect(() => {
    const fetchPO = async () => {
      try {

        const response = await axios.get(
          `${API_BASE_URL}/coursemanager/programoffered/${programofferedId}`
        );
        const po = response.data.data;
        console.log(po)
        setProgramOffered(po);
        let sos;
        if (po.program_name === 'Bachelor of Science in Computer Science') {
          sos = [
            "Semester 1",
            "Semester 2",
          ];
        } else if ((po.program_name === 'Bachelor Of Computer Science (Blockchain Development)') ||
         (po.program_name === 'Bachelor Of Computer Science (AI Development & Data Science)') ||
         (po.program_name === 'Bachelor Of Computer Science (Full Stack Development)'))
         {
          sos = [
            "Semester 3",
            "Semester 4",
            "Semester 5",
            "Semester 6",
            "Semester 7",
            "Semester 8",
          ];
        } else {
          sos = [
            "Semester 1",
            "Semester 2",
            "Semester 3",
            "Semester 4",
            "Semester 5",
            "Semester 6",
            "Semester 7",
            "Semester 8",
          ];
        }

        setSemesterOptions(sos)
        // alert(JSON.stringify(response.data.data))
      } catch (error) {
        console.error(error);
      }
    };

    fetchPO();

    const fetchModulesOffered = async () => {
      if (selectedSemester) {
        try {
          const response = await axios.get(
            `${API_BASE_URL}/coursemanager/modulesoffered/getmodulesoffered`,
            {
              params: {
                programofferedId,
                selectedSemester,
              },
            }
          );

          const fetchedSections = response.data.data.map((module) => ({
            moduleId: module.module_id,
            moduleName: getmodules.find((mod) => mod.module_id === module.module_id)?.module_name || "",
            tutorId: module.tutor,
            tutorName: gettutors.find((tutor) => tutor.user_id === module.tutor)?.username || "",
            verifierId: module.module_verifier,
            verifierName: gettutors.find((tutor) => tutor.user_id === module.module_verifier)?.username || "",
          }));

          const len = fetchedSections.length
          for (let i = 0; i < 5 - len; i++) {
            fetchedSections.push({
              moduleId: "",
              moduleName: "",
              tutorId: "",
              tutorName: "",
              verifierId: "",
              verifierName: "",
            })
          }


          setSections([{
            formData: len > 0 ? fetchedSections : Array.from({ length: 5 }, () => ({
              moduleId: "",
              moduleName: "",
              tutorId: "",
              tutorName: "",
              verifierId: "",
              verifierName: "",
            }))
          }]);
        } catch (error) {
          console.error(error);
          setSections([{
            formData: Array.from({ length: 5 }, () => ({
              moduleId: "",
              moduleName: "",
              tutorId: "",
              tutorName: "",
              verifierId: "",
              verifierName: "",
            }))
          }]);
        }
      } else {
        setSections([{
          formData: Array.from({ length: 5 }, () => ({
            moduleId: "",
            moduleName: "",
            tutorId: "",
            tutorName: "",
            verifierId: "",
            verifierName: "",
          }))
        }]);
      }
    };
    fetchModulesOffered();
  }, [selectedSemester, programofferedId, getmodules, gettutors]);


  const handleSemesterChange = (event) => {
    const selectedSemester = event.target.value;
    setSelectedSemester(selectedSemester);
  };

  const handleProgramLeadChange = (event) => {
    const { value } = event.target;
    const selectedTutor = gettutors.find((tutor) => tutor.username === value);
    setProgramLeadId(selectedTutor.user_id);
    setProgramLead(value);
  };

  const addSection = () => {
    const newSectionData = Array.from({ length: 5 }, () => ({
      moduleId: "",
      moduleName: "",
      tutorId: "",
      tutorName: "",
      verifierId: "",
      verifierName: "",
    }));

    setSections([...sections, { formData: newSectionData }]);
  };

  const removeSection = (index) => {
    const updatedSections = [...sections];
    updatedSections.splice(index, 1);
    setSections(updatedSections);
  };

  const handleModuleChange = (index, dataIndex, selectedModule) => {
    const specificModule = getmodules.find(
      (module) => module.module_name === selectedModule
    );
    console.log("INDEX", specificModule)
    const moduleId = specificModule ? specificModule.module_id : "";
    const moduleName = specificModule ? specificModule.module_name : "";
    const updatedSections = [...sections];
    if (sections.length)
      updatedSections[index].formData[dataIndex].moduleId = moduleId;
    updatedSections[index].formData[dataIndex].moduleName = moduleName;
    setSections(updatedSections);
  };

  const handleTutorChange = (index, dataIndex, selectedTutor) => {
    const specificTutor = gettutors.find(
      (tutor) => tutor.username === selectedTutor
    );
    const tutorId = specificTutor ? specificTutor.user_id : "";
    const tutorName = specificTutor ? specificTutor.username : "";
    const updatedSections = [...sections];
    updatedSections[index].formData[dataIndex].tutorId = tutorId;
    updatedSections[index].formData[dataIndex].tutorName = tutorName;
    setSections(updatedSections);
  };

  const handleVerifierChange = (index, dataIndex, selectedVerifier) => {
    const specificVerifier = gettutors.find(
      (tutor) => tutor.username === selectedVerifier
    );
    const verifierId = specificVerifier ? specificVerifier.user_id : "";
    const verifierName = specificVerifier ? specificVerifier.username : "";
    const updatedSections = [...sections];
    updatedSections[index].formData[dataIndex].verifierId = verifierId;
    updatedSections[index].formData[dataIndex].verifierName = verifierName;
    setSections(updatedSections);
  };

  const handleSave = async () => {
    try {
      const dataToSave = {
        programLeadId,
        selectedSemester,
        sections,
        programofferedId,
      };

      const response = await axios.post(
        `${API_BASE_URL}/coursemanager/modulesoffered`,
        dataToSave
      );
      showAlert(response.data.message, 'success');
      console.log("Data saved successfully:", response.data);
      // handleReset();
    } catch (error) {
      console.error("Error saving data:", error);
      showAlert(error.response.data.message, 'danger');
    }
  };

  const handleReset = () => {
    setProgramLead("");
    setSelectedSemester("");
    setSections([
      {
        formData: Array.from({ length: 5 }, () => ({
          moduleId: "",
          moduleName: "",
          tutorId: "",
          tutorName: "",
          verifierId: "",
          verifierName: "",
        })),
      },
    ]);
  };
  console.log(sections);
  return (
    <div className="container-fluid">
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-9 ms-3 mt-1">
          <h4 className="p-5 mt-1" style={{ color: 'rgba(246,130,24,0.8)' }}>
            Manage Program Offered
          </h4>
          {/* <h5 className="ps-5" style={{color: 'rgba(246,130,24,0.8)'}}>
            Program clicked
          </h5> */}
          <div
            className="p-5 ms-5 my-3"
            style={{
              borderColor: "#F68218",
              borderWidth: "2px",
              borderStyle: "solid",
              borderRadius: "10px",
            }}
          >
            <div style={{ display: "flex", gap: "16px", marginBottom: "16px" }}>
              <TextField
                select
                label="Program Lead Name"
                variant="outlined"
                fullWidth
                value={programLead}
                onChange={handleProgramLeadChange}
                style={{ marginBottom: "16px" }}
              >
                {gettutors.map((tutor) => (
                  <MenuItem key={tutor.user_id} value={tutor.username}>
                    {tutor.username}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                select
                label="Select Semester"
                variant="outlined"
                fullWidth
                value={selectedSemester}
                onChange={handleSemesterChange}
              >
                {semesterOptions.map((semester) => (
                  <MenuItem key={semester} value={semester}>
                    {semester}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            {selectedSemester && (
              <div style={{ marginTop: "16px", overflowX: "auto" }}>
                {sections.map((section, index) => (
                  <div key={index}>
                    <h3 style={{ color: "grey" }}>{selectedSemester}</h3>
                    <h4 style={{ color: "grey" }}>Section {index + 1}</h4>
                    <Table
                      style={{
                        borderStyle: "solid",
                        borderColor: "#1280B0",
                        borderWidth: "1px",
                        borderRadius: "10px",
                      }}
                    >
                      <TableHead>
                        <TableRow>
                          <TableCell
                            style={{ fontSize: "22px", textAlign: "center" }}
                          >
                            Module
                          </TableCell>
                          <TableCell
                            style={{ fontSize: "22px", textAlign: "center" }}
                          >
                            Tutor
                          </TableCell>
                          <TableCell
                            style={{ fontSize: "22px", textAlign: "center" }}
                          >
                            Verifier
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {section.formData.map((data, dataIndex) => (
                          <TableRow
                            key={dataIndex}
                            style={{
                              backgroundColor:
                                dataIndex % 2 === 0
                                  ? "white"
                                  : "rgba(18, 128, 176, 0.12)",
                            }}
                          >
                            <TableCell>
                              <TextField
                                select
                                value={section.formData[dataIndex]?.moduleName}
                                fullWidth
                                variant="outlined"
                                disabled={index > 0}
                                onChange={(e) =>
                                  handleModuleChange(
                                    index,
                                    dataIndex,
                                    e.target.value
                                  )
                                }
                              >
                                {getmodules.map((module) => (
                                  <MenuItem
                                    key={module.module_id}
                                    value={module.module_name}
                                  >
                                    <span>{module.module_name}</span>
                                    <span>({module.module_code})</span>
                                  </MenuItem>
                                ))}
                              </TextField>
                            </TableCell>
                            <TableCell>
                              <TextField
                                select
                                value={section.formData[dataIndex]?.tutorName}
                                fullWidth
                                variant="outlined"
                                onChange={(e) =>
                                  handleTutorChange(
                                    index,
                                    dataIndex,
                                    e.target.value
                                  )
                                }
                              >
                                {gettutors.map((tutor) => (
                                  <MenuItem
                                    key={tutor.user_id}
                                    value={tutor.username}
                                  >
                                    {tutor.username}
                                  </MenuItem>
                                ))}
                              </TextField>
                            </TableCell>
                            <TableCell>
                              <TextField
                                select
                                value={section.formData[dataIndex]?.verifierName}
                                fullWidth
                                variant="outlined"
                                disabled={index > 0}
                                onChange={(e) =>
                                  handleVerifierChange(
                                    index,
                                    dataIndex,
                                    e.target.value
                                  )
                                }
                              >
                                {gettutors.map((tutor) => (
                                  <MenuItem
                                    key={tutor.user_id}
                                    value={tutor.username}
                                  >
                                    {tutor.username}
                                  </MenuItem>
                                ))}
                              </TextField>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                    <div className="my-3">
                      {index > 0 && (
                        <Button
                          variant="outlined"
                          onClick={() => removeSection(index)}
                        >
                          Delete Section
                        </Button>
                      )}
                    </div>
                  </div>
                ))}
                <Button variant="outlined" onClick={addSection}>
                  Add Section
                </Button>
              </div>
            )}
            <div
              className="row mt-3 justify-content-center"
              style={{ textAlign: "center" }}
            >
              <div className="col-3">
                <CButton className="ms-3" label="SAVE" onClick={handleSave}></CButton>
              </div>
              <div className="col-1"></div>
              <div className="col-3">
                <CButton className="ms-3" label="RESET" onClick={handleReset}></CButton>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManageProgramOffered;
