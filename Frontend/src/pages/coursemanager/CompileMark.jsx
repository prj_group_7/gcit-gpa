import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from 'react-icons/ri';
import CButton from '../../components/Button';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import CompileMarkTable from '../../components/CompileMarkTable';
import {Alert} from 'react-bootstrap';

const API_BASE_URL = 'http://localhost:3001';

function CompileMark() {
  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];

  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [modules, setModules] = useState([]);
  const [filteredModules, setFilteredModules] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
        setAlertMessage(null);
    }, 3000);
    };

  const navigate = useNavigate();

  useEffect(() => {
    const fetchModules = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/coursemanager/modulesoffered/compilemodules`);
        setModules(response.data);
        setFilteredModules(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchModules();
  }, []);

  const handleAlertClose = () => {
    setError('');
    setSuccess('');
  };

  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = modules.filter((module) =>
      module.module_name.toLowerCase().includes(value.toLowerCase()) ||
      module.module_code.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredModules(filtered);
  };

  const handleActionClick = (modulecode, moduleoffered_id) => {
    navigate(`/modulemarks/${modulecode}/${moduleoffered_id}`);
  };

  const CompileAllModule = async () => {
    try {
      const response = await axios.post(`${API_BASE_URL}/coursemanager/modulesoffered/compileall`);
      if (response.data.success) {
        showAlert(response.data.message, 'success')
        // Optionally update the state to reflect the new status
        const updatedModules = modules.map(module => 
          module.status === 'Pending' ? { ...module, status: 'Completed' } : module
        );
        setModules(updatedModules);
        setFilteredModules(updatedModules);
      } else {
        showAlert(response.data.message, 'danger')
      }
    } catch (error) {
      console.error('Error compiling all modules:', error);
      showAlert('An error occurred while compiling the modules.', 'danger');
    }
  };

  const columnNames = ["Module Code", "Programme", "Semester", "Action"];
  const colKeyMapping = {
    'Module Code': 'module_code',
    'Programme': 'program_name',
    'Semester': 'semester_id',
    'Action': 'action',
  };

  return (
    <div className="">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>

        <div className="col-10 p-5">
          <div className="row mb-3">
            <div className="col-6">
              <h3 style={{ color: 'rgba(246,130,24,0.8)' }}>Compile Mark</h3>
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>

          <div className="row align-items-center mb-3">
            <div className="col-7">
              <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
            <div className="col-1"></div>
            <div className="col-2"></div>
            <div className="col-2">
              <CButton className="ms-3" label="COMPILE ALL" onClick={CompileAllModule}></CButton>
            </div>
          </div>

          <div className="row">
            <div className="col-12 main">
              <CompileMarkTable
                rows={filteredModules}
                colNames={columnNames}
                colKeyMapping={colKeyMapping}
                onActionClick={handleActionClick}
              />
              {alertMessage && (
                <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
                  {alertMessage.message}
                </Alert>
              )}
            </div>
          </div>
        </div>
      </div>
      
    </div>
  );
}

export default CompileMark;
