import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel, faChartLine } from "@fortawesome/free-solid-svg-icons"; // Import the line graph icon
import StudentPieChart from "../president/StudentPieChart";
import TeamLeadPieChart from "../president/TeamLeadPieChart";
import TutorPieChart from "../president/TutorPieChart";
import CardCarousel from "../president/Components/Card";
import LineGraph from "../president/Components/LineGraph";
import Sidebar from "../../components/Sidebar";
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from "react-icons/ri"; // Import the required icons
import { Line } from "recharts";
import PresidentDashboard from "../president/PresidentDashboard";

export default function CourseManagerDashboard() {
  const menuItems = [
    {
      link: "/coursemanagerdashboard",
      icon: <RiDashboardLine />,
      label: "Dashboard",
    },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];

  return (
    <div className="container-fluid">
        <div className="row flex-nowrap">
            <div className="col-2">
                <Sidebar menuItems={menuItems} />
            </div>
            <div className="col-10 p-5">
              <div className="col-3">
                <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Dashboard</h3>
              </div>
                <PresidentDashboard />
            </div>
        </div>
    </div>
  );
}