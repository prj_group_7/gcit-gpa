import React from 'react';
import { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import StickyHeadTable from '../../components/Table';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from 'react-icons/ri'; // Import the required icons
import CButton from '../../components/Button';
import { Alert } from 'react-bootstrap';
import BasicModal from '../../components/Modal';
import ToggleButton from '../../components/ToggleButton';
import AddProgramTable from '../../components/AddProgramTable';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function AddProgram() {
  // Define the array of menu items
  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];

  // Define column names
  const columnNames = ["Sl.No", "Name"];

  const colKeyMapping = {
    'Sl.No': 'program_id',
    'Name': 'program_name',
  };

  const initialFormData = {
    programname: '',
    description: '',
    duration: '',
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [programs, setPrograms] = useState([]);
  const [filteredPrograms, setFilteredPrograms] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');

  const [alertMessage, setAlertMessage] = useState(null);
  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const [formData, setFormData] = useState(initialFormData);

  useEffect(() => {
    const fetchPrograms = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/coursemanager/programs`);
        setPrograms(response.data.data);
        setFilteredPrograms(response.data.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchPrograms();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!formData.programname || !formData.description || !formData.duration) {
      showAlert('Please fill in all fields before submitting.', 'danger');
      return;
    }
    try {
      const response = await axios.post(`${API_BASE_URL}/coursemanager/programs`, formData);
      console.log(formData);
      console.log('This is form data: ', formData);
      if (response.data.success) {
        showAlert(response.data.message, 'success');
        setFormData(initialFormData); // Reset the form data to initial state
      } else {
        showAlert(response.data.message, 'danger');
      }
      setIsModalOpen(false); // Close the modal on success
    } catch (err) {
      showAlert(err.response.message.data, 'danger');
    }
  };

  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = programs.filter((program) =>
      program.program_name.toLowerCase().includes(value.toLowerCase()) ||
      program.program_duration.toString().includes(value.toLowerCase())
    );
    setFilteredPrograms(filtered);
  };

  const handleAlertClose = () => {
    setError('');
    setSuccess('');
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleSwitchToggle = (isChecked) => {
    console.log('Switch toggled:', isChecked);
    // Add your logic to handle the switch toggle event
  };

  const fields = [
    {
      type: 'text',
      label: 'Program Name',
      name: 'programname',
      defaultValue: formData.programname,
    },
    {
      type: 'text',
      label: 'Description',
      name: 'description',
      defaultValue: formData.description,
      error: true, // Example: Show error state
    },
    {
      type: 'text',
      label: 'Durations',
      name: 'duration',
      defaultValue: formData.duration,
      error: true,
    }
  ];

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3 style={{ color: 'rgba(246,130,24,0.8)' }}>Program Management</h3> {/* Replace the Page Heading */}
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-7">
              <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
            <div className="col-1"></div>
            <div className="col-2"></div>
            <div className="col-2">
              <CButton className="ms-3" label="ADD" onClick={openModal}></CButton>
              {isModalOpen && (
                <BasicModal
                  open={isModalOpen}
                  handleClose={closeModal}
                  title=" Details"
                  // fields={[{ label: 'Field 1', defaultValue: '' }, { label: 'Field 2', defaultValue: '' }]}
                  fields={fields}
                  handleChange={handleChange}
                  handleSubmit={handleSubmit}
                  buttonName={"Add Program"}
                />
              )}
            </div>
          </div>
          <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
              <AddProgramTable rows={filteredPrograms} colNames={columnNames} colKeyMapping={colKeyMapping} />
            </div>
          </div>
        </div>
      </div>
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
    </div>
  );
}

export default AddProgram;
