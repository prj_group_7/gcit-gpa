import React from 'react';
import { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import StickyHeadTable from '../../components/Table';
import SearchBar from '../../components/SearchBar';
import { RiDashboardLine, RiTeamLine, RiUserSearchLine } from 'react-icons/ri'; // Import the required icons
import CButton from '../../components/Button';
import BasicModal from '../../components/Modal';
import { Alert } from 'react-bootstrap';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001';

function AddModule() {
  // Define the array of menu items
  const menuItems = [
    { link: "/coursemanagerdashboard", icon: <RiDashboardLine />, label: "Dashboard" },
    { link: "/addmodule", icon: <RiTeamLine />, label: "Module" },
    { link: "/addprogram", icon: <RiUserSearchLine />, label: "Program" },
    { link: "/programoffered", icon: <RiTeamLine />, label: "Program Offered" },
    { link: "/compilemark", icon: <RiUserSearchLine />, label: "Compile Mark" },
  ];
  
  const initialFormData = {
    modulename: '',
    modulecode: '',
    modulecredit: '',
    descriptions: '',
  };

  const [formData, setFormData] = useState(initialFormData);
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modules, setModules] = useState([]);
  const [filteredModules, setFilteredModules] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  useEffect(() => {
    const fetchModules = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/coursemanager/modules`);
        setModules(response.data.data);
        setFilteredModules(response.data.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchModules();
  }, []); 

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!formData.modulecode || !formData.modulename || !formData.modulecredit) {
      showAlert('Please fill in all fields before submitting.', 'danger');
      return;
    }
    try {
      const response = await axios.post(`${API_BASE_URL}/coursemanager/modules`, formData);
      console.log('This is form data: ', formData);
      if (response.data.success) {
        showAlert(response.data.message, 'success');
        setFormData(initialFormData); // Reset the form data to initial state
      } else {
        showAlert(response.data.message, 'danger');
      }
      setIsModalOpen(false); // Close the modal on success
    } catch (err) {
      showAlert(err.message, 'danger');
    }
  };

  const handleAlertClose = () => {
    setError('');
    setSuccess('');
  };

  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    const filtered = modules.filter((module) =>
      module.module_name.toLowerCase().includes(value.toLowerCase()) ||
      module.module_code.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredModules(filtered);
  };

  // Define column names
  const columnNames = ["Sl.No", "Module Code", "Name", "Status"];

  const colKeyMapping = {
    'Sl.No': 'module_id',
    'Module Code': 'module_code',
    'Name': 'module_name',
    'Status': 'status',
  };

  const fields = [
    {
      type: 'text',
      label: 'Module Name',
      name: 'modulename',
      defaultValue: formData.modulename,
    },
    {
      type: 'text',
      label: 'Module Code',
      name: 'modulecode',
      defaultValue: formData.modulecode,
      error: true, // Example: Show error state
    },
    {
      type: 'text',
      label: 'Module Credits',
      name: 'modulecredit',
      defaultValue: formData.modulecredits,
      error: true, // Example: Show error state
    },
    {
      type: 'text',
      label: 'Description',
      name: 'description',
      defaultValue: formData.descriptions,
      error: true, // Example: Show error state
    },
  ];

  return (
    <div className="">
      <div className="row">
        <div className="col-2">
          <Sidebar menuItems={menuItems} />
        </div>
        <div className="col-10 p-5">
          <div className="row mb-3"> {/* Top-level header */}
            <div className="col-6">
              <h3 style={{color: 'rgba(246,130,24,0.8)'}}>Module Management</h3> {/* Replace the Page Heading */}
            </div>
            <div className="col-2"></div>
            <div className="col-2"></div>
            <div className="col-2"></div>
          </div>
          <div className="row align-items-center mb-3"> {/* Second-level header */}
            <div className="col-7">
              <SearchBar handleSearch={handleSearch} value={searchQuery} />
            </div>
            <div className="col-1"></div>
            <div className="col-2"></div>
            <div className="col-2">
              <CButton className="ms-3" label="ADD" onClick={openModal}></CButton>
              {isModalOpen && (
                <BasicModal
                  open={isModalOpen}
                  handleClose={closeModal}
                  title="Details"
                  fields={fields}
                  handleChange={handleChange}
                  handleSubmit={handleSubmit}
                  buttonName={"Add Program"}
                />
              )}
            </div>
          </div>
          <div className="row"> {/* Body/main content */}
            <div className="col-12 main">
              <StickyHeadTable rows={filteredModules} colNames={columnNames} colKeyMapping={colKeyMapping} />
            </div>
          </div>
        </div>
      </div>
      {alertMessage && (
        <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
          {alertMessage.message}
        </Alert>
      )}
    </div>
  );
}

export default AddModule;
