const pool = require('../../db');

const modulesfortutor = async (req, res) => {
  const { user_id } = req.query;
  const query = `
    SELECT
      mo.moduleoffered_id,
      mo.programoffered_id,
      p.program_name,
      po.year,
      mo.semester_id,
      mo.module_verifier,
      uv.username AS module_verifier_name,
      mo.tutor,
      mo.section_id,
      m.module_id,
      m.module_name,
      m.module_code,
      m.module_description,
      m.credits,
      m.status,
      COALESCE(students.student_count, 0) AS student_count
    FROM
      modulesoffereds mo
    JOIN
      modules m ON mo.module_id = m.module_id
    LEFT JOIN
      programoffereds po ON mo.programoffered_id = po.programoffered_id
    LEFT JOIN
      programs p ON po.program_id = p.program_id
    LEFT JOIN (
      SELECT
        s.programoffered_id,
        COUNT(s.student_id) AS student_count
      FROM
        students s
      GROUP BY
        s.programoffered_id
    ) students ON mo.programoffered_id = students.programoffered_id
    LEFT JOIN
      users uv ON mo.module_verifier = uv.user_id
    WHERE
      mo.tutor = $1
  `;

  try {
    const result = await pool.query(query, [user_id]);
    // console.log(result.rows);
    res.json(result.rows);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};
const checkModuleVerifier = async (req, res) => {
    const { user_id } = req.query;
    const query = `
      SELECT EXISTS (
        SELECT 1
        FROM modulesoffereds
        WHERE module_verifier = $1
      ) AS is_verifier;
    `;
  
    try {
      const result = await pool.query(query, [user_id]);
      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  };
  
  const gettutormodules = async (req, res) => {
    const { user_id } = req.query;
    const query = `
      SELECT mo.moduleoffered_id, m.module_name, mo.semester_id
      FROM modulesoffereds mo
      JOIN modules m ON mo.module_id = m.module_id
      WHERE mo.tutor = $1 AND (mo.status IS NULL OR mo.status = 'Pending')
      ORDER BY mo.semester_id ASC;
    `;
    
    try {
      const result = await pool.query(query, [user_id]);
      res.json(result.rows);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  }
  
  
  const getverifiermodules = async (req, res) => {
    const { user_id } = req.query;
    try {
      const result = await pool.query(`
        SELECT mo.moduleoffered_id, m.module_name , mo.status
        FROM modulesoffereds mo
        JOIN modules m ON mo.module_id = m.module_id
        WHERE mo.module_verifier = $1
        AND (mo.status IS NULL OR mo.status = 'Pending')
        ORDER BY mo.semester_id ASC;
      `, [user_id]);
      res.json(result.rows);
      console.log(result.rows)
    } catch (e) {
      console.error(e);
      res.status(500).send('Server error');
    }
  }
  
module.exports = {
  modulesfortutor,
  checkModuleVerifier,
  gettutormodules,
  getverifiermodules
};
