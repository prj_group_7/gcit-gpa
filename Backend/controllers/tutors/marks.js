const pool = require('../../db');
const transporter = require('../../email.js');

const addMarks = async (req, res) => {
    const { marks, moduleoffered_id } = req.body;
   
    try {
        const modulesCompleted = await pool.query(
            'SELECT * FROM modulesoffereds WHERE moduleoffered_id = $1 AND (status = $2 OR status = $3)',
            [moduleoffered_id, 'Pending', 'Completed']
        );
          
        if (modulesCompleted.rows.length > 0) {
            return res.status(200).json({ success: false, message: 'You cannot make changes to this mark' });
        }
        for (const mark of marks) {
            // Check if marks already exist for the student and moduleoffered_id
            const existingMark = await pool.query(
                'SELECT * FROM marks WHERE student_id = $1 AND modulesoffered_id = $2',
                [mark.student_id, mark.modulesoffered_id]
            );

            if (existingMark.rows.length > 0) {
                // Update existing marks
                await pool.query(
                    'UPDATE marks SET marks_obtained = $1 WHERE student_id = $2 AND modulesoffered_id = $3',
                    [mark.marks_obtained, mark.student_id, mark.modulesoffered_id]
                );
            } else {
                // Insert new marks
                await pool.query(
                    'INSERT INTO marks (student_id, modulesoffered_id, marks_obtained, staff_id) VALUES ($1, $2, $3, $4)',
                    [mark.student_id, mark.modulesoffered_id, mark.marks_obtained, mark.staff_id]
                );
            }
        }

        res.status(200).json({ success: true, message: 'Marks submitted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
};
  

const getMarks = async (req,res)=>{
    const { moduleoffered_id } = req.query;
    console.log(moduleoffered_id)
    const query = `
        SELECT s.student_id, m.marks_obtained, m.staff_id
        FROM marks m
        JOIN students s ON m.student_id = s.student_id
        WHERE m.modulesoffered_id = $1
    `;

    try {
        const result = await pool.query(query, [moduleoffered_id]);
        res.json(result.rows);
        console.log(result.rows);
    } catch (error) {
        console.error(error);
        res.status(500).send('Internal Server Error');
    }
}

const verifyMarks = async(req,res)=>{
    const { marks_id, verified, module_verifier, staff_email, student_id, marks_obtained } = req.body;
    try {
        const result = await pool.query(`
            UPDATE marks 
            SET verified = $1, module_verifier = $2
            WHERE marks_id = $3
            RETURNING *
        `, [verified, module_verifier, marks_id]);


        res.json(result.rows[0]);
    } catch (e) {
        console.error(e);
        res.status(500).send('Server error');
    }
}

const getMarksVerifier = async (req, res) => {
    const { moduleoffered_id } = req.query;
    console.log(moduleoffered_id);
    try {
        const result = await pool.query(`
            SELECT m.marks_id, s.student_id, s.name as student_name, m.marks_obtained, m.verified, u.email as staff_email
            FROM marks m
            JOIN students s ON m.student_id = s.student_id
            JOIN users u ON m.staff_id = u.user_id
            WHERE m.modulesoffered_id = $1
            ORDER BY s.student_id ASC
        `, [moduleoffered_id]);
        res.json(result.rows);
        console.log(result.rows);
    } catch (e) {
        console.error(e);
        res.status(500).send('Server error');
    }
}


async function uploadCSV(parsedData, moduleoffered_id, staff_id) {
    try {
        const marksInserts = [];

        for (const row of parsedData) {
            // Prepare marks data
            const student_id = row[0];
            console.log('Student ID', student_id);
            const marks_obtained = row[1];

            // Check if marks already exist for the student
            const existingMarks = await pool.query(`
                SELECT * FROM marks
                WHERE student_id = $1 AND modulesoffered_id = $2 AND staff_id = $3
            `, [student_id, moduleoffered_id, staff_id]);

            if (existingMarks.rows.length > 0) {
                // Update marks if already exists
                await pool.query(`
                    UPDATE marks
                    SET marks_obtained = $1
                    WHERE student_id = $2 AND modulesoffered_id = $3 AND staff_id = $4
                `, [marks_obtained, student_id, moduleoffered_id, staff_id]);
            } else {
                // Insert new marks if not exists
                await pool.query(`
                    INSERT INTO marks (student_id, modulesoffered_id, marks_obtained, staff_id)
                    VALUES ($1, $2, $3, $4)
                `, [student_id, moduleoffered_id, marks_obtained, staff_id]);
            }
        }

        console.log('Marks data uploaded successfully!');
        return { success: true, message: 'Marks uploaded successfully!' };
    } catch (error) {
        console.error(error);
        return { success: false, message: 'Error uploading marks data!' };
    }
}


const addAllMarks = async (req, res) => {
    try {
        const { parsedData, moduleoffered_id, staff_id } = req.body; // Extract moduleoffered_id, staff_id, and parsedData from request body
        const modulesCompleted = await pool.query(
            'SELECT * FROM modulesoffereds WHERE moduleoffered_id = $1 AND (status = $2 OR status = $3)',
            [moduleoffered_id, 'Pending', 'Completed']
        );

        if (modulesCompleted.rows.length > 0) {
            return res.status(200).json({ success: false, message: 'You cannot make changes to this mark' });
        }
        const result = await uploadCSV(parsedData, moduleoffered_id, staff_id); // Pass moduleoffered_id and staff_id to uploadCSV function
        res.status(200).json(result); // Send the result to the frontend
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Error uploading file!' }); // Send error message to frontend
    }
};

  const notify = async (req, res) => {
    const { uncheckedMarks } = req.body;
    if (!uncheckedMarks || uncheckedMarks.length === 0) {
      return res.status(400).json({ message: 'No unchecked marks to notify.' });
    }
  
    const getUserEmailById = async (email) => {
      const userResult = await pool.query('SELECT email, username FROM users WHERE email = $1 AND role = $2', [email, 'Tutor']);
      return userResult.rows[0];
    };
  
    const getModuleNameByMarkId = async (marks_id) => {
      const moduleResult = await pool.query(`
        SELECT m.module_name 
        FROM marks mk
        JOIN modulesoffereds mo ON mk.modulesoffered_id = mo.moduleoffered_id
        JOIN modules m ON mo.module_id = m.module_id
        WHERE mk.marks_id = $1
      `, [marks_id]);
      return moduleResult.rows[0].module_name;
    };
  
    const emailsSent = uncheckedMarks.map(async (mark) => {
      try {
        const user = await getUserEmailById(mark.staff_email);
        const moduleName = await getModuleNameByMarkId(mark.marks_id);
  
        const mailOptions = {
          from: '"GCIT GPA MANAGEMENT" shyambasnet289@gmail.com', // Replace with your app name and email
          to: user.email,
          subject: 'Marks Verification Failed!',
          html:  `
          <p>Hi ${user.username},</p>
          <p>This email is to inform you that the marks for student ID <strong>${mark.student_id}</strong> in the module <strong>${moduleName}</strong> has failed the verification.</p>
          <p>They obtained <strong>${mark.marks_obtained}</strong> marks.</p>
          <p>Please take a moment to <strong>verify the marks</strong> for this student.</p>
        `
        };
  
        await transporter.sendMail(mailOptions);
        return { success: true };
      } catch (error) {
        console.error(`Error sending email to staff ID ${mark.staff_id}:`, error);
        return { success: false, error };
      }
    });
  
    const results = await Promise.all(emailsSent);
  
    if (results.every(result => result.success)) {
      res.json({ message: 'Notification emails sent successfully.' });
    } else {
      res.status(500).json({ message: 'Some emails failed to send.', results });
    }
  };
module.exports = {
  addMarks,
  getMarks,
  verifyMarks,
  getMarksVerifier,
  addAllMarks,
  notify
}
