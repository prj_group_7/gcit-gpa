const pool = require('../../db');

const addProgram = async (req,res) =>{
    try{
        const { programname,description, duration } = req.body;
        console.log(req.body)
        const status= 'Offered'
        const programExist= await pool.query('SELECT * FROM programs WHERE program_name=$1',[programname])
        if(programExist.rowCount >0){
            return res.status(201).json({success:false, message: 'Program already exists!'})
        }
        const intDuration = parseInt(duration);
        const studentQuery = `INSERT INTO programs (program_name, program_description,program_duration, status) VALUES ($1, $2, $3, $4)`;
            await pool.query(studentQuery, [programname,description,intDuration,status]);

            res.status(201).json({ success: true, message: 'Program added successfully' });
    }catch(error){
        console.log(error);
        res.status(500).json({ success: false, message: 'Error inserting program data' });
    }
}

const getProgram = async (req,res) => {
    try{
        const programdata = await pool.query('SELECT * FROM programs');
        res.status(200).json({ success: true, data: programdata.rows });
    }catch(error){
        console.log(error);
        res.status(500).json({success: false, message: 'Trouble Fetching Details'});
    }
}

const getOfferedProgram = async (req,res) => {
    try{
        const offereProgramdata= await pool.query ('SELECT * FROM programs WHERE status = $1',['Offered']);
        res.status(200).json({ success: true, data: offereProgramdata.rows });
    }catch(error){
        console.log(error);
        res.status(500).json({success: false, message: 'Trouble fetching Details'});
    }
}



const updateProgram = async (req,res) => {

}


module.exports = {
  addProgram,
  getProgram,
  updateProgram,
  getOfferedProgram,

}