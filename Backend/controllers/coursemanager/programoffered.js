const pool = require('../../db');

const getProgramOffered = async (req, res) => {
  try {
    // Join programoffereds and programs tables
    const programOfferedData = await pool.query(
      `SELECT po.*, p.program_name
      FROM programoffereds po
      INNER JOIN programs p ON po.program_id = p.program_id`
    );

    const offeredPrograms = programOfferedData.rows.map((row) => {
      // Create a new object with program name and existing data
      return {
        programoffered_id: row.programoffered_id,
        program_id: row.program_id,
        year: row.year,
        teamlead_id: row.teamlead_id,
        program_name: row.program_name, // Add program name
      };
    });

    res.status(200).json({ success: true, data: offeredPrograms });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Trouble fetching details' });
  }
};

const getProgramOfferedById = async (req, res) => {
  const { program_id } = req.params;

  try {
    // Join programoffereds and programs tables with a specific program_id
    const programOfferedData = await pool.query(
      `SELECT po.*, p.program_name
      FROM programoffereds po
      INNER JOIN programs p ON po.program_id = p.program_id
      WHERE po.programoffered_id = $1`,
      [program_id]
    );

    // Check if any data was returned
    if (programOfferedData.rows.length === 0) {
      return res.status(404).json({ success: false, message: 'Program not found' });
    }

    // Create a new object with program name and existing data
    const offeredProgram = {
      programoffered_id: programOfferedData.rows[0].programoffered_id,
      program_id: programOfferedData.rows[0].program_id,
      year: programOfferedData.rows[0].year,
      teamlead_id: programOfferedData.rows[0].teamlead_id,
      program_name: programOfferedData.rows[0].program_name,
    };

    res.status(200).json({ success: true, data: offeredProgram });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Trouble fetching details' });
  }
};

const addOfferedProgram = async (req,res) => {
    try{
        const { programid,year} = req.body;
        console.log(programid);
        const intYear = parseInt(year);
        console.log(intYear)
        const result= await pool.query('SELECT * FROM programoffereds WHERE program_id =$1 AND year = $2',[programid,year]);
        if(result.rowCount>0){
          return res.status(201).json({success:false, message: 'Program is already offered!'})
        }

        const studentQuery = `INSERT INTO programoffereds (program_id, year) VALUES ($1, $2)`;
        await pool.query(studentQuery, [programid,intYear]);

        res.status(201).json({ success: true, message: 'Program offered successfully' });
}catch(error){
    console.log(error);
    res.status(500).json({ success: false, message: 'Error offering  program' });
    }
}

const getOfferedProgramDetail = async (req, res) => {
  try {
      const offereProgramdetaildata = await pool.query (`
      SELECT
        po.programoffered_id,
        po.year,
        u.username AS teamlead_name,
        p.program_name,
        COUNT(DISTINCT s.student_id) AS active_student_count,
        COUNT(DISTINCT mo.moduleoffered_id) AS total_modules_offered
      FROM
        programoffereds po
      JOIN
        users u ON po.teamlead_id = u.user_id
      JOIN
        programs p ON po.program_id = p.program_id
      LEFT JOIN
        students s ON po.programoffered_id = s.programoffered_id
      LEFT JOIN
        modulesoffereds mo ON po.programoffered_id = mo.programoffered_id
      WHERE
        po.year >= EXTRACT(YEAR FROM (DATE_TRUNC('year', CURRENT_DATE) - INTERVAL '6 months')) - 3
      GROUP BY
        po.programoffered_id, po.year, u.username, p.program_name
      ORDER BY
        po.year DESC, u.username;
    `);
      res.status(200).json({ success: true, data: offereProgramdetaildata.rows });
      console.log(offereProgramdetaildata.rows)
      } catch (error) {
          console.log(error);
          res.status(500).json({success: false, menubar: 'Trouble Fetching Details'})
          }
}

module.exports = {
    addOfferedProgram,
    getProgramOffered,
    getOfferedProgramDetail,
    getProgramOfferedById
}