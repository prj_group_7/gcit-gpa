const pool = require('../../db');

const addModule = async (req,res) =>{
    try{
        const { modulename, modulecode, modulecredit, description } = req.body;
        const intCredits = parseInt(modulecredit);
        
        const checkmodule= await pool.query(`SELECT * FROM modules WHERE module_name=$1 AND module_code=$2`,[modulename,modulecode])
        if(checkmodule.rowCount > 0){
            return res.status(201).json({success:false, message: 'Module already exists!'})
        }
        // console.log('Module Credits', intCredits, typeof(intCredits))
        const status = 'Offered';
        const studentQuery = `INSERT INTO modules (module_name,module_code,module_description,credits, status) VALUES ($1, $2, $3, $4,$5)`;
            await pool.query(studentQuery, [modulename,modulecode, description,intCredits, status]);

            res.status(201).json({ success: true, message: 'Module added successfully' });
    }catch(error){
        console.log(error);
        res.status(500).json({ success: false, message: 'Error inserting module data' });
    }
}

const getModule = async (req,res) => {
    try{
        const moduledata = await pool.query('SELECT * FROM modules');
        res.status(200).json({ success: true, data: moduledata.rows });
    }catch(error){
        console.log(error);
        res.status(500).json({success: false, message: 'Trouble Fetching Details'});
    }
}

const updateModule = async (req,res) => {

}


module.exports = {
    addModule,
    getModule,
    updateModule,
}