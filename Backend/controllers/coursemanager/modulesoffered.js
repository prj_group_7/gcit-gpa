const pool = require('../../db');
const transporter = require('../../email.js');

const addToModulesOffered = async (req, res) => {
  try {
    const { programLeadId, selectedSemester, sections, programofferedId } = req.body;

    //validation
    for (const section of sections) {

      //validation empty fields
      if (section.formData.filter(f => f.moduleId || f.tutorId || f.verifierId).filter(f => !f.moduleId || !f.tutorId || !f.verifierId).length > 0) {
        return res.status(400).json({ message: `Some fields are left emptied.` });
      }

      // unique module and tutor
      for (const formData of section.formData) {
        const { moduleId, tutorId, verifierId } = formData;

        if (section.formData.filter(f => f.moduleId === moduleId).length > 1 && moduleId) {
          return res.status(400).json({ message: `Modules offered should be unique.` });
        }

        // if (section.formData.filter(f => f.tutorId === tutorId).length > 1 && tutorId) {
        //   return res.status(400).json({ message: `Tutors should be unique.` });
        // }
      }
    }

    console.log(selectedSemester);
    const client = await pool.connect();

    try {
      await client.query('BEGIN'); // Start transaction

      // Update ProgramOffered table with programLeadId
      await client.query('UPDATE programoffereds SET teamlead_id = $1 WHERE programoffered_id = $2', [programLeadId, programofferedId]);
      const semesterNumber = parseInt(selectedSemester.slice(-1), 10);

      // Delete existing records for the given programoffered_id and semester_id
      const deleteQuery = 'DELETE FROM modulesoffereds WHERE programoffered_id = $1 AND semester_id = $2';
      await client.query(deleteQuery, [programofferedId, semesterNumber]);

      // Insert records into ModuleOffered table for each section's formData
      for (const section of sections) {
        for (const formData of section.formData) {
          const { moduleId, tutorId, verifierId } = formData;

          if (!moduleId){
            continue
          }
          
          //finding mr students
          const mrrecords = await client.query(`SELECT 
                    m.marks_id,
                    m.student_id
                    FROM 
                    marks m
                    JOIN 
                    students s ON m.student_id = s.student_id
                    JOIN 
                    modulesoffereds mo ON m.modulesoffered_id = mo.moduleoffered_id
                    WHERE 
                    m.grade = 'F'
                    AND m.clear=false 
                    AND mo.module_id = $1
                    AND mo.semester_id > $2`, [moduleId, semesterNumber]);
          const mrStudents = mrrecords.rows.map(row => row.student_id);
          const insertQuery = `
                        INSERT INTO modulesoffereds (module_id, programoffered_id, semester_id, module_verifier, tutor, mr_students) 
                        VALUES ($1, $2, $3, $4, $5, $6)
                    `;
          await client.query(insertQuery, [moduleId, programofferedId, semesterNumber, verifierId, tutorId, mrStudents]);
        }
      }
      await client.query('COMMIT'); // Commit transaction
      res.status(200).json({ message: 'Data saved successfully' });
    } catch (error) {
      await client.query('ROLLBACK'); // Rollback transaction
      throw error;
    } finally {
      client.release(); // Release client back to the pool
    }
  } catch (error) {
    console.error('Error saving data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}

const getModulesForTutors = async (req, res) => {
  try {
    const { user_id } = req.body.params;

    const result = await pool.query("SELECT * FROM modulesoffereds WHERE tutor_id = $1", [user_id]);

    res.status(201).json({ data: result.rows });
  } catch (e) {
    console.log(e);
    res.status(500).json({ error: 'Internal server error' });
  }
}

const getModulesOffered = async (req, res) => {
  try {
    const { programofferedId, selectedSemester } = req.query;
    console.log(req.query);
    const semesterNumber = parseInt(selectedSemester.slice(-1), 10);

    const result = await pool.query(
      `SELECT mo.*, u.username AS team_lead_name
          FROM modulesoffereds mo
          LEFT JOIN users u ON mo.module_verifier = u.user_id
          WHERE mo.programoffered_id = $1 AND mo.semester_id = $2`,
      [programofferedId, semesterNumber]
    );

    res.status(200).json({ data: result.rows });
    console.log(result.rows)
  } catch (error) {
    console.error('Error fetching data:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

const getStudents = async (req, res) => {
  const { moduleoffered_id } = req.query;

  const query = `
    SELECT s.student_id, s.name, COALESCE(m.marks_obtained, 0) AS marks_obtained
    FROM students s
    LEFT JOIN marks m ON s.student_id = m.student_id AND m.modulesoffered_id = $1
    JOIN programoffereds p ON s.programoffered_id = p.programoffered_id
    JOIN modulesoffereds mo ON p.programoffered_id = mo.programoffered_id AND s.enrolled_semester = mo.semester_id
    WHERE mo.moduleoffered_id = $1;
  `;

  try {
    const result = await pool.query(query, [moduleoffered_id]);
    res.json(result.rows);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};

const approve = async (req, res) => {
  const { selectedModule } = req.body;
  try {
    // Check if there are unverified marks
    const unverifiedMark = await pool.query('SELECT * FROM marks WHERE verified = $1 AND modulesoffered_id = $2', [false, selectedModule]);
    if (unverifiedMark.rowCount > 0) {
      return res.status(200).json({ success: false, message: 'Please verify all the marks' });
    }

    // Fetch all marks for the selected module
    const marks = await pool.query('SELECT * FROM marks WHERE modulesoffered_id = $1', [selectedModule]);

    // Define the grade criteria
    const gradeCriteria = [
      { min: 80, gpa: 4.0, grade: 'A' },
      { min: 75, gpa: 3.5, grade: 'B+' },
      { min: 70, gpa: 3.0, grade: 'B' },
      { min: 65, gpa: 2.5, grade: 'C+' },
      { min: 60, gpa: 2.0, grade: 'C' },
      { min: 55, gpa: 1.5, grade: 'D+' },
      { min: 50, gpa: 1.0, grade: 'D' },
      { min: 0, gpa: 0.0, grade: 'F' }
    ];

    // Update GPA, Grade, and Clear status for each mark
    for (let row of marks.rows) {
      let gpa = 0.0;
      let grade = 'F';
      let clear = true;
      for (let criteria of gradeCriteria) {
        if (row.marks_obtained >= criteria.min) {
          gpa = criteria.gpa;
          grade = criteria.grade;
          break;
        }
      }

      // Set clear to false if the grade is F
      if (grade === 'F') {
        clear = false;
      }

      // Update the marks table with calculated GPA, grade, and clear status
      await pool.query(
        'UPDATE marks SET gradepoint = $1, grade = $2, clear = $3 WHERE marks_id = $4',
        [gpa, grade, clear, row.marks_id]
      );
    }

    // Update the module status to 'Pending'
    const result = await pool.query('UPDATE modulesoffereds SET status = $1 WHERE moduleoffered_id = $2 AND status IS NULL', ['Pending', selectedModule]);

    if (result.rowCount === 1) {
      return res.status(200).json({ success: true, message: 'Marks sent for compilation.' });
    } else {
      return res.status(404).json({ success: false, message: 'Failed to send the marks' });
    }
  } catch (error) {
    console.error('Error updating module status:', error);
    return res.status(500).json({ success: false, message: 'Internal server error. Failed to update module status.' });
  }
};

module.exports = { approve };


const compileModulesOffered = async (req, res) => {
  const query = `
    SELECT 
      mo.moduleoffered_id,
      m.module_code,
      p.program_name,
      po.year,
      mo.semester_id
    FROM 
      modulesoffereds mo
    JOIN 
      modules m ON mo.module_id = m.module_id
    JOIN 
      programoffereds po ON mo.programoffered_id = po.programoffered_id
    JOIN 
      programs p ON po.program_id = p.program_id
    WHERE 
      mo.status = 'Pending';
  `;

  try {
    const result = await pool.query(query);
    res.status(200).json(result.rows);
    console.log(result.rows)
  } catch (error) {
    console.error('Error fetching pending modules offered:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}

const compileOne = async (req, res) => {
  const { moduleoffered_id } = req.body;
  console.log(req.body);
  console.log(moduleoffered_id);
  try {
    // Update module status to 'Completed'
    const moduleResult = await pool.query('UPDATE modulesoffereds SET status = $1 WHERE moduleoffered_id = $2', ['Completed', moduleoffered_id]);

    if (moduleResult.rowCount > 0) {
      // Fetch programoffered_id from modulesoffereds
      const programOfferedQuery = 'SELECT programoffered_id FROM modulesoffereds WHERE moduleoffered_id = $1';
      const programOfferedResult = await pool.query(programOfferedQuery, [moduleoffered_id]);
      const programOfferedId = programOfferedResult.rows[0].programoffered_id;

      // Update enrolled_semester for students associated with the programoffered_id
      const studentUpdateResult = await pool.query('UPDATE students SET enrolled_semester = enrolled_semester + 1 WHERE programoffered_id = $1', [programOfferedId]);

      if (studentUpdateResult.rowCount > 0) {
        res.status(200).json({ success: true, message: "Marks compiled successfully and student enrolled semester updated" });
      } else {
        res.status(200).json({ success: false, message: "Marks compiled successfully but failed to update student enrolled semester" });
      }
    } else {
      res.status(200).json({ success: false, message: "Couldn't compile the marks" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: 'Internal server error' });
  }
};


const compileAllModules = async (req, res) => {
  try {
    // Query to get all active students
    const activeStudentsResult = await pool.query(`
      SELECT DISTINCT s.student_id, mo.moduleoffered_id
      FROM students s
      JOIN modulesoffereds mo ON s.programoffered_id = mo.programoffered_id AND s.enrolled_semester = mo.semester_id
    `);

    const activeModules = new Set(activeStudentsResult.rows.map(row => row.moduleoffered_id));

    // Query to count modules with 'Pending' status
    const pendingModulesResult = await pool.query(`
      SELECT moduleoffered_id
      FROM modulesoffereds
      WHERE status = 'Pending'
    `);

    const pendingModules = new Set(pendingModulesResult.rows.map(row => row.moduleoffered_id));

    // Check if every active module is in the pendingModules set
    for (const moduleId of activeModules) {
      if (!pendingModules.has(moduleId)) {
        return res.status(200).json({ success: false, message: "Cannot compile all modules because not all modules have been marked as pending." });
      }
    }

    // Update status of modules to 'Completed'
    const moduleResult = await pool.query(
      'UPDATE modulesoffereds SET status = $1 WHERE status = $2 RETURNING *',
      ['Completed', 'Pending']
    );

    // Update enrolled_semester for all students
    await pool.query(
      'UPDATE students SET enrolled_semester = enrolled_semester + 1'
    );

    if (moduleResult.rowCount > 0) {
      // Fetch all student emails
      const studentsResult = await pool.query('SELECT email FROM users');

      // Send notification emails to all students
      for (let student of studentsResult.rows) {
        const mailOptions = {
          from: '"GCIT GPA MANAGEMENT" <shyamabasnet289@gmail.com>', // Replace with your app name and email
          to: student.email,
          subject: 'Result Declared!',
          text: `Hello everyone, the results for the last semester are now out. Please log in to the GCIT GPA website to check your results.`
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.error(`Error sending email to ${student.email}:`, error);
          }
          console.log(`Email sent to ${student.email}: ${info.response}`);
        });
      }

      res.status(200).json({ success: true, message: "All pending modules compiled successfully and notifications sent.", data: moduleResult.rows });
    } else {
      res.status(200).json({ success: false, message: "No pending modules found" });
    }
  } catch (err) {
    console.error('Error compiling all modules:', err);
    res.status(500).json({ success: false, message: 'Internal server error' });
  }

}

module.exports = {
  addToModulesOffered,
  getModulesForTutors,
  getModulesOffered,
  getStudents,
  approve,
  compileModulesOffered,
  compileOne,
  compileAllModules
};
