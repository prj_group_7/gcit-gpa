const pool = require('../../db.js');
const transporter = require('../../email.js');
const { generateRandomPassword } = require('../../codeSnippet.js')
const bcrypt = require('bcryptjs');


async function uploadCSV(parsedData) {
    try {
        const userInserts = [];

        for (const row of parsedData) {
            // Prepare user data
            const password = generateRandomPassword()
            const user = {
                username: row[1],
                email: row[2],
                password: password,
                hashedPassword : await bcrypt.hash(password, 10),
                gender: row[4],
                role: row[3]
            };

            userInserts.push(user);
        }

        // Insert users first
        
        for (const user of userInserts) {
            console.log(user)
            const query = `INSERT INTO users (username, email, password, role,gender) VALUES ($1, $2, $3, $4,$5) RETURNING user_id`;
            const result = await pool.query(query, [user.username, user.email, user.hashedPassword, user.role, user.gender]);
            
            const mailOptions = {
                from: '"GCIT GPA MANAGEMENT" shyamabasnet289@gmail.com', // Replace with your app name and email
                to: user.email,
                subject: 'Welcome to our app!',
                text: `Hi ${username}, Welcome to our website!. This is your login credetinals Email : ${user.email} and Password: ${user.password}. Please login to change your password.`
            };

            try {
                const info = transporter.sendMail(mailOptions);
                console.log('Email sent:', 'Email Sent Successfully');
            } catch (error) {
                console.error('Error sending email:', error);
            }
        
        }

        console.log('Data uploaded successfully!');
        return { success: true, message: 'Data uploaded successfully!' };
    } catch (error) {
        console.error(error);
        return { success: false, message: 'Error uploading data!' };
    }
}
const addAll = async (req, res) => {
    try {
        const parsedData = req.body; // Assuming the parsed CSV data is sent in the request body
        const result = await uploadCSV(parsedData);
        res.status(200).json(result); // Send the result to the frontend
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Error uploading file!' }); // Send error message to frontend
    }
};

const addUser = async (req, res) => {
    const { username, email, role } = req.body;
    const password = generateRandomPassword();

    // Check if the email already exists in the database
    const existingUser = await pool.query('SELECT * FROM users WHERE email = $1', [email]);

    if (existingUser.rows.length > 0) {
        // If the email already exists, send an error response to the frontend
        return res.status(400).json({ success: false, message: 'Email already exists' });
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    // If the email is unique, insert the new user into the database
    await pool.query(
        'INSERT INTO users (username, email, password, role) VALUES ($1, $2, $3, $4)',
        [username, email, hashedPassword, role],
        (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                return res.status(500).json({ success: false, message: 'Error inserting user data' });
            }
            console.log('Data inserted successfully:', results.rowCount);
            const mailOptions = {
                from: '"GCIT GPA MANAGEMENT" shyamabasnet289@gmail.com', // Replace with your app name and email
                to: email,
                subject: 'Welcome to our app!',
                text: `Hi ${username}, Welcome to our website!. This is your login credetinals Email : ${email} and Password: ${password}. Please login to change your password.`
            };

            try {
                const info = transporter.sendMail(mailOptions);
                console.log('Email sent:', 'Email Sent Successfully');
            } catch (error) {
                console.error('Error sending email:', error);
            }
            return res.status(201).json({ success: true, message: 'User data inserted successfully' });
        }
    );
}

const getAllUsers = async (req, res) => {
    try {
        const results = await pool.query('SELECT * FROM users WHERE role != $1 AND role !=$2', ['Student', 'admin']);
        res.status(200).json({ success: true, data: results.rows });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: 'Error retrieving users' });
    }
}

const getTutors = async (req, res) => {
    try {
        const results = await pool.query('SELECT * FROM users WHERE role = $1', ['Tutor']);
        res.status(200).json({ success: true, data: results.rows });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: 'Error retrieving users' });
    }
}

const getUsersWithRole = async (req, res) => {
    try {
        const result = await pool.query("SELECT DISTINCT role FROM users WHERE role != 'admin'");
        res.json(result.rows);
        console.log(result.rows)
    } catch (error) {
        console.error(error);
        res.status(500).send('Server error');
    }
}

const getUserfromrole = async (req, res) => {
    const role = req.params.role;
    try {
        const result = await pool.query('SELECT * FROM users WHERE role = $1', [role]);
        res.json(result.rows);
        console.log(result.rows)
    } catch (error) {
        console.error(error);
        res.status(500).send('Server error');
    }
}

const forgotPassword = async (req, res) => {
    const { email } = req.body;
    const password = generateRandomPassword();

    // Check if the email already exists in the database
    const existingUser = await pool.query('SELECT * FROM users WHERE email = $1', [email]);

    if (existingUser.rows.length === 0) {
        return res.status(400).json({ success: false, message: "Please enter a registered email!" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    // If the email is unique, insert the new user into the database
    await pool.query(
        'UPDATE users SET password = $1 WHERE user_id = $2',
        [hashedPassword, existingUser.rows[0].user_id],
        (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                return res.status(500).json({ success: false, message: 'Error updating user password' });
            }
            console.log('Password updated successfully:', results.rowCount);
            const mailOptions = {
                from: '"GCIT GPA MANAGEMENT" shyamabasnet289@gmail.com', // Replace with your app name and email
                to: email,
                subject: 'Password Reset',
                text: `Hi ${existingUser.rows[0].username}, Your password has been reset. Your new password is: ${password}`
            };

            try {
                const info = transporter.sendMail(mailOptions);
                console.log('Email sent:', 'Email Sent Successfully');
            } catch (error) {
                console.error('Error sending email:', error);
            }
            return res.status(201).json({ success: true, message: 'Your password has been reset.' });
        }
    );
}

const changePassword = async (req, res) => {
    const { user_id, currentPassword, newPassword, confirmPassword } = req.body;

    try {
        // Check if the user exists in the database
        const user = await pool.query('SELECT * FROM users WHERE user_id = $1', [user_id]);

        if (user.rows.length === 0) {
            return res.status(404).json({ success: false, message: 'User not found' });
        }

        const userData = user.rows[0];

        // Check if the current password matches the stored password
        const isMatch = await bcrypt.compare(currentPassword, userData.password);

        if (!isMatch) {
            return res.status(400).json({ success: false, message: 'Current password is incorrect' });
        }

        // Check if the new password matches the confirm password
        if (newPassword !== confirmPassword) {
            return res.status(400).json({ success: false, message: 'New password and confirm password do not match' });
        }

        // Hash the new password
        const hashedPassword = await bcrypt.hash(newPassword, 10);

        // Update the user's password in the database
        await pool.query(
            'UPDATE users SET password = $1 WHERE user_id = $2',
            [hashedPassword, user_id]
        );

        return res.status(200).json({ success: true, message: 'Password changed successfully' });
    } catch (error) {
        console.error('Error changing password:', error);
        return res.status(500).json({ success: false, message: 'An error occurred while changing the password' });
    }
};


module.exports = {
    addUser,
    getAllUsers,
    getTutors,
    addAll,
    getUsersWithRole,
    getUserfromrole,
    forgotPassword,
    changePassword
}