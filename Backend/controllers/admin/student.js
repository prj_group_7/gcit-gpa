const Papa = require('papaparse');
const pool = require('../../db.js');
const transporter = require('../../email.js');
const { generateRandomPassword } = require('../../codeSnippet.js');
const bcrypt = require('bcryptjs');


const updateStatus = async (req, res) => {
  const { user_id } = req.params; // Corrected extraction
  try {
    const { status } = req.body;

    if (!status) {
      return res.status(400).json({ success: false, message: 'Please select a status' });
    }

    const response = await pool.query('SELECT * FROM students WHERE user_id=$1', [user_id]);
    if (response.rowCount > 0) {
      const studentId = response.rows[0].student_id;
      await pool.query('UPDATE students SET status=$1 WHERE student_id=$2', [status, studentId]);
    }
    await pool.query('UPDATE users SET status=$1 WHERE user_id=$2', [status, user_id]);

    res.status(200).json({ success: true, message: 'Status updated successfully' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: 'Some error occurred' });
  }
};


async function uploadCSV(parsedData) {
    try {
        const userInserts = [];
        const studentInserts = [];


        for (const row of parsedData) {
            // Generate email using studentId
            const studentId = row[1];
            const email = `${studentId}.gcit@rub.edu.bt`;
            const password=generateRandomPassword()

            // Prepare user data
            const user = {
                username: row[2],
                email: email,
                password: password,
                hashedPassword : await bcrypt.hash(password, 10),
                role: 'Student'
            };

            // Prepare student data
            const student = {
                student_id: row[1],
                username: row[2],
                address: row[5],
                phone: row[8],
                type: row[10],
                date_of_birth: row[7],
                gender: row[4],
                enrolled_semester: row[3],
                year_of_enrollment: row[11],
                program_name: row[9] // Added program_name from CSV
            };
            userInserts.push(user);
            studentInserts.push(student);
        }

        // Insert users first
        for (const user of userInserts) {
            const query = `INSERT INTO users (username, email, password, role) VALUES ($1, $2, $3, $4) RETURNING user_id`;
            const result = await pool.query(query, [user.username, user.email, user.hashedPassword, user.role]);
            const userId = result.rows[0].user_id;
            const username= user.username;
            const email= user.email;
            const password=user.password;
            console.log(password)
           
           
            const mailOptions = {
                from: '"GCIT GPA MANAGEMENT" shyamabasnet289@gmail.com', // Replace with your app name and email
                to: email,
                subject: 'Welcome to our app!',
                text: `Hi ${username}, Welcome to our website!. This is your login credetinals Email : ${email} and Password: ${password}. Please login to change your password.`
              };
              transporter.sendMail(mailOptions);
              console.log('Email sent successfully', email)

            
            // Find corresponding student and set user_id
            const student = studentInserts.find(student => student.username === user.username);
            student.user_id = userId;
        }

        // Insert students with foreign keys
        for (const student of studentInserts) {
            // Retrieve program_id using program_name
            const programQuery = `SELECT program_id FROM programs WHERE program_name = $1`;
            const programResult = await pool.query(programQuery, [student.program_name]);
            const programId = programResult.rows[0].program_id;

            console.log(programId)
           
           console.log(student.year_of_enrollment);
            
            // Retrieve programoffered_id using program_id and year_of_enrollment
            const programOfferedQuery = `SELECT programoffered_id FROM programoffereds WHERE program_id = $1 AND year = $2`;
            const programOfferedResult = await pool.query(programOfferedQuery, [programId, parseInt(student.year_of_enrollment)]);
            const programOfferedId = programOfferedResult.rows[0].programoffered_id;
            console.log(programOfferedId)
            console.log(programOfferedResult.rows[0])

            // Insert student with foreign keys
            const studentQuery = `INSERT INTO students (student_id, user_id, name, address, phone, type, date_of_birth, gender, year_of_enrollment, programoffered_id, enrolled_semester) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,$11)`;
            await pool.query(studentQuery, [student.student_id, student.user_id, student.username, student.address, student.phone, student.type, student.date_of_birth, student.gender, student.year_of_enrollment, programOfferedId, student.enrolled_semester]);
        }

        console.log('Data uploaded successfully!');
        return { success: true, message: 'Data uploaded successfully!' };
    } catch (error) {
        console.error(error);
        return { success: false, message: 'Error uploading data!' };
    }
}


const addStudents = async (req, res) => {
    try {
        const parsedData = req.body; // Assuming the parsed CSV data is sent in the request body
        const result = await uploadCSV(parsedData);
        res.status(200).json(result); // Send the result to the frontend
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Error uploading file!' }); // Send error message to frontend
    }
};


const getStudents = async (req, res) => {
    try {
        const results = await pool.query(`
            SELECT 
                s.*, 
                po.programoffered_id, 
                p.program_name,
                s.status
            FROM 
                students s 
            INNER JOIN 
                programoffereds po ON s.programoffered_id = po.programoffered_id 
            INNER JOIN 
                programs p ON po.program_id = p.program_id
            ORDER BY 
                s.student_id
        `);
        res.status(200).json({ success: true, data: results.rows });
    } catch (err) {
        console.error(err);
        res.status(500).json({ success: false, message: 'Error retrieving students' });
    }
};

const getFirstYearStudents = async (req, res) => {
    const query = `
      SELECT
        s.student_id,
        s.name AS student_name,
        s.address,
        s.phone,
        s.date_of_birth,
        s.gender,
        s.year_of_enrollment,
        p.program_name,
        s.enrolled_semester
      FROM
        students s
      JOIN
        programoffereds po ON s.programoffered_id = po.programoffered_id
      JOIN
        programs p ON po.program_id = p.program_id
      WHERE
        s.enrolled_semester = 3
        AND p.program_name != 'Bachelor of Digital Media & Development'
    `;
  
    try {
      const result = await pool.query(query);
      console.log(result.rows)
      res.json(result.rows);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  };

  const getStudentMarks = async (req, res) => {
    const { user_id, semester_id } = req.params;
  
    try {
      // First, fetch the student_id using the user_id
      const studentQuery = 'SELECT student_id FROM students WHERE user_id = $1';
      const studentResult = await pool.query(studentQuery, [user_id]);
  
      if (studentResult.rowCount === 0) {
        return res.status(404).json({ success: false, message: 'Student not found' });
      }
  
      const { student_id } = studentResult.rows[0];
  
      // Then, fetch the marks using the student_id and semester_id
      const marksQuery = `
        SELECT 
          m.marks_obtained,
          m.gradepoint,
          m.grade,
          mod.module_code,
          mod.module_name,
          CASE
            WHEN m.grade = 'A' THEN 'Excellent'
            WHEN m.grade = 'B+' THEN 'Very Good'
            WHEN m.grade = 'B' THEN 'Good'
            WHEN m.grade = 'C+' THEN 'Above Average'
            WHEN m.grade = 'C' THEN 'Average'
            WHEN m.grade = 'D+' THEN 'Below Average'
            WHEN m.grade = 'D' THEN 'Poor'
            ELSE 'Fail'
          END AS remark
        FROM marks m
        JOIN modulesoffereds mo ON m.modulesoffered_id = mo.moduleoffered_id
        JOIN modules mod ON mo.module_id = mod.module_id
        WHERE m.student_id = $1 AND mo.status = 'Completed' AND mo.semester_id = $2;
      `;
      console.log(student_id, semester_id)
      const marksResult = await pool.query(marksQuery, [student_id, semester_id]);
    //   console.log(marksResult)
  
      res.status(200).json({ success: true, data: marksResult.rows });
      console.log(marksResult.rows)
    } catch (error) {
      console.error('Error fetching student marks:', error);
      res.status(500).json({ success: false, message: 'Internal server error.' });
    }
}
  
const getGrade = (gpa) => {
    if (gpa >= 4.0) {
      return 'A';
    } else if (gpa >= 3.5) {
      return 'B+';
    } else if (gpa >= 3.0) {
      return 'B';
    } else if (gpa >= 2.5) {
      return 'C+';
    } else if (gpa >= 2.0) {
      return 'C';
    } else if (gpa >= 1.5) {
      return 'D+';
    } else if (gpa >= 1.0) {
      return 'D';
    } else if (gpa < 1.0){
        return 'F'
    } else {
      return 'F';
    }
  };
  
  const getStudentGPA = async (req, res) => {
    const user_id = req.params.user_id;
  
    try {
      // Fetch student_id using user_id
      const studentQuery = `
        SELECT student_id
        FROM students
        WHERE user_id = $1;
      `;
      const { rows: students } = await pool.query(studentQuery, [user_id]);
      if (students.length === 0) {
        return res.status(404).json({ error: 'Student not found' });
      }
      const studentId = students[0].student_id;
  
      // Fetch marks data for the student for modules with status 'Completed'
      const marksQuery = `
        SELECT mo.semester_id, mod.credits, m.gradepoint, m.clear, mo.module_id
        FROM marks m
        INNER JOIN modulesoffereds mo ON m.modulesoffered_id = mo.moduleoffered_id
        INNER JOIN modules mod ON mo.module_id = mod.module_id
        WHERE m.student_id = $1 AND mo.status = 'Completed';
      `;
      const { rows: marks } = await pool.query(marksQuery, [studentId]);
  
      // Initialize structures to hold GPA and credit data
      const gpaBySemester = {};
      const semesterCredits = {};
      const semesterGPA = {};
  
      // Process marks data to calculate GPA and handle repeat modules
      const processedModules = new Map();
  
      for (const mark of marks) {
        const { semester_id, credits, gradepoint, clear, module_id } = mark;
  
        if (!gpaBySemester[semester_id]) {
          gpaBySemester[semester_id] = 0;
          semesterCredits[semester_id] = 0;
        }
  
        if (!processedModules.has(module_id)) {
          processedModules.set(module_id, []);
        }
        processedModules.get(module_id).push({ clear, gradepoint, credits, semester_id });
      }
  
      for (const [moduleId, entries] of processedModules.entries()) {
        let hasFalseClear = entries.some(entry => !entry.clear);
        for (const entry of entries) {
          const { semester_id, credits, gradepoint, clear } = entry;
          if (clear && hasFalseClear) {
            gpaBySemester[semester_id] += credits * 1.0;  // Use 1.0 gradepoint for cleared modules with a false clear record
          } else {
            gpaBySemester[semester_id] += credits * gradepoint;  // Use actual gradepoint
          }
          semesterCredits[semester_id] += credits;
        }
      }
  
      for (const semesterId in gpaBySemester) {
        semesterGPA[semesterId] = semesterCredits[semesterId] !== 0 ? gpaBySemester[semesterId] / semesterCredits[semesterId] : 0;
      }
  
      // Calculate CGPA
      const cgpa = {};
      let totalGPA = 0;
      let semesterCount = 0;
  
      for (const semesterId in semesterGPA) {
        totalGPA += semesterGPA[semesterId];
        semesterCount++;
  
        cgpa[semesterId] = (totalGPA / semesterCount).toFixed(2);
      }
  
      // Assign letter grades to semesters
      const getGrade = (gpa) => {
        if (gpa >= 4.0) return 'A';
        if (gpa >= 3.5) return 'B+';
        if (gpa >= 3.0) return 'B';
        if (gpa >= 2.5) return 'C+';
        if (gpa >= 2.0) return 'C';
        if (gpa >= 1.5) return 'D+';
        if (gpa >= 1.0) return 'D';
        return 'F';
      };
  
      const semesterGrades = {};
      for (const semesterId in semesterGPA) {
        const gpa = semesterGPA[semesterId];
        semesterGrades[semesterId] = getGrade(gpa);
      }
  
      // Send the data to the frontend
      const responseData = [];
      for (let i = 1; i <= 8; i++) {
        responseData.push({
          semester: i,
          gpa: semesterGPA[i] || 0,
          cgpa: cgpa[i] || 0,
          grade: semesterGrades[i] || ''
        });
      }
  
      res.json(responseData);
    } catch (error) {
      console.error('Error fetching GPA and CGPA:', error);
      res.status(500).json({ error: 'An error occurred' });
    }
  };
  
  
  
  
  

  async function updateCSV(parsedData) {
    try {
        for (const row of parsedData) {
            // Prepare student data
            const student_id = row[1];
            const enrolled_semester = row[3];
            const year_of_enrollment = parseInt(row[11]) + 1;
            const program_name = row[9];
            

            // Get the programoffered_id
            const programOfferedQuery = `
                SELECT po.programoffered_id
                FROM programoffereds po
                JOIN programs p ON po.program_id = p.program_id
                WHERE p.program_name = $1 AND po.year = $2
            `;
            const programOfferedResult = await pool.query(programOfferedQuery, [program_name, year_of_enrollment]);
          
            if (programOfferedResult.rows.length > 0) {
                const programofferedId = programOfferedResult.rows[0].programoffered_id;
                console.log(programofferedId);
                console.log(student_id)
                // Check if student already exi
                
                    // Update student record if already exists
                    await pool.query(`
                        UPDATE students
                        SET programoffered_id = $1
                        WHERE student_id = $2
                    `, [programofferedId, student_id]);
                
                    console.log('Student data updated successfully!');
               
            }
        }

        
        return { success: true, message: 'Data updated successfully!' };
    } catch (error) {
        console.error(error);
        return { success: false, message: 'Error updating data!' };
    }
}

const updateStudent = async (req, res) => {
    try {
        const parsedData = req.body; // Assuming the parsed CSV data is sent in the request body
        const result = await updateCSV(parsedData);
        res.status(200).json(result); // Send the result to the frontend
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Error uploading file!' }); // Send error message to frontend
    }
};
  

const getStudentDashboardData = async(req,res)=>{
  const user_id = req.params.user_id;

  try {
    // Fetch student_id using user_id
    const studentQuery = `
      SELECT student_id
      FROM students
      WHERE user_id = $1;
    `;
    const { rows: students } = await pool.query(studentQuery, [user_id]);
    if (students.length === 0) {
      return res.status(404).json({ error: 'Student not found' });
    }
    const studentId = students[0].student_id;

    // Fetch GPA data
    const gpaQuery = `
      SELECT mo.semester_id, mod.credits, m.gradepoint, m.clear, mo.module_id
      FROM marks m
      INNER JOIN modulesoffereds mo ON m.modulesoffered_id = mo.moduleoffered_id
      INNER JOIN modules mod ON mo.module_id = mod.module_id
      WHERE m.student_id = $1 AND mo.status = 'Completed';
    `;
    const { rows: marks } = await pool.query(gpaQuery, [studentId]);

    // Initialize structures to hold GPA and credit data
    const gpaBySemester = {};
    const semesterCredits = {};
    const semesterGPA = {};
    const processedModules = new Map();

    let completedModules = 0;
    let repeatedModules = 0;

    // Process marks data to calculate GPA and handle repeat modules
    for (const mark of marks) {
      const { semester_id, credits, gradepoint, clear, module_id } = mark;

      if (!gpaBySemester[semester_id]) {
        gpaBySemester[semester_id] = 0;
        semesterCredits[semester_id] = 0;
      }

      if (!processedModules.has(module_id)) {
        processedModules.set(module_id, []);
      }
      processedModules.get(module_id).push({ clear, gradepoint, credits, semester_id });

      if (clear) {
        completedModules++;
      } else {
        repeatedModules++;
      }
    }

    for (const [moduleId, entries] of processedModules.entries()) {
      let hasFalseClear = entries.some(entry => !entry.clear);
      for (const entry of entries) {
        const { semester_id, credits, gradepoint, clear } = entry;
        if (clear && hasFalseClear) {
          gpaBySemester[semester_id] += credits * 1.0;  // Use 1.0 gradepoint for cleared modules with a false clear record
        } else {
          gpaBySemester[semester_id] += credits * gradepoint;  // Use actual gradepoint
        }
        semesterCredits[semester_id] += credits;
      }
    }

    for (const semesterId in gpaBySemester) {
      semesterGPA[semesterId] = semesterCredits[semesterId] !== 0 ? gpaBySemester[semesterId] / semesterCredits[semesterId] : 0;
    }

    // Calculate CGPA
    const cgpa = {};
    let totalGPA = 0;
    let semesterCount = 0;

    for (const semesterId in semesterGPA) {
      totalGPA += semesterGPA[semesterId];
      semesterCount++;

      cgpa[semesterId] = (totalGPA / semesterCount).toFixed(2); // Keep CGPA to two decimal places
    }

    res.json({
      completedModules,
      repeatedModules,
      cgpa: cgpa[semesterCount] || 0 // Use the latest CGPA
    });
  } catch (error) {
    console.error('Error fetching student dashboard data:', error);
    res.status(500).json({ error: 'An error occurred' });
  }

}

const getGenderCount = async(req,res)=>{
  try {
    // Query to get gender count for non-student roles
    const nonStudentQuery = `
      SELECT gender, COUNT(*) as count
      FROM users
      WHERE role != 'Student'
      GROUP BY gender;
    `;

    // Query to get gender count for students only
    const studentQuery = `
      SELECT gender, COUNT(*) as count
      FROM students
      GROUP BY gender;
    `;

    const [nonStudentResult, studentResult] = await Promise.all([
      pool.query(nonStudentQuery),
      pool.query(studentQuery),
    ]);

    res.json({
      nonStudentGenderCount: nonStudentResult.rows,
      studentGenderCount: studentResult.rows,
    });
    // console.log(nonStudentResult.rows)
    // console.log(studentResult.rows)
  } catch (error) {
    console.error('Error executing query', error.stack);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}
module.exports = {
    addStudents,
    getStudents,
    getFirstYearStudents,
    getStudentMarks,
    getStudentGPA,
    updateStudent,
    getStudentDashboardData,
    getGenderCount,
    updateStatus
};
