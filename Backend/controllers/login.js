const pool = require('../db.js');
const bcrypt = require('bcryptjs');

const login = async (req,res) => {
    try {
        const { password, email } = req.body;
        console.log(email);
    
        // Find user by username
        const user = await pool.query('SELECT * FROM users WHERE email = $1 AND status=$2', [email,'Active']);
    
        if (!user.rows[0]) {
          return res.status(200).json({success:false, error: 'Invalid username or password' });
        }
        console.log(user.rows[0])
        
        // Compare hashed passwords
        const validPassword = await bcrypt.compare(password, user.rows[0].password);
        console.log(validPassword);

        if (!validPassword) {
          return res.status(200).json({success:false, error: 'Invalid username or password' });
        }
    
        // Create JWT payload

        res.status(200).json({success:true, role: user.rows[0].role, user_id: user.rows[0].user_id});
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error logging in' });
      }
}

module.exports = {
    login,
}