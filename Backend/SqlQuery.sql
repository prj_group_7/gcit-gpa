CREATE TABLE "users" (
  "user_id" SERIAL PRIMARY KEY,
  "username" VARCHAR,
  "email" VARCHAR,
  "password" VARCHAR,
  "role" VARCHAR
);

CREATE TABLE "students" (
  "student_id" int PRIMARY KEY,
  "user_id" int,
  "name" varchar,
  "address" varchar,
  "phone" varchar,
  "type" varchar,
  "date_of_birth" date,
  "gender" varchar,
  "year_of_enrollment" int,
  "programoffered_id" int,
  "enrolled_semester" int
);

CREATE TABLE "programoffereds" (
  "programoffered_id" SERIAL PRIMARY KEY,
  "program_id" int,
  "year" int,
  "teamlead_id" int
);


CREATE TABLE "programs" (
  "program_id" SERIAL PRIMARY KEY,
  "program_name" varchar,
  "program_description" text,
  "program_duration" int,
  "status" varchar
);

ALTER TABLE "programoffereds" ADD FOREIGN KEY ("program_id") REFERENCES "programs" ("program_id");


ALTER TABLE "programoffereds" ADD FOREIGN KEY ("teamlead_id") REFERENCES "users" ("user_id");

ALTER TABLE "students" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("user_id");
ALTER TABLE "students" ADD FOREIGN KEY ("programoffered_id") REFERENCES "programoffereds" ("programoffered_id");




CREATE TABLE "modules" (
  "module_id" SERIAL PRIMARY KEY,
  "module_name" varchar,
  "module_code" varchar,
  "module_description" text,
  "credits" int,
  "status" varchar
);



CREATE TABLE "modulesoffereds" (
  "moduleoffered_id" SERIAL PRIMARY KEY,
  "module_id" int,
  "programoffered_id" int,
  "semester_id" int,
  "module_verifier" int,
  "tutor" int,
  "section_id" int,
  "status" varchar DEFAULT NULL,
  "mr_students" INTEGER[]
);

-- ALTER TABLE "modulesoffereds" ADD FOREIGN KEY ("semester_id") REFERENCES "Semester" ("semester_id");
ALTER TABLE "modulesoffereds" ADD FOREIGN KEY ("programoffered_id") REFERENCES "programoffereds" ("programoffered_id");
ALTER TABLE "modulesoffereds" ADD FOREIGN KEY ("module_verifier") REFERENCES "users" ("user_id");
ALTER TABLE "modulesoffereds" ADD FOREIGN KEY ("tutor") REFERENCES "users" ("user_id");


CREATE TABLE "marks" (
  "marks_id" SERIAL PRIMARY KEY,
  "student_id" int,
  "modulesoffered_id" int,
  "marks_obtained" int,
  "staff_id" int,
  "module_verifier" int DEFAULT NULL,
  "gradepoint" numeric(3,1) DEFAULT 0.0,
  "grade" varchar DEFAULT null,
  "verified" bool DEFAULT TRUE,
  "clear" bool DEFAULT TRUE
);

ALTER TABLE "marks" ADD FOREIGN KEY ("modulesoffered_id") REFERENCES "modulesoffereds" ("moduleoffered_id");
ALTER TABLE "marks" ADD FOREIGN KEY ("student_id") REFERENCES "students" ("student_id"); 
ALTER TABLE "marks" ADD FOREIGN KEY ("staff_id") REFERENCES "users" ("user_id");
ALTER TABLE "marks" ADD FOREIGN KEY ("module_verifier") REFERENCES "users" ("user_id");
ALTER TABLE marks ADD CONSTRAINT unique_student_module UNIQUE (student_id, modulesoffered_id);