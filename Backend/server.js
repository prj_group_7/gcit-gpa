const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const loginRoute = require('./routes/login.js')
const userRoutes = require('./routes/admin/user.js');
const studentRoutes = require('./routes/admin/student.js');
const moduleRoutes = require('./routes/coursemanager/modules.js')
const programRoutes = require('./routes/coursemanager/program.js')
const programOfferedRoutes = require('./routes/coursemanager/programoffered.js')
const moduleOfferedRoutes = require('./routes/coursemanager/modulesoffered.js')
const tutorRoutes= require('./routes/tutors/main.js')
const marksRoutes = require('./routes/tutors/marks.js')
dotenv.config();
const app = express();
app.use(express.json({ limit: '10mb' }));
const PORT =  3001;
app.use(cors());


// Routes
app.use('/login', loginRoute);
app.use('/users', userRoutes);
app.use('/students',studentRoutes);
app.use('/coursemanager/modules', moduleRoutes);
app.use('/coursemanager/programs', programRoutes);
app.use('/coursemanager/programoffered',programOfferedRoutes)
app.use('/coursemanager/modulesoffered',moduleOfferedRoutes)
app.use('/tutor', tutorRoutes)
app.use('/tutor/marks',marksRoutes)

app.listen(PORT, () => {
  console.log('listening on port ' + PORT);
});