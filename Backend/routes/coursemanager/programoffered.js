const express = require('express');
const router = express.Router();
const { addOfferedProgram, getProgramOffered, getOfferedProgramDetail, getProgramOfferedById } = require('../../controllers/coursemanager/programoffered')

router
    .route('/')
    .get(getProgramOffered)
    .post(addOfferedProgram);
router
    .route('/:program_id')
    .get(getProgramOfferedById)
router.get('/getprogramoffereddetails', getOfferedProgramDetail);



module.exports = router