const express = require('express');
const router = express.Router();
const {addProgram, getProgram,updateProgram} = require('../../controllers/coursemanager/program')

router
    .route('/')
    .get(getProgram)
    .post(addProgram)



module.exports = router