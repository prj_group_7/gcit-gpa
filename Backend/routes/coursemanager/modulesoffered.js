const express = require('express');
const router = express.Router();
const {addToModulesOffered,
    getModulesOffered,
    getStudents,
    approve,
    compileModulesOffered, 
    compileOne, 
    compileAllModules} = require('../../controllers/coursemanager/modulesoffered')

router
    .route('/')
    .post(addToModulesOffered)

router.get('/getmodulesoffered', getModulesOffered)

router.get('/getstudents', getStudents)

router.post('/approve', approve)

router.get('/compilemodules', compileModulesOffered)

router.post('/compileone', compileOne)

router.post('/compileall', compileAllModules)


module.exports = router