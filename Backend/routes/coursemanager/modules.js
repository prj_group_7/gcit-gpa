const express = require('express');
const router = express.Router();
const {addModule, getModule,updateModule} = require('../../controllers/coursemanager/modules')

router
    .route('/')
    .get(getModule)
    .post(addModule)

module.exports = router