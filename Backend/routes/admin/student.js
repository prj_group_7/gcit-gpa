const express = require('express');
const router = express.Router();
const {addStudents, getStudents,getFirstYearStudents, getStudentMarks, getStudentGPA, updateStudent, getStudentDashboardData, getGenderCount, updateStatus} = require('../../controllers/admin/student')

router
    .route('/')
    .get(getStudents)
    .post(addStudents)

router.get('/marks/:user_id/:semester_id', getStudentMarks)
router.get('/gpa/:user_id', getStudentGPA)
router.post('/update', updateStudent)
router.get('/studentdashboard/:user_id', getStudentDashboardData)
router.get('/gendercount', getGenderCount)
router.post('/updatestudentstatus/:user_id', updateStatus)

router.get('/firstyearstudent',getFirstYearStudents)
module.exports = router