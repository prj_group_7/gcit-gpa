const express = require('express');
const pool = require('../../db'); 
const router = express.Router();
const {addUser,getAllUsers,getTutors,addAll, getUsersWithRole, getUserfromrole, forgotPassword, changePassword} = require('../../controllers/admin/user')

router
    .route('/')
    .post(addUser)
    .get(getAllUsers)

router.get('/tutors', getTutors)
router.post('/addall',addAll)

router.get('/roles',getUsersWithRole)
router.get('/roles/:role',getUserfromrole)

router.post('/forgot-password', forgotPassword);
router.post('/change-password', changePassword);

module.exports = router