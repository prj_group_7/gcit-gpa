const express = require('express')
const router = express.Router();
const {addMarks, getMarks, verifyMarks, getMarksVerifier, addAllMarks, notify} = require('../../controllers/tutors/marks')

router.post('/addmarks', addMarks)
router.get('/getmarks',getMarks)
router.put('/verifymark',verifyMarks)
router.get('/getmarksverifier',getMarksVerifier)
router.post('/addallmarks',addAllMarks)
router.post('/notify', notify)


module.exports = router;