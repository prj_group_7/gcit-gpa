const express = require('express');
const router = express.Router();
const {modulesfortutor, checkModuleVerifier,gettutormodules, getverifiermodules} = require('../../controllers/tutors/main')

router.get('/', modulesfortutor)

router.get('/check',checkModuleVerifier)

router.get('/getmodules', gettutormodules)

router.get('/getverifiermodules',getverifiermodules)

module.exports = router