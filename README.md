# Getting Started 
This guide outlines the steps to set up the development environment and run this PERN stack application locally.

## Prerequisities
-Node.js and npm (or yarn)
- Git client

## Steps to Set Up

1. **Clone the repository**

git clone <https://your_gitlab_repository_url>

2. **Install Dependencies**

cd <project_name>

npm install

or 

yarn install


## Running the Applicationm
**Frontend(React)**

npm run dev

**Backend(Express)**

nodemon server.js

